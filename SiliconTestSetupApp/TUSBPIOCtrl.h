#pragma once
#include "TUSBPIO.h"
#include <iostream>

class TUSBPIOCtrl
{
private:
	int DevId;
	unsigned char PortABitmap;
	unsigned char PortBBitmap;
	unsigned char PortCBitmap;
public:
	TUSBPIOCtrl();
	TUSBPIOCtrl(int id);

	~TUSBPIOCtrl() {
	}
	void SetDevId(int id) {
		DevId = id;
	}
	int GetDevId() {
		return DevId;
	}
	void SetBit(int port, unsigned char bit) {
		if (port == 0)PortABitmap = bit;
		else if (port == 1)PortBBitmap = bit;
		else if (port == 2)PortCBitmap = bit;
		else std::cerr << "strange port selected : " << port << std::endl;
	}
	int Initialize();
	int OpenDevice();
	void CloseDevice();
	int SetAlloutput();
	int SetSWon(int port);
	int SetSWoff();

};

