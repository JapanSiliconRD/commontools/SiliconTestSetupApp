#pragma once
#include "TUSBPIO.h"

namespace SiliconTestSetupApp {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// TUSBPIOControlGUI の概要
	/// </summary>
	/*
	public ref class TUSBPIOControlGUI : public System::Windows::Forms::Form
	{
	public:
		TUSBPIOControlGUI(void)
		{
			InitializeComponent();
			//
			//TODO: ここにコンストラクター コードを追加します
			//
		}

	protected:
		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		~TUSBPIOControlGUI()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = gcnew System::ComponentModel::Container();
			this->Size = System::Drawing::Size(300,300);
			this->Text = L"TUSBPIOControlGUI";
			this->Padding = System::Windows::Forms::Padding(0);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
		}
#pragma endregion
	};
	*/
	public ref class TUSBPIOControlGUI : public System::Windows::Forms::Form
	{
	public:
		TUSBPIOControlGUI(void)
		{
			InitializeComponent();
			//
			//TODO: ここにコンストラクター コードを追加します
			//
			ChangeLock = true;
		}

	protected:
		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		~TUSBPIOControlGUI()
		{
			if (components)
			{
				delete components;
			}
		}
	internal: System::Windows::Forms::RadioButton^ Dev2PortASelOut;
	protected:
	public: System::Windows::Forms::PictureBox^ Dev1PortC_I2;
	internal:
	public: System::Windows::Forms::PictureBox^ Dev1PortB_I7;
	public: System::Windows::Forms::PictureBox^ Dev1PortC_I1;
	public: System::Windows::Forms::PictureBox^ Dev1PortC_I0;
	public: System::Windows::Forms::PictureBox^ Dev1PortB_I6;
	public: System::Windows::Forms::PictureBox^ Dev1PortB_I5;
	public: System::Windows::Forms::PictureBox^ Dev1PortB_I4;
	public: System::Windows::Forms::PictureBox^ Dev1PortB_I3;
	public: System::Windows::Forms::PictureBox^ Dev1PortB_I2;
	public: System::Windows::Forms::PictureBox^ Dev1PortC_I3;
	public: System::Windows::Forms::PictureBox^ Dev1PortB_I1;
	public: System::Windows::Forms::PictureBox^ Dev1PortB_I0;
	public: System::Windows::Forms::PictureBox^ Dev1PortC_I6;
	internal: System::Windows::Forms::CheckBox^ Dev1PortC_O3;
	public:
	public: System::Windows::Forms::PictureBox^ Dev1PortC_I5;
	internal:
	internal: System::Windows::Forms::CheckBox^ Dev1PortC_O2;
	public:
	internal: System::Windows::Forms::RadioButton^ Dev1PortCHSelIn;
	internal: System::Windows::Forms::CheckBox^ Dev1PortC_O1;
	public: System::Windows::Forms::PictureBox^ Dev1PortC_I7;
	internal:
	internal: System::Windows::Forms::CheckBox^ Dev1PortC_O0;
	public:
	internal: System::Windows::Forms::GroupBox^ Dev1PortCH_GB;
	public: System::Windows::Forms::PictureBox^ Dev1PortC_I4;
	internal:
	internal: System::Windows::Forms::CheckBox^ Dev1PortC_O7;
	public:
	internal: System::Windows::Forms::CheckBox^ Dev1PortC_O6;
	internal: System::Windows::Forms::CheckBox^ Dev1PortC_O5;
	internal: System::Windows::Forms::CheckBox^ Dev1PortC_O4;
	internal: System::Windows::Forms::RadioButton^ Dev1PortCHSelOut;
	internal: System::Windows::Forms::RadioButton^ Dev1PortCLSelOut;
	internal: System::Windows::Forms::RadioButton^ Dev1PortCLSelIn;
	internal: System::Windows::Forms::CheckBox^ Dev1PortB_O7;
	internal: System::Windows::Forms::CheckBox^ Dev1PortB_O6;
	public: System::Windows::Forms::PictureBox^ Dev1PortA_I7;
	internal:
	public: System::Windows::Forms::PictureBox^ Dev1PortA_I6;
	public: System::Windows::Forms::PictureBox^ Dev1PortA_I5;
	public: System::Windows::Forms::PictureBox^ Dev1PortA_I4;
	public: System::Windows::Forms::PictureBox^ Dev1PortA_I3;
	internal: System::Windows::Forms::CheckBox^ Dev1PortB_O5;
	public:
	public: System::Windows::Forms::PictureBox^ Dev1PortA_I2;
	internal:
	public: System::Windows::Forms::PictureBox^ Dev1PortA_I1;
	internal: System::Windows::Forms::ImageList^ ImageList1;
	public:
	internal: System::Windows::Forms::Button^ DevClose;
	internal: System::Windows::Forms::Timer^ Timer1;
	public: System::Windows::Forms::PictureBox^ Dev1PortA_I0;
	internal:
	internal: System::Windows::Forms::Button^ DevOpen;
	public:
	internal: System::Windows::Forms::CheckBox^ Dev1PortA_O6;
	internal: System::Windows::Forms::CheckBox^ Dev1PortB_O2;
	internal: System::Windows::Forms::RadioButton^ Dev1PortBSelIn;
	internal: System::Windows::Forms::CheckBox^ Dev1PortA_O7;
	internal: System::Windows::Forms::CheckBox^ Dev1PortB_O3;
	internal: System::Windows::Forms::CheckBox^ Dev1PortA_O5;
	internal: System::Windows::Forms::CheckBox^ Dev1PortB_O4;
	internal: System::Windows::Forms::CheckBox^ Dev1PortA_O4;
	internal: System::Windows::Forms::CheckBox^ Dev1PortA_O3;
	internal: System::Windows::Forms::CheckBox^ Dev1PortB_O1;
	internal: System::Windows::Forms::CheckBox^ Dev1PortB_O0;
	internal: System::Windows::Forms::RadioButton^ Dev1PortBSelOut;
	internal: System::Windows::Forms::CheckBox^ Dev1PortA_O2;
	internal: System::Windows::Forms::CheckBox^ Dev1PortA_O1;
	internal: System::Windows::Forms::Button^ Dev1SetBtn;
	internal: System::Windows::Forms::CheckBox^ Dev1PortA_O0;
	internal: System::Windows::Forms::RadioButton^ Dev1PortASelOut;
	internal: System::Windows::Forms::RadioButton^ Dev1PortASelIn;
	public: System::Windows::Forms::PictureBox^ Dev2PortB_I7;
	internal:
	public: System::Windows::Forms::PictureBox^ Dev2PortB_I6;
	internal: System::Windows::Forms::GroupBox^ Dev1PortA_GB;
	public:
	public: System::Windows::Forms::PictureBox^ Dev2PortB_I5;
	internal:
	public: System::Windows::Forms::PictureBox^ Dev2PortB_I4;
	public: System::Windows::Forms::PictureBox^ Dev2PortB_I3;
	public: System::Windows::Forms::PictureBox^ Dev2PortB_I2;
	public: System::Windows::Forms::PictureBox^ Dev2PortB_I0;
	internal: System::Windows::Forms::GroupBox^ Device1_GB;
	public:
	internal: System::Windows::Forms::GroupBox^ Dev1PortCL_GB;
	internal: System::Windows::Forms::GroupBox^ Dev1PortB_GB;
	public: System::Windows::Forms::PictureBox^ Dev2PortB_I1;
	internal:
	internal: System::Windows::Forms::CheckBox^ Dev2PortB_O7;
	public:
	internal: System::Windows::Forms::CheckBox^ Dev2PortB_O6;
	internal: System::Windows::Forms::CheckBox^ Dev2PortB_O5;
	internal: System::Windows::Forms::CheckBox^ Dev2PortB_O4;
	internal: System::Windows::Forms::CheckBox^ Dev2PortB_O3;
	internal: System::Windows::Forms::CheckBox^ Dev2PortB_O2;
	internal: System::Windows::Forms::CheckBox^ Dev2PortB_O1;
	internal: System::Windows::Forms::CheckBox^ Dev2PortB_O0;
	internal: System::Windows::Forms::GroupBox^ Dev2PortB_GB;
	internal: System::Windows::Forms::RadioButton^ Dev2PortBSelOut;
	internal: System::Windows::Forms::RadioButton^ Dev2PortBSelIn;
	public: System::Windows::Forms::PictureBox^ Dev2PortC_I7;
	internal:
	internal: System::Windows::Forms::CheckBox^ Dev2PortC_O1;
	public:
	internal: System::Windows::Forms::CheckBox^ Dev2PortC_O0;
	public: System::Windows::Forms::PictureBox^ Dev2PortC_I6;
	internal:
	internal: System::Windows::Forms::RadioButton^ Dev2PortCLSelOut;
	public:
	public: System::Windows::Forms::PictureBox^ Dev2PortC_I5;
	internal:
	public: System::Windows::Forms::PictureBox^ Dev2PortC_I4;
	internal: System::Windows::Forms::RadioButton^ Dev2PortCLSelIn;
	public:
	internal: System::Windows::Forms::CheckBox^ Dev2PortC_O7;
	internal: System::Windows::Forms::CheckBox^ Dev2PortC_O6;
	internal: System::Windows::Forms::CheckBox^ Dev2PortC_O5;
	internal: System::Windows::Forms::CheckBox^ Dev2PortC_O4;
	internal: System::Windows::Forms::CheckBox^ Dev2PortC_O2;
	public: System::Windows::Forms::PictureBox^ Dev2PortC_I2;
	internal:
	internal: System::Windows::Forms::GroupBox^ Dev2PortA_GB;
	public:
	public: System::Windows::Forms::PictureBox^ Dev2PortA_I7;
	internal:
	public: System::Windows::Forms::PictureBox^ Dev2PortA_I6;
	public: System::Windows::Forms::PictureBox^ Dev2PortA_I5;
	public: System::Windows::Forms::PictureBox^ Dev2PortA_I4;
	public: System::Windows::Forms::PictureBox^ Dev2PortA_I3;
	public: System::Windows::Forms::PictureBox^ Dev2PortA_I2;
	public: System::Windows::Forms::PictureBox^ Dev2PortA_I1;
	public: System::Windows::Forms::PictureBox^ Dev2PortA_I0;
	internal: System::Windows::Forms::CheckBox^ Dev2PortA_O7;
	public:
	internal: System::Windows::Forms::CheckBox^ Dev2PortA_O6;
	internal: System::Windows::Forms::CheckBox^ Dev2PortA_O5;
	internal: System::Windows::Forms::CheckBox^ Dev2PortA_O4;
	internal: System::Windows::Forms::CheckBox^ Dev2PortA_O3;
	internal: System::Windows::Forms::CheckBox^ Dev2PortA_O2;
	internal: System::Windows::Forms::CheckBox^ Dev2PortA_O1;
	internal: System::Windows::Forms::CheckBox^ Dev2PortA_O0;
	internal: System::Windows::Forms::RadioButton^ Dev2PortASelIn;
	public: System::Windows::Forms::PictureBox^ Dev2PortC_I1;
	internal:
	public: System::Windows::Forms::PictureBox^ Dev2PortC_I3;
	internal: System::Windows::Forms::GroupBox^ Dev2PortCL_GB;
	public:
	public: System::Windows::Forms::PictureBox^ Dev2PortC_I0;
	internal:
	internal: System::Windows::Forms::CheckBox^ Dev2PortC_O3;
	public:
	internal: System::Windows::Forms::RadioButton^ Dev2PortCHSelOut;
	internal: System::Windows::Forms::RadioButton^ Dev2PortCHSelIn;
	internal: System::Windows::Forms::GroupBox^ Dev2PortCH_GB;
	internal: System::Windows::Forms::GroupBox^ Device2_GB;
	internal: System::Windows::Forms::Button^ Dev2SetBtn;
	internal: System::Windows::Forms::ComboBox^ IDNUM;
	private: System::ComponentModel::IContainer^ components;
	internal:

	private:
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>

		short DevId;//デバイスID
		Boolean ChangeLock;//このビットがtrueの時はビット出力設定しない

#pragma region Windows Form Designer generated code
		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(TUSBPIOControlGUI::typeid));
			this->Dev2PortASelOut = (gcnew System::Windows::Forms::RadioButton());
			this->Dev1PortC_I2 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortB_I7 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortC_I1 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortC_I0 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortB_I6 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortB_I5 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortB_I4 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortB_I3 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortB_I2 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortC_I3 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortB_I1 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortB_I0 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortC_I6 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortC_O3 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortC_I5 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortC_O2 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortCHSelIn = (gcnew System::Windows::Forms::RadioButton());
			this->Dev1PortC_O1 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortC_I7 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortC_O0 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortCH_GB = (gcnew System::Windows::Forms::GroupBox());
			this->Dev1PortC_I4 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortC_O7 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortC_O6 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortC_O5 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortC_O4 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortCHSelOut = (gcnew System::Windows::Forms::RadioButton());
			this->Dev1PortCLSelOut = (gcnew System::Windows::Forms::RadioButton());
			this->Dev1PortCLSelIn = (gcnew System::Windows::Forms::RadioButton());
			this->Dev1PortB_O7 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortB_O6 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortA_I7 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortA_I6 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortA_I5 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortA_I4 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortA_I3 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortB_O5 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortA_I2 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortA_I1 = (gcnew System::Windows::Forms::PictureBox());
			this->ImageList1 = (gcnew System::Windows::Forms::ImageList(this->components));
			this->DevClose = (gcnew System::Windows::Forms::Button());
			this->Timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->Dev1PortA_I0 = (gcnew System::Windows::Forms::PictureBox());
			this->DevOpen = (gcnew System::Windows::Forms::Button());
			this->Dev1PortA_O6 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortB_O2 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortBSelIn = (gcnew System::Windows::Forms::RadioButton());
			this->Dev1PortA_O7 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortB_O3 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortA_O5 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortB_O4 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortA_O4 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortA_O3 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortB_O1 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortB_O0 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortBSelOut = (gcnew System::Windows::Forms::RadioButton());
			this->Dev1PortA_O2 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortA_O1 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1SetBtn = (gcnew System::Windows::Forms::Button());
			this->Dev1PortA_O0 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev1PortASelOut = (gcnew System::Windows::Forms::RadioButton());
			this->Dev1PortASelIn = (gcnew System::Windows::Forms::RadioButton());
			this->Dev2PortB_I7 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortB_I6 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev1PortA_GB = (gcnew System::Windows::Forms::GroupBox());
			this->Dev2PortB_I5 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortB_I4 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortB_I3 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortB_I2 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortB_I0 = (gcnew System::Windows::Forms::PictureBox());
			this->Device1_GB = (gcnew System::Windows::Forms::GroupBox());
			this->Dev1PortCL_GB = (gcnew System::Windows::Forms::GroupBox());
			this->Dev1PortB_GB = (gcnew System::Windows::Forms::GroupBox());
			this->Dev2PortB_I1 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortB_O7 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortB_O6 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortB_O5 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortB_O4 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortB_O3 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortB_O2 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortB_O1 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortB_O0 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortB_GB = (gcnew System::Windows::Forms::GroupBox());
			this->Dev2PortBSelOut = (gcnew System::Windows::Forms::RadioButton());
			this->Dev2PortBSelIn = (gcnew System::Windows::Forms::RadioButton());
			this->Dev2PortC_I7 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortC_O1 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortC_O0 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortC_I6 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortCLSelOut = (gcnew System::Windows::Forms::RadioButton());
			this->Dev2PortC_I5 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortC_I4 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortCLSelIn = (gcnew System::Windows::Forms::RadioButton());
			this->Dev2PortC_O7 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortC_O6 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortC_O5 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortC_O4 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortC_O2 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortC_I2 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortA_GB = (gcnew System::Windows::Forms::GroupBox());
			this->Dev2PortA_I7 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortA_I6 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortA_I5 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortA_I4 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortA_I3 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortA_I2 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortA_I1 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortA_I0 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortA_O7 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortA_O6 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortA_O5 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortA_O4 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortA_O3 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortA_O2 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortA_O1 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortA_O0 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortASelIn = (gcnew System::Windows::Forms::RadioButton());
			this->Dev2PortC_I1 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortC_I3 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortCL_GB = (gcnew System::Windows::Forms::GroupBox());
			this->Dev2PortC_I0 = (gcnew System::Windows::Forms::PictureBox());
			this->Dev2PortC_O3 = (gcnew System::Windows::Forms::CheckBox());
			this->Dev2PortCHSelOut = (gcnew System::Windows::Forms::RadioButton());
			this->Dev2PortCHSelIn = (gcnew System::Windows::Forms::RadioButton());
			this->Dev2PortCH_GB = (gcnew System::Windows::Forms::GroupBox());
			this->Device2_GB = (gcnew System::Windows::Forms::GroupBox());
			this->Dev2SetBtn = (gcnew System::Windows::Forms::Button());
			this->IDNUM = (gcnew System::Windows::Forms::ComboBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I0))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I0))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I7))->BeginInit();
			this->Dev1PortCH_GB->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I0))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I6))->BeginInit();
			this->Dev1PortA_GB->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I0))->BeginInit();
			this->Device1_GB->SuspendLayout();
			this->Dev1PortCL_GB->SuspendLayout();
			this->Dev1PortB_GB->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I1))->BeginInit();
			this->Dev2PortB_GB->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I2))->BeginInit();
			this->Dev2PortA_GB->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I0))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I3))->BeginInit();
			this->Dev2PortCL_GB->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I0))->BeginInit();
			this->Dev2PortCH_GB->SuspendLayout();
			this->Device2_GB->SuspendLayout();
			this->SuspendLayout();
			// 
			// Dev2PortASelOut
			// 
			this->Dev2PortASelOut->AutoSize = true;
			this->Dev2PortASelOut->Location = System::Drawing::Point(16, 46);
			this->Dev2PortASelOut->Name = L"Dev2PortASelOut";
			this->Dev2PortASelOut->Size = System::Drawing::Size(57, 17);
			this->Dev2PortASelOut->TabIndex = 1;
			this->Dev2PortASelOut->TabStop = true;
			this->Dev2PortASelOut->Text = L"Output";
			this->Dev2PortASelOut->UseVisualStyleBackColor = true;
			// 
			// Dev1PortC_I2
			// 
			this->Dev1PortC_I2->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortC_I2->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortC_I2->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I2.ErrorImage")));
			this->Dev1PortC_I2->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortC_I2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I2.Image")));
			this->Dev1PortC_I2->Location = System::Drawing::Point(48, 130);
			this->Dev1PortC_I2->Name = L"Dev1PortC_I2";
			this->Dev1PortC_I2->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortC_I2->Size = System::Drawing::Size(16, 17);
			this->Dev1PortC_I2->TabIndex = 15;
			this->Dev1PortC_I2->TabStop = false;
			// 
			// Dev1PortB_I7
			// 
			this->Dev1PortB_I7->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortB_I7->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortB_I7->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I7.ErrorImage")));
			this->Dev1PortB_I7->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortB_I7->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I7.Image")));
			this->Dev1PortB_I7->Location = System::Drawing::Point(48, 248);
			this->Dev1PortB_I7->Name = L"Dev1PortB_I7";
			this->Dev1PortB_I7->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortB_I7->Size = System::Drawing::Size(16, 17);
			this->Dev1PortB_I7->TabIndex = 20;
			this->Dev1PortB_I7->TabStop = false;
			// 
			// Dev1PortC_I1
			// 
			this->Dev1PortC_I1->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortC_I1->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortC_I1->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I1.ErrorImage")));
			this->Dev1PortC_I1->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortC_I1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I1.Image")));
			this->Dev1PortC_I1->Location = System::Drawing::Point(48, 106);
			this->Dev1PortC_I1->Name = L"Dev1PortC_I1";
			this->Dev1PortC_I1->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortC_I1->Size = System::Drawing::Size(16, 17);
			this->Dev1PortC_I1->TabIndex = 14;
			this->Dev1PortC_I1->TabStop = false;
			// 
			// Dev1PortC_I0
			// 
			this->Dev1PortC_I0->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortC_I0->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortC_I0->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I0.ErrorImage")));
			this->Dev1PortC_I0->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortC_I0->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I0.Image")));
			this->Dev1PortC_I0->Location = System::Drawing::Point(48, 82);
			this->Dev1PortC_I0->Name = L"Dev1PortC_I0";
			this->Dev1PortC_I0->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortC_I0->Size = System::Drawing::Size(16, 17);
			this->Dev1PortC_I0->TabIndex = 13;
			this->Dev1PortC_I0->TabStop = false;
			// 
			// Dev1PortB_I6
			// 
			this->Dev1PortB_I6->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortB_I6->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortB_I6->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I6.ErrorImage")));
			this->Dev1PortB_I6->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortB_I6->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I6.Image")));
			this->Dev1PortB_I6->Location = System::Drawing::Point(48, 224);
			this->Dev1PortB_I6->Name = L"Dev1PortB_I6";
			this->Dev1PortB_I6->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortB_I6->Size = System::Drawing::Size(16, 17);
			this->Dev1PortB_I6->TabIndex = 19;
			this->Dev1PortB_I6->TabStop = false;
			// 
			// Dev1PortB_I5
			// 
			this->Dev1PortB_I5->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortB_I5->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortB_I5->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I5.ErrorImage")));
			this->Dev1PortB_I5->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortB_I5->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I5.Image")));
			this->Dev1PortB_I5->Location = System::Drawing::Point(48, 201);
			this->Dev1PortB_I5->Name = L"Dev1PortB_I5";
			this->Dev1PortB_I5->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortB_I5->Size = System::Drawing::Size(16, 17);
			this->Dev1PortB_I5->TabIndex = 18;
			this->Dev1PortB_I5->TabStop = false;
			// 
			// Dev1PortB_I4
			// 
			this->Dev1PortB_I4->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortB_I4->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortB_I4->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I4.ErrorImage")));
			this->Dev1PortB_I4->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortB_I4->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I4.Image")));
			this->Dev1PortB_I4->Location = System::Drawing::Point(48, 176);
			this->Dev1PortB_I4->Name = L"Dev1PortB_I4";
			this->Dev1PortB_I4->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortB_I4->Size = System::Drawing::Size(16, 17);
			this->Dev1PortB_I4->TabIndex = 17;
			this->Dev1PortB_I4->TabStop = false;
			// 
			// Dev1PortB_I3
			// 
			this->Dev1PortB_I3->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortB_I3->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortB_I3->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I3.ErrorImage")));
			this->Dev1PortB_I3->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortB_I3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I3.Image")));
			this->Dev1PortB_I3->Location = System::Drawing::Point(48, 154);
			this->Dev1PortB_I3->Name = L"Dev1PortB_I3";
			this->Dev1PortB_I3->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortB_I3->Size = System::Drawing::Size(16, 17);
			this->Dev1PortB_I3->TabIndex = 16;
			this->Dev1PortB_I3->TabStop = false;
			// 
			// Dev1PortB_I2
			// 
			this->Dev1PortB_I2->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortB_I2->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortB_I2->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I2.ErrorImage")));
			this->Dev1PortB_I2->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortB_I2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I2.Image")));
			this->Dev1PortB_I2->Location = System::Drawing::Point(48, 130);
			this->Dev1PortB_I2->Name = L"Dev1PortB_I2";
			this->Dev1PortB_I2->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortB_I2->Size = System::Drawing::Size(16, 17);
			this->Dev1PortB_I2->TabIndex = 15;
			this->Dev1PortB_I2->TabStop = false;
			// 
			// Dev1PortC_I3
			// 
			this->Dev1PortC_I3->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortC_I3->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortC_I3->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I3.ErrorImage")));
			this->Dev1PortC_I3->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortC_I3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I3.Image")));
			this->Dev1PortC_I3->Location = System::Drawing::Point(48, 154);
			this->Dev1PortC_I3->Name = L"Dev1PortC_I3";
			this->Dev1PortC_I3->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortC_I3->Size = System::Drawing::Size(16, 17);
			this->Dev1PortC_I3->TabIndex = 16;
			this->Dev1PortC_I3->TabStop = false;
			// 
			// Dev1PortB_I1
			// 
			this->Dev1PortB_I1->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortB_I1->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortB_I1->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I1.ErrorImage")));
			this->Dev1PortB_I1->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortB_I1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I1.Image")));
			this->Dev1PortB_I1->Location = System::Drawing::Point(48, 106);
			this->Dev1PortB_I1->Name = L"Dev1PortB_I1";
			this->Dev1PortB_I1->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortB_I1->Size = System::Drawing::Size(16, 17);
			this->Dev1PortB_I1->TabIndex = 14;
			this->Dev1PortB_I1->TabStop = false;
			// 
			// Dev1PortB_I0
			// 
			this->Dev1PortB_I0->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortB_I0->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortB_I0->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I0.ErrorImage")));
			this->Dev1PortB_I0->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortB_I0->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortB_I0.Image")));
			this->Dev1PortB_I0->Location = System::Drawing::Point(48, 82);
			this->Dev1PortB_I0->Name = L"Dev1PortB_I0";
			this->Dev1PortB_I0->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortB_I0->Size = System::Drawing::Size(16, 17);
			this->Dev1PortB_I0->TabIndex = 13;
			this->Dev1PortB_I0->TabStop = false;
			// 
			// Dev1PortC_I6
			// 
			this->Dev1PortC_I6->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortC_I6->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortC_I6->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I6.ErrorImage")));
			this->Dev1PortC_I6->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortC_I6->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I6.Image")));
			this->Dev1PortC_I6->Location = System::Drawing::Point(48, 130);
			this->Dev1PortC_I6->Name = L"Dev1PortC_I6";
			this->Dev1PortC_I6->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortC_I6->Size = System::Drawing::Size(16, 17);
			this->Dev1PortC_I6->TabIndex = 15;
			this->Dev1PortC_I6->TabStop = false;
			// 
			// Dev1PortC_O3
			// 
			this->Dev1PortC_O3->AutoSize = true;
			this->Dev1PortC_O3->Location = System::Drawing::Point(16, 153);
			this->Dev1PortC_O3->Name = L"Dev1PortC_O3";
			this->Dev1PortC_O3->Size = System::Drawing::Size(32, 17);
			this->Dev1PortC_O3->TabIndex = 5;
			this->Dev1PortC_O3->Text = L"3";
			this->Dev1PortC_O3->UseVisualStyleBackColor = true;
			this->Dev1PortC_O3->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortC_O_Changed);
			// 
			// Dev1PortC_I5
			// 
			this->Dev1PortC_I5->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortC_I5->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortC_I5->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I5.ErrorImage")));
			this->Dev1PortC_I5->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortC_I5->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I5.Image")));
			this->Dev1PortC_I5->Location = System::Drawing::Point(48, 106);
			this->Dev1PortC_I5->Name = L"Dev1PortC_I5";
			this->Dev1PortC_I5->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortC_I5->Size = System::Drawing::Size(16, 17);
			this->Dev1PortC_I5->TabIndex = 14;
			this->Dev1PortC_I5->TabStop = false;
			// 
			// Dev1PortC_O2
			// 
			this->Dev1PortC_O2->AutoSize = true;
			this->Dev1PortC_O2->Location = System::Drawing::Point(16, 129);
			this->Dev1PortC_O2->Name = L"Dev1PortC_O2";
			this->Dev1PortC_O2->Size = System::Drawing::Size(32, 17);
			this->Dev1PortC_O2->TabIndex = 4;
			this->Dev1PortC_O2->Text = L"2";
			this->Dev1PortC_O2->UseVisualStyleBackColor = true;
			this->Dev1PortC_O2->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortC_O_Changed);
			// 
			// Dev1PortCHSelIn
			// 
			this->Dev1PortCHSelIn->AutoSize = true;
			this->Dev1PortCHSelIn->Location = System::Drawing::Point(16, 23);
			this->Dev1PortCHSelIn->Name = L"Dev1PortCHSelIn";
			this->Dev1PortCHSelIn->Size = System::Drawing::Size(49, 17);
			this->Dev1PortCHSelIn->TabIndex = 0;
			this->Dev1PortCHSelIn->TabStop = true;
			this->Dev1PortCHSelIn->Text = L"Input";
			this->Dev1PortCHSelIn->UseVisualStyleBackColor = true;
			// 
			// Dev1PortC_O1
			// 
			this->Dev1PortC_O1->AutoSize = true;
			this->Dev1PortC_O1->Location = System::Drawing::Point(16, 105);
			this->Dev1PortC_O1->Name = L"Dev1PortC_O1";
			this->Dev1PortC_O1->Size = System::Drawing::Size(32, 17);
			this->Dev1PortC_O1->TabIndex = 3;
			this->Dev1PortC_O1->Text = L"1";
			this->Dev1PortC_O1->UseVisualStyleBackColor = true;
			this->Dev1PortC_O1->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortC_O_Changed);
			// 
			// Dev1PortC_I7
			// 
			this->Dev1PortC_I7->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortC_I7->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortC_I7->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I7.ErrorImage")));
			this->Dev1PortC_I7->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortC_I7->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I7.Image")));
			this->Dev1PortC_I7->Location = System::Drawing::Point(48, 154);
			this->Dev1PortC_I7->Name = L"Dev1PortC_I7";
			this->Dev1PortC_I7->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortC_I7->Size = System::Drawing::Size(16, 17);
			this->Dev1PortC_I7->TabIndex = 16;
			this->Dev1PortC_I7->TabStop = false;
			// 
			// Dev1PortC_O0
			// 
			this->Dev1PortC_O0->AutoSize = true;
			this->Dev1PortC_O0->Location = System::Drawing::Point(16, 82);
			this->Dev1PortC_O0->Name = L"Dev1PortC_O0";
			this->Dev1PortC_O0->Size = System::Drawing::Size(32, 17);
			this->Dev1PortC_O0->TabIndex = 2;
			this->Dev1PortC_O0->Text = L"0";
			this->Dev1PortC_O0->UseVisualStyleBackColor = true;
			this->Dev1PortC_O0->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortC_O_Changed);
			// 
			// Dev1PortCH_GB
			// 
			this->Dev1PortCH_GB->Controls->Add(this->Dev1PortC_I7);
			this->Dev1PortCH_GB->Controls->Add(this->Dev1PortC_I6);
			this->Dev1PortCH_GB->Controls->Add(this->Dev1PortC_I5);
			this->Dev1PortCH_GB->Controls->Add(this->Dev1PortC_I4);
			this->Dev1PortCH_GB->Controls->Add(this->Dev1PortC_O7);
			this->Dev1PortCH_GB->Controls->Add(this->Dev1PortC_O6);
			this->Dev1PortCH_GB->Controls->Add(this->Dev1PortC_O5);
			this->Dev1PortCH_GB->Controls->Add(this->Dev1PortC_O4);
			this->Dev1PortCH_GB->Controls->Add(this->Dev1PortCHSelOut);
			this->Dev1PortCH_GB->Controls->Add(this->Dev1PortCHSelIn);
			this->Dev1PortCH_GB->Location = System::Drawing::Point(268, 66);
			this->Dev1PortCH_GB->Name = L"Dev1PortCH_GB";
			this->Dev1PortCH_GB->Size = System::Drawing::Size(79, 188);
			this->Dev1PortCH_GB->TabIndex = 3;
			this->Dev1PortCH_GB->TabStop = false;
			this->Dev1PortCH_GB->Text = L"PortC_High";
			// 
			// Dev1PortC_I4
			// 
			this->Dev1PortC_I4->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortC_I4->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortC_I4->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I4.ErrorImage")));
			this->Dev1PortC_I4->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortC_I4->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortC_I4.Image")));
			this->Dev1PortC_I4->Location = System::Drawing::Point(48, 82);
			this->Dev1PortC_I4->Name = L"Dev1PortC_I4";
			this->Dev1PortC_I4->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortC_I4->Size = System::Drawing::Size(16, 17);
			this->Dev1PortC_I4->TabIndex = 13;
			this->Dev1PortC_I4->TabStop = false;
			// 
			// Dev1PortC_O7
			// 
			this->Dev1PortC_O7->AutoSize = true;
			this->Dev1PortC_O7->Location = System::Drawing::Point(16, 153);
			this->Dev1PortC_O7->Name = L"Dev1PortC_O7";
			this->Dev1PortC_O7->Size = System::Drawing::Size(32, 17);
			this->Dev1PortC_O7->TabIndex = 5;
			this->Dev1PortC_O7->Text = L"7";
			this->Dev1PortC_O7->UseVisualStyleBackColor = true;
			this->Dev1PortC_O7->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortC_O_Changed);
			// 
			// Dev1PortC_O6
			// 
			this->Dev1PortC_O6->AutoSize = true;
			this->Dev1PortC_O6->Location = System::Drawing::Point(16, 129);
			this->Dev1PortC_O6->Name = L"Dev1PortC_O6";
			this->Dev1PortC_O6->Size = System::Drawing::Size(32, 17);
			this->Dev1PortC_O6->TabIndex = 4;
			this->Dev1PortC_O6->Text = L"6";
			this->Dev1PortC_O6->UseVisualStyleBackColor = true;
			this->Dev1PortC_O6->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortC_O_Changed);
			// 
			// Dev1PortC_O5
			// 
			this->Dev1PortC_O5->AutoSize = true;
			this->Dev1PortC_O5->Location = System::Drawing::Point(16, 105);
			this->Dev1PortC_O5->Name = L"Dev1PortC_O5";
			this->Dev1PortC_O5->Size = System::Drawing::Size(32, 17);
			this->Dev1PortC_O5->TabIndex = 3;
			this->Dev1PortC_O5->Text = L"5";
			this->Dev1PortC_O5->UseVisualStyleBackColor = true;
			this->Dev1PortC_O5->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortC_O_Changed);
			// 
			// Dev1PortC_O4
			// 
			this->Dev1PortC_O4->AutoSize = true;
			this->Dev1PortC_O4->Location = System::Drawing::Point(16, 82);
			this->Dev1PortC_O4->Name = L"Dev1PortC_O4";
			this->Dev1PortC_O4->Size = System::Drawing::Size(32, 17);
			this->Dev1PortC_O4->TabIndex = 2;
			this->Dev1PortC_O4->Text = L"4";
			this->Dev1PortC_O4->UseVisualStyleBackColor = true;
			this->Dev1PortC_O4->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortC_O_Changed);
			// 
			// Dev1PortCHSelOut
			// 
			this->Dev1PortCHSelOut->AutoSize = true;
			this->Dev1PortCHSelOut->Location = System::Drawing::Point(16, 46);
			this->Dev1PortCHSelOut->Name = L"Dev1PortCHSelOut";
			this->Dev1PortCHSelOut->Size = System::Drawing::Size(57, 17);
			this->Dev1PortCHSelOut->TabIndex = 1;
			this->Dev1PortCHSelOut->TabStop = true;
			this->Dev1PortCHSelOut->Text = L"Output";
			this->Dev1PortCHSelOut->UseVisualStyleBackColor = true;
			// 
			// Dev1PortCLSelOut
			// 
			this->Dev1PortCLSelOut->AutoSize = true;
			this->Dev1PortCLSelOut->Location = System::Drawing::Point(16, 46);
			this->Dev1PortCLSelOut->Name = L"Dev1PortCLSelOut";
			this->Dev1PortCLSelOut->Size = System::Drawing::Size(57, 17);
			this->Dev1PortCLSelOut->TabIndex = 1;
			this->Dev1PortCLSelOut->TabStop = true;
			this->Dev1PortCLSelOut->Text = L"Output";
			this->Dev1PortCLSelOut->UseVisualStyleBackColor = true;
			// 
			// Dev1PortCLSelIn
			// 
			this->Dev1PortCLSelIn->AutoSize = true;
			this->Dev1PortCLSelIn->Location = System::Drawing::Point(16, 23);
			this->Dev1PortCLSelIn->Name = L"Dev1PortCLSelIn";
			this->Dev1PortCLSelIn->Size = System::Drawing::Size(49, 17);
			this->Dev1PortCLSelIn->TabIndex = 0;
			this->Dev1PortCLSelIn->TabStop = true;
			this->Dev1PortCLSelIn->Text = L"Input";
			this->Dev1PortCLSelIn->UseVisualStyleBackColor = true;
			// 
			// Dev1PortB_O7
			// 
			this->Dev1PortB_O7->AutoSize = true;
			this->Dev1PortB_O7->Location = System::Drawing::Point(16, 248);
			this->Dev1PortB_O7->Name = L"Dev1PortB_O7";
			this->Dev1PortB_O7->Size = System::Drawing::Size(32, 17);
			this->Dev1PortB_O7->TabIndex = 9;
			this->Dev1PortB_O7->Text = L"7";
			this->Dev1PortB_O7->UseVisualStyleBackColor = true;
			this->Dev1PortB_O7->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortB_O_Changed);
			// 
			// Dev1PortB_O6
			// 
			this->Dev1PortB_O6->AutoSize = true;
			this->Dev1PortB_O6->Location = System::Drawing::Point(16, 224);
			this->Dev1PortB_O6->Name = L"Dev1PortB_O6";
			this->Dev1PortB_O6->Size = System::Drawing::Size(32, 17);
			this->Dev1PortB_O6->TabIndex = 8;
			this->Dev1PortB_O6->Text = L"6";
			this->Dev1PortB_O6->UseVisualStyleBackColor = true;
			this->Dev1PortB_O6->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortB_O_Changed);
			// 
			// Dev1PortA_I7
			// 
			this->Dev1PortA_I7->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortA_I7->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortA_I7->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I7.ErrorImage")));
			this->Dev1PortA_I7->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortA_I7->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I7.Image")));
			this->Dev1PortA_I7->Location = System::Drawing::Point(48, 248);
			this->Dev1PortA_I7->Name = L"Dev1PortA_I7";
			this->Dev1PortA_I7->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortA_I7->Size = System::Drawing::Size(16, 17);
			this->Dev1PortA_I7->TabIndex = 20;
			this->Dev1PortA_I7->TabStop = false;
			// 
			// Dev1PortA_I6
			// 
			this->Dev1PortA_I6->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortA_I6->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortA_I6->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I6.ErrorImage")));
			this->Dev1PortA_I6->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortA_I6->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I6.Image")));
			this->Dev1PortA_I6->Location = System::Drawing::Point(48, 224);
			this->Dev1PortA_I6->Name = L"Dev1PortA_I6";
			this->Dev1PortA_I6->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortA_I6->Size = System::Drawing::Size(16, 17);
			this->Dev1PortA_I6->TabIndex = 19;
			this->Dev1PortA_I6->TabStop = false;
			// 
			// Dev1PortA_I5
			// 
			this->Dev1PortA_I5->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortA_I5->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortA_I5->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I5.ErrorImage")));
			this->Dev1PortA_I5->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortA_I5->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I5.Image")));
			this->Dev1PortA_I5->Location = System::Drawing::Point(48, 201);
			this->Dev1PortA_I5->Name = L"Dev1PortA_I5";
			this->Dev1PortA_I5->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortA_I5->Size = System::Drawing::Size(16, 17);
			this->Dev1PortA_I5->TabIndex = 18;
			this->Dev1PortA_I5->TabStop = false;
			// 
			// Dev1PortA_I4
			// 
			this->Dev1PortA_I4->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortA_I4->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortA_I4->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I4.ErrorImage")));
			this->Dev1PortA_I4->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortA_I4->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I4.Image")));
			this->Dev1PortA_I4->Location = System::Drawing::Point(48, 176);
			this->Dev1PortA_I4->Name = L"Dev1PortA_I4";
			this->Dev1PortA_I4->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortA_I4->Size = System::Drawing::Size(16, 17);
			this->Dev1PortA_I4->TabIndex = 17;
			this->Dev1PortA_I4->TabStop = false;
			// 
			// Dev1PortA_I3
			// 
			this->Dev1PortA_I3->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortA_I3->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortA_I3->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I3.ErrorImage")));
			this->Dev1PortA_I3->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortA_I3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I3.Image")));
			this->Dev1PortA_I3->Location = System::Drawing::Point(48, 154);
			this->Dev1PortA_I3->Name = L"Dev1PortA_I3";
			this->Dev1PortA_I3->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortA_I3->Size = System::Drawing::Size(16, 17);
			this->Dev1PortA_I3->TabIndex = 16;
			this->Dev1PortA_I3->TabStop = false;
			// 
			// Dev1PortB_O5
			// 
			this->Dev1PortB_O5->AutoSize = true;
			this->Dev1PortB_O5->Location = System::Drawing::Point(16, 201);
			this->Dev1PortB_O5->Name = L"Dev1PortB_O5";
			this->Dev1PortB_O5->Size = System::Drawing::Size(32, 17);
			this->Dev1PortB_O5->TabIndex = 7;
			this->Dev1PortB_O5->Text = L"5";
			this->Dev1PortB_O5->UseVisualStyleBackColor = true;
			this->Dev1PortB_O5->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortB_O_Changed);
			// 
			// Dev1PortA_I2
			// 
			this->Dev1PortA_I2->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortA_I2->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortA_I2->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I2.ErrorImage")));
			this->Dev1PortA_I2->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortA_I2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I2.Image")));
			this->Dev1PortA_I2->Location = System::Drawing::Point(48, 130);
			this->Dev1PortA_I2->Name = L"Dev1PortA_I2";
			this->Dev1PortA_I2->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortA_I2->Size = System::Drawing::Size(16, 17);
			this->Dev1PortA_I2->TabIndex = 15;
			this->Dev1PortA_I2->TabStop = false;
			// 
			// Dev1PortA_I1
			// 
			this->Dev1PortA_I1->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortA_I1->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortA_I1->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I1.ErrorImage")));
			this->Dev1PortA_I1->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortA_I1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I1.Image")));
			this->Dev1PortA_I1->Location = System::Drawing::Point(48, 106);
			this->Dev1PortA_I1->Name = L"Dev1PortA_I1";
			this->Dev1PortA_I1->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortA_I1->Size = System::Drawing::Size(16, 17);
			this->Dev1PortA_I1->TabIndex = 14;
			this->Dev1PortA_I1->TabStop = false;
			// 
			// ImageList1
			// 
			this->ImageList1->ImageStream = (cli::safe_cast<System::Windows::Forms::ImageListStreamer^>(resources->GetObject(L"ImageList1.ImageStream")));
			this->ImageList1->TransparentColor = System::Drawing::Color::Transparent;
			this->ImageList1->Images->SetKeyName(0, L"Bitmap1.gif");
			this->ImageList1->Images->SetKeyName(1, L"Bitmap2.gif");
			// 
			// DevClose
			// 
			this->DevClose->Location = System::Drawing::Point(259, 350);
			this->DevClose->Name = L"DevClose";
			this->DevClose->Size = System::Drawing::Size(96, 26);
			this->DevClose->TabIndex = 28;
			this->DevClose->Text = L"Dev_Close";
			this->DevClose->Click += gcnew System::EventHandler(this, &TUSBPIOControlGUI::DevClose_Click);
			// 
			// Timer1
			// 
			this->Timer1->Tick += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Timer1_Tick);
			// 
			// Dev1PortA_I0
			// 
			this->Dev1PortA_I0->BackColor = System::Drawing::SystemColors::Control;
			this->Dev1PortA_I0->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev1PortA_I0->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I0.ErrorImage")));
			this->Dev1PortA_I0->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev1PortA_I0->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev1PortA_I0.Image")));
			this->Dev1PortA_I0->Location = System::Drawing::Point(48, 82);
			this->Dev1PortA_I0->Name = L"Dev1PortA_I0";
			this->Dev1PortA_I0->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev1PortA_I0->Size = System::Drawing::Size(16, 17);
			this->Dev1PortA_I0->TabIndex = 13;
			this->Dev1PortA_I0->TabStop = false;
			// 
			// DevOpen
			// 
			this->DevOpen->Location = System::Drawing::Point(141, 350);
			this->DevOpen->Name = L"DevOpen";
			this->DevOpen->Size = System::Drawing::Size(96, 26);
			this->DevOpen->TabIndex = 27;
			this->DevOpen->Text = L"Dev_Open";
			this->DevOpen->Click += gcnew System::EventHandler(this, &TUSBPIOControlGUI::DevOpen_Click);
			// 
			// Dev1PortA_O6
			// 
			this->Dev1PortA_O6->AutoSize = true;
			this->Dev1PortA_O6->Location = System::Drawing::Point(16, 224);
			this->Dev1PortA_O6->Name = L"Dev1PortA_O6";
			this->Dev1PortA_O6->Size = System::Drawing::Size(32, 17);
			this->Dev1PortA_O6->TabIndex = 8;
			this->Dev1PortA_O6->Text = L"6";
			this->Dev1PortA_O6->UseVisualStyleBackColor = true;
			this->Dev1PortA_O6->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortA_O_Changed);
			// 
			// Dev1PortB_O2
			// 
			this->Dev1PortB_O2->AutoSize = true;
			this->Dev1PortB_O2->Location = System::Drawing::Point(16, 129);
			this->Dev1PortB_O2->Name = L"Dev1PortB_O2";
			this->Dev1PortB_O2->Size = System::Drawing::Size(32, 17);
			this->Dev1PortB_O2->TabIndex = 4;
			this->Dev1PortB_O2->Text = L"2";
			this->Dev1PortB_O2->UseVisualStyleBackColor = true;
			this->Dev1PortB_O2->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortB_O_Changed);
			// 
			// Dev1PortBSelIn
			// 
			this->Dev1PortBSelIn->AutoSize = true;
			this->Dev1PortBSelIn->Location = System::Drawing::Point(16, 23);
			this->Dev1PortBSelIn->Name = L"Dev1PortBSelIn";
			this->Dev1PortBSelIn->Size = System::Drawing::Size(49, 17);
			this->Dev1PortBSelIn->TabIndex = 0;
			this->Dev1PortBSelIn->TabStop = true;
			this->Dev1PortBSelIn->Text = L"Input";
			this->Dev1PortBSelIn->UseVisualStyleBackColor = true;
			// 
			// Dev1PortA_O7
			// 
			this->Dev1PortA_O7->AutoSize = true;
			this->Dev1PortA_O7->Location = System::Drawing::Point(16, 248);
			this->Dev1PortA_O7->Name = L"Dev1PortA_O7";
			this->Dev1PortA_O7->Size = System::Drawing::Size(32, 17);
			this->Dev1PortA_O7->TabIndex = 9;
			this->Dev1PortA_O7->Text = L"7";
			this->Dev1PortA_O7->UseVisualStyleBackColor = true;
			this->Dev1PortA_O7->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortA_O_Changed);
			// 
			// Dev1PortB_O3
			// 
			this->Dev1PortB_O3->AutoSize = true;
			this->Dev1PortB_O3->Location = System::Drawing::Point(16, 153);
			this->Dev1PortB_O3->Name = L"Dev1PortB_O3";
			this->Dev1PortB_O3->Size = System::Drawing::Size(32, 17);
			this->Dev1PortB_O3->TabIndex = 5;
			this->Dev1PortB_O3->Text = L"3";
			this->Dev1PortB_O3->UseVisualStyleBackColor = true;
			this->Dev1PortB_O3->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortB_O_Changed);
			// 
			// Dev1PortA_O5
			// 
			this->Dev1PortA_O5->AutoSize = true;
			this->Dev1PortA_O5->Location = System::Drawing::Point(16, 201);
			this->Dev1PortA_O5->Name = L"Dev1PortA_O5";
			this->Dev1PortA_O5->Size = System::Drawing::Size(32, 17);
			this->Dev1PortA_O5->TabIndex = 7;
			this->Dev1PortA_O5->Text = L"5";
			this->Dev1PortA_O5->UseVisualStyleBackColor = true;
			this->Dev1PortA_O5->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortA_O_Changed);
			// 
			// Dev1PortB_O4
			// 
			this->Dev1PortB_O4->AutoSize = true;
			this->Dev1PortB_O4->Location = System::Drawing::Point(16, 176);
			this->Dev1PortB_O4->Name = L"Dev1PortB_O4";
			this->Dev1PortB_O4->Size = System::Drawing::Size(32, 17);
			this->Dev1PortB_O4->TabIndex = 6;
			this->Dev1PortB_O4->Text = L"4";
			this->Dev1PortB_O4->UseVisualStyleBackColor = true;
			this->Dev1PortB_O4->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortB_O_Changed);
			// 
			// Dev1PortA_O4
			// 
			this->Dev1PortA_O4->AutoSize = true;
			this->Dev1PortA_O4->Location = System::Drawing::Point(16, 176);
			this->Dev1PortA_O4->Name = L"Dev1PortA_O4";
			this->Dev1PortA_O4->Size = System::Drawing::Size(32, 17);
			this->Dev1PortA_O4->TabIndex = 6;
			this->Dev1PortA_O4->Text = L"4";
			this->Dev1PortA_O4->UseVisualStyleBackColor = true;
			this->Dev1PortA_O4->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortA_O_Changed);
			// 
			// Dev1PortA_O3
			// 
			this->Dev1PortA_O3->AutoSize = true;
			this->Dev1PortA_O3->Location = System::Drawing::Point(16, 153);
			this->Dev1PortA_O3->Name = L"Dev1PortA_O3";
			this->Dev1PortA_O3->Size = System::Drawing::Size(32, 17);
			this->Dev1PortA_O3->TabIndex = 5;
			this->Dev1PortA_O3->Text = L"3";
			this->Dev1PortA_O3->UseVisualStyleBackColor = true;
			this->Dev1PortA_O3->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortA_O_Changed);
			// 
			// Dev1PortB_O1
			// 
			this->Dev1PortB_O1->AutoSize = true;
			this->Dev1PortB_O1->Location = System::Drawing::Point(16, 105);
			this->Dev1PortB_O1->Name = L"Dev1PortB_O1";
			this->Dev1PortB_O1->Size = System::Drawing::Size(32, 17);
			this->Dev1PortB_O1->TabIndex = 3;
			this->Dev1PortB_O1->Text = L"1";
			this->Dev1PortB_O1->UseVisualStyleBackColor = true;
			this->Dev1PortB_O1->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortB_O_Changed);
			// 
			// Dev1PortB_O0
			// 
			this->Dev1PortB_O0->AutoSize = true;
			this->Dev1PortB_O0->Location = System::Drawing::Point(16, 82);
			this->Dev1PortB_O0->Name = L"Dev1PortB_O0";
			this->Dev1PortB_O0->Size = System::Drawing::Size(32, 17);
			this->Dev1PortB_O0->TabIndex = 2;
			this->Dev1PortB_O0->Text = L"0";
			this->Dev1PortB_O0->UseVisualStyleBackColor = true;
			this->Dev1PortB_O0->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortB_O_Changed);
			// 
			// Dev1PortBSelOut
			// 
			this->Dev1PortBSelOut->AutoSize = true;
			this->Dev1PortBSelOut->Location = System::Drawing::Point(16, 46);
			this->Dev1PortBSelOut->Name = L"Dev1PortBSelOut";
			this->Dev1PortBSelOut->Size = System::Drawing::Size(57, 17);
			this->Dev1PortBSelOut->TabIndex = 1;
			this->Dev1PortBSelOut->TabStop = true;
			this->Dev1PortBSelOut->Text = L"Output";
			this->Dev1PortBSelOut->UseVisualStyleBackColor = true;
			this->Dev1PortBSelOut->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortBSelOut_CheckedChanged);
			// 
			// Dev1PortA_O2
			// 
			this->Dev1PortA_O2->AutoSize = true;
			this->Dev1PortA_O2->Location = System::Drawing::Point(16, 129);
			this->Dev1PortA_O2->Name = L"Dev1PortA_O2";
			this->Dev1PortA_O2->Size = System::Drawing::Size(32, 17);
			this->Dev1PortA_O2->TabIndex = 4;
			this->Dev1PortA_O2->Text = L"2";
			this->Dev1PortA_O2->UseVisualStyleBackColor = true;
			this->Dev1PortA_O2->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortA_O_Changed);
			// 
			// Dev1PortA_O1
			// 
			this->Dev1PortA_O1->AutoSize = true;
			this->Dev1PortA_O1->Location = System::Drawing::Point(16, 105);
			this->Dev1PortA_O1->Name = L"Dev1PortA_O1";
			this->Dev1PortA_O1->Size = System::Drawing::Size(32, 17);
			this->Dev1PortA_O1->TabIndex = 3;
			this->Dev1PortA_O1->Text = L"1";
			this->Dev1PortA_O1->UseVisualStyleBackColor = true;
			this->Dev1PortA_O1->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortA_O_Changed);
			// 
			// Dev1SetBtn
			// 
			this->Dev1SetBtn->Location = System::Drawing::Point(183, 26);
			this->Dev1SetBtn->Name = L"Dev1SetBtn";
			this->Dev1SetBtn->Size = System::Drawing::Size(163, 30);
			this->Dev1SetBtn->TabIndex = 18;
			this->Dev1SetBtn->Text = L"DirectionSet";
			this->Dev1SetBtn->UseVisualStyleBackColor = true;
			this->Dev1SetBtn->Click += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1SetBtn_Click);
			// 
			// Dev1PortA_O0
			// 
			this->Dev1PortA_O0->AutoSize = true;
			this->Dev1PortA_O0->Location = System::Drawing::Point(16, 82);
			this->Dev1PortA_O0->Name = L"Dev1PortA_O0";
			this->Dev1PortA_O0->Size = System::Drawing::Size(32, 17);
			this->Dev1PortA_O0->TabIndex = 2;
			this->Dev1PortA_O0->Text = L"0";
			this->Dev1PortA_O0->UseVisualStyleBackColor = true;
			this->Dev1PortA_O0->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev1PortA_O_Changed);
			// 
			// Dev1PortASelOut
			// 
			this->Dev1PortASelOut->AutoSize = true;
			this->Dev1PortASelOut->Location = System::Drawing::Point(16, 46);
			this->Dev1PortASelOut->Name = L"Dev1PortASelOut";
			this->Dev1PortASelOut->Size = System::Drawing::Size(57, 17);
			this->Dev1PortASelOut->TabIndex = 1;
			this->Dev1PortASelOut->TabStop = true;
			this->Dev1PortASelOut->Text = L"Output";
			this->Dev1PortASelOut->UseVisualStyleBackColor = true;
			// 
			// Dev1PortASelIn
			// 
			this->Dev1PortASelIn->AutoSize = true;
			this->Dev1PortASelIn->Location = System::Drawing::Point(16, 23);
			this->Dev1PortASelIn->Name = L"Dev1PortASelIn";
			this->Dev1PortASelIn->Size = System::Drawing::Size(49, 17);
			this->Dev1PortASelIn->TabIndex = 0;
			this->Dev1PortASelIn->TabStop = true;
			this->Dev1PortASelIn->Text = L"Input";
			this->Dev1PortASelIn->UseVisualStyleBackColor = true;
			// 
			// Dev2PortB_I7
			// 
			this->Dev2PortB_I7->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortB_I7->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortB_I7->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I7.ErrorImage")));
			this->Dev2PortB_I7->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortB_I7->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I7.Image")));
			this->Dev2PortB_I7->Location = System::Drawing::Point(48, 248);
			this->Dev2PortB_I7->Name = L"Dev2PortB_I7";
			this->Dev2PortB_I7->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortB_I7->Size = System::Drawing::Size(16, 17);
			this->Dev2PortB_I7->TabIndex = 20;
			this->Dev2PortB_I7->TabStop = false;
			// 
			// Dev2PortB_I6
			// 
			this->Dev2PortB_I6->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortB_I6->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortB_I6->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I6.ErrorImage")));
			this->Dev2PortB_I6->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortB_I6->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I6.Image")));
			this->Dev2PortB_I6->Location = System::Drawing::Point(48, 224);
			this->Dev2PortB_I6->Name = L"Dev2PortB_I6";
			this->Dev2PortB_I6->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortB_I6->Size = System::Drawing::Size(16, 17);
			this->Dev2PortB_I6->TabIndex = 19;
			this->Dev2PortB_I6->TabStop = false;
			// 
			// Dev1PortA_GB
			// 
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_I7);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_I6);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_I5);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_I4);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_I3);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_I2);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_I1);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_I0);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_O7);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_O6);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_O5);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_O4);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_O3);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_O2);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_O1);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortA_O0);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortASelOut);
			this->Dev1PortA_GB->Controls->Add(this->Dev1PortASelIn);
			this->Dev1PortA_GB->Location = System::Drawing::Point(13, 19);
			this->Dev1PortA_GB->Name = L"Dev1PortA_GB";
			this->Dev1PortA_GB->Size = System::Drawing::Size(79, 283);
			this->Dev1PortA_GB->TabIndex = 0;
			this->Dev1PortA_GB->TabStop = false;
			this->Dev1PortA_GB->Text = L"PortA";
			// 
			// Dev2PortB_I5
			// 
			this->Dev2PortB_I5->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortB_I5->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortB_I5->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I5.ErrorImage")));
			this->Dev2PortB_I5->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortB_I5->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I5.Image")));
			this->Dev2PortB_I5->Location = System::Drawing::Point(48, 201);
			this->Dev2PortB_I5->Name = L"Dev2PortB_I5";
			this->Dev2PortB_I5->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortB_I5->Size = System::Drawing::Size(16, 17);
			this->Dev2PortB_I5->TabIndex = 18;
			this->Dev2PortB_I5->TabStop = false;
			// 
			// Dev2PortB_I4
			// 
			this->Dev2PortB_I4->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortB_I4->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortB_I4->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I4.ErrorImage")));
			this->Dev2PortB_I4->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortB_I4->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I4.Image")));
			this->Dev2PortB_I4->Location = System::Drawing::Point(48, 176);
			this->Dev2PortB_I4->Name = L"Dev2PortB_I4";
			this->Dev2PortB_I4->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortB_I4->Size = System::Drawing::Size(16, 17);
			this->Dev2PortB_I4->TabIndex = 17;
			this->Dev2PortB_I4->TabStop = false;
			// 
			// Dev2PortB_I3
			// 
			this->Dev2PortB_I3->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortB_I3->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortB_I3->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I3.ErrorImage")));
			this->Dev2PortB_I3->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortB_I3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I3.Image")));
			this->Dev2PortB_I3->Location = System::Drawing::Point(48, 154);
			this->Dev2PortB_I3->Name = L"Dev2PortB_I3";
			this->Dev2PortB_I3->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortB_I3->Size = System::Drawing::Size(16, 17);
			this->Dev2PortB_I3->TabIndex = 16;
			this->Dev2PortB_I3->TabStop = false;
			// 
			// Dev2PortB_I2
			// 
			this->Dev2PortB_I2->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortB_I2->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortB_I2->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I2.ErrorImage")));
			this->Dev2PortB_I2->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortB_I2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I2.Image")));
			this->Dev2PortB_I2->Location = System::Drawing::Point(48, 130);
			this->Dev2PortB_I2->Name = L"Dev2PortB_I2";
			this->Dev2PortB_I2->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortB_I2->Size = System::Drawing::Size(16, 17);
			this->Dev2PortB_I2->TabIndex = 15;
			this->Dev2PortB_I2->TabStop = false;
			// 
			// Dev2PortB_I0
			// 
			this->Dev2PortB_I0->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortB_I0->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortB_I0->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I0.ErrorImage")));
			this->Dev2PortB_I0->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortB_I0->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I0.Image")));
			this->Dev2PortB_I0->Location = System::Drawing::Point(48, 82);
			this->Dev2PortB_I0->Name = L"Dev2PortB_I0";
			this->Dev2PortB_I0->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortB_I0->Size = System::Drawing::Size(16, 17);
			this->Dev2PortB_I0->TabIndex = 13;
			this->Dev2PortB_I0->TabStop = false;
			// 
			// Device1_GB
			// 
			this->Device1_GB->Controls->Add(this->Dev1SetBtn);
			this->Device1_GB->Controls->Add(this->Dev1PortCH_GB);
			this->Device1_GB->Controls->Add(this->Dev1PortCL_GB);
			this->Device1_GB->Controls->Add(this->Dev1PortB_GB);
			this->Device1_GB->Controls->Add(this->Dev1PortA_GB);
			this->Device1_GB->Location = System::Drawing::Point(8, 19);
			this->Device1_GB->Name = L"Device1_GB";
			this->Device1_GB->Size = System::Drawing::Size(365, 312);
			this->Device1_GB->TabIndex = 29;
			this->Device1_GB->TabStop = false;
			this->Device1_GB->Text = L"Device 1";
			// 
			// Dev1PortCL_GB
			// 
			this->Dev1PortCL_GB->Controls->Add(this->Dev1PortC_I3);
			this->Dev1PortCL_GB->Controls->Add(this->Dev1PortC_I2);
			this->Dev1PortCL_GB->Controls->Add(this->Dev1PortC_I1);
			this->Dev1PortCL_GB->Controls->Add(this->Dev1PortC_I0);
			this->Dev1PortCL_GB->Controls->Add(this->Dev1PortC_O3);
			this->Dev1PortCL_GB->Controls->Add(this->Dev1PortC_O2);
			this->Dev1PortCL_GB->Controls->Add(this->Dev1PortC_O1);
			this->Dev1PortCL_GB->Controls->Add(this->Dev1PortC_O0);
			this->Dev1PortCL_GB->Controls->Add(this->Dev1PortCLSelOut);
			this->Dev1PortCL_GB->Controls->Add(this->Dev1PortCLSelIn);
			this->Dev1PortCL_GB->Location = System::Drawing::Point(183, 66);
			this->Dev1PortCL_GB->Name = L"Dev1PortCL_GB";
			this->Dev1PortCL_GB->Size = System::Drawing::Size(79, 188);
			this->Dev1PortCL_GB->TabIndex = 2;
			this->Dev1PortCL_GB->TabStop = false;
			this->Dev1PortCL_GB->Text = L"PortC_Low";
			// 
			// Dev1PortB_GB
			// 
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_I7);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_I6);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_I5);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_I4);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_I3);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_I2);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_I1);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_I0);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_O7);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_O6);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_O5);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_O4);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_O3);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_O2);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_O1);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortB_O0);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortBSelOut);
			this->Dev1PortB_GB->Controls->Add(this->Dev1PortBSelIn);
			this->Dev1PortB_GB->Location = System::Drawing::Point(98, 19);
			this->Dev1PortB_GB->Name = L"Dev1PortB_GB";
			this->Dev1PortB_GB->Size = System::Drawing::Size(79, 283);
			this->Dev1PortB_GB->TabIndex = 1;
			this->Dev1PortB_GB->TabStop = false;
			this->Dev1PortB_GB->Text = L"PortB";
			// 
			// Dev2PortB_I1
			// 
			this->Dev2PortB_I1->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortB_I1->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortB_I1->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I1.ErrorImage")));
			this->Dev2PortB_I1->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortB_I1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortB_I1.Image")));
			this->Dev2PortB_I1->Location = System::Drawing::Point(48, 106);
			this->Dev2PortB_I1->Name = L"Dev2PortB_I1";
			this->Dev2PortB_I1->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortB_I1->Size = System::Drawing::Size(16, 17);
			this->Dev2PortB_I1->TabIndex = 14;
			this->Dev2PortB_I1->TabStop = false;
			// 
			// Dev2PortB_O7
			// 
			this->Dev2PortB_O7->AutoSize = true;
			this->Dev2PortB_O7->Location = System::Drawing::Point(16, 248);
			this->Dev2PortB_O7->Name = L"Dev2PortB_O7";
			this->Dev2PortB_O7->Size = System::Drawing::Size(32, 17);
			this->Dev2PortB_O7->TabIndex = 9;
			this->Dev2PortB_O7->Text = L"7";
			this->Dev2PortB_O7->UseVisualStyleBackColor = true;
			this->Dev2PortB_O7->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortB_O_Changed);
			// 
			// Dev2PortB_O6
			// 
			this->Dev2PortB_O6->AutoSize = true;
			this->Dev2PortB_O6->Location = System::Drawing::Point(16, 224);
			this->Dev2PortB_O6->Name = L"Dev2PortB_O6";
			this->Dev2PortB_O6->Size = System::Drawing::Size(32, 17);
			this->Dev2PortB_O6->TabIndex = 8;
			this->Dev2PortB_O6->Text = L"6";
			this->Dev2PortB_O6->UseVisualStyleBackColor = true;
			this->Dev2PortB_O6->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortB_O_Changed);
			// 
			// Dev2PortB_O5
			// 
			this->Dev2PortB_O5->AutoSize = true;
			this->Dev2PortB_O5->Location = System::Drawing::Point(16, 201);
			this->Dev2PortB_O5->Name = L"Dev2PortB_O5";
			this->Dev2PortB_O5->Size = System::Drawing::Size(32, 17);
			this->Dev2PortB_O5->TabIndex = 7;
			this->Dev2PortB_O5->Text = L"5";
			this->Dev2PortB_O5->UseVisualStyleBackColor = true;
			this->Dev2PortB_O5->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortB_O_Changed);
			// 
			// Dev2PortB_O4
			// 
			this->Dev2PortB_O4->AutoSize = true;
			this->Dev2PortB_O4->Location = System::Drawing::Point(16, 176);
			this->Dev2PortB_O4->Name = L"Dev2PortB_O4";
			this->Dev2PortB_O4->Size = System::Drawing::Size(32, 17);
			this->Dev2PortB_O4->TabIndex = 6;
			this->Dev2PortB_O4->Text = L"4";
			this->Dev2PortB_O4->UseVisualStyleBackColor = true;
			this->Dev2PortB_O4->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortB_O_Changed);
			// 
			// Dev2PortB_O3
			// 
			this->Dev2PortB_O3->AutoSize = true;
			this->Dev2PortB_O3->Location = System::Drawing::Point(16, 153);
			this->Dev2PortB_O3->Name = L"Dev2PortB_O3";
			this->Dev2PortB_O3->Size = System::Drawing::Size(32, 17);
			this->Dev2PortB_O3->TabIndex = 5;
			this->Dev2PortB_O3->Text = L"3";
			this->Dev2PortB_O3->UseVisualStyleBackColor = true;
			this->Dev2PortB_O3->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortB_O_Changed);
			// 
			// Dev2PortB_O2
			// 
			this->Dev2PortB_O2->AutoSize = true;
			this->Dev2PortB_O2->Location = System::Drawing::Point(16, 129);
			this->Dev2PortB_O2->Name = L"Dev2PortB_O2";
			this->Dev2PortB_O2->Size = System::Drawing::Size(32, 17);
			this->Dev2PortB_O2->TabIndex = 4;
			this->Dev2PortB_O2->Text = L"2";
			this->Dev2PortB_O2->UseVisualStyleBackColor = true;
			this->Dev2PortB_O2->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortB_O_Changed);
			// 
			// Dev2PortB_O1
			// 
			this->Dev2PortB_O1->AutoSize = true;
			this->Dev2PortB_O1->Location = System::Drawing::Point(16, 105);
			this->Dev2PortB_O1->Name = L"Dev2PortB_O1";
			this->Dev2PortB_O1->Size = System::Drawing::Size(32, 17);
			this->Dev2PortB_O1->TabIndex = 3;
			this->Dev2PortB_O1->Text = L"1";
			this->Dev2PortB_O1->UseVisualStyleBackColor = true;
			this->Dev2PortB_O1->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortB_O_Changed);
			// 
			// Dev2PortB_O0
			// 
			this->Dev2PortB_O0->AutoSize = true;
			this->Dev2PortB_O0->Location = System::Drawing::Point(16, 82);
			this->Dev2PortB_O0->Name = L"Dev2PortB_O0";
			this->Dev2PortB_O0->Size = System::Drawing::Size(32, 17);
			this->Dev2PortB_O0->TabIndex = 2;
			this->Dev2PortB_O0->Text = L"0";
			this->Dev2PortB_O0->UseVisualStyleBackColor = true;
			this->Dev2PortB_O0->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortB_O_Changed);
			// 
			// Dev2PortB_GB
			// 
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_I7);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_I6);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_I5);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_I4);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_I3);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_I2);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_I1);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_I0);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_O7);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_O6);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_O5);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_O4);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_O3);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_O2);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_O1);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortB_O0);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortBSelOut);
			this->Dev2PortB_GB->Controls->Add(this->Dev2PortBSelIn);
			this->Dev2PortB_GB->Location = System::Drawing::Point(98, 19);
			this->Dev2PortB_GB->Name = L"Dev2PortB_GB";
			this->Dev2PortB_GB->Size = System::Drawing::Size(79, 283);
			this->Dev2PortB_GB->TabIndex = 1;
			this->Dev2PortB_GB->TabStop = false;
			this->Dev2PortB_GB->Text = L"PortB";
			// 
			// Dev2PortBSelOut
			// 
			this->Dev2PortBSelOut->AutoSize = true;
			this->Dev2PortBSelOut->Location = System::Drawing::Point(16, 46);
			this->Dev2PortBSelOut->Name = L"Dev2PortBSelOut";
			this->Dev2PortBSelOut->Size = System::Drawing::Size(57, 17);
			this->Dev2PortBSelOut->TabIndex = 1;
			this->Dev2PortBSelOut->TabStop = true;
			this->Dev2PortBSelOut->Text = L"Output";
			this->Dev2PortBSelOut->UseVisualStyleBackColor = true;
			// 
			// Dev2PortBSelIn
			// 
			this->Dev2PortBSelIn->AutoSize = true;
			this->Dev2PortBSelIn->Location = System::Drawing::Point(16, 23);
			this->Dev2PortBSelIn->Name = L"Dev2PortBSelIn";
			this->Dev2PortBSelIn->Size = System::Drawing::Size(49, 17);
			this->Dev2PortBSelIn->TabIndex = 0;
			this->Dev2PortBSelIn->TabStop = true;
			this->Dev2PortBSelIn->Text = L"Input";
			this->Dev2PortBSelIn->UseVisualStyleBackColor = true;
			// 
			// Dev2PortC_I7
			// 
			this->Dev2PortC_I7->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortC_I7->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortC_I7->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I7.ErrorImage")));
			this->Dev2PortC_I7->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortC_I7->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I7.Image")));
			this->Dev2PortC_I7->Location = System::Drawing::Point(48, 154);
			this->Dev2PortC_I7->Name = L"Dev2PortC_I7";
			this->Dev2PortC_I7->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortC_I7->Size = System::Drawing::Size(16, 17);
			this->Dev2PortC_I7->TabIndex = 16;
			this->Dev2PortC_I7->TabStop = false;
			// 
			// Dev2PortC_O1
			// 
			this->Dev2PortC_O1->AutoSize = true;
			this->Dev2PortC_O1->Location = System::Drawing::Point(16, 105);
			this->Dev2PortC_O1->Name = L"Dev2PortC_O1";
			this->Dev2PortC_O1->Size = System::Drawing::Size(32, 17);
			this->Dev2PortC_O1->TabIndex = 3;
			this->Dev2PortC_O1->Text = L"1";
			this->Dev2PortC_O1->UseVisualStyleBackColor = true;
			this->Dev2PortC_O1->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortC_O_Changed);
			// 
			// Dev2PortC_O0
			// 
			this->Dev2PortC_O0->AutoSize = true;
			this->Dev2PortC_O0->Location = System::Drawing::Point(16, 82);
			this->Dev2PortC_O0->Name = L"Dev2PortC_O0";
			this->Dev2PortC_O0->Size = System::Drawing::Size(32, 17);
			this->Dev2PortC_O0->TabIndex = 2;
			this->Dev2PortC_O0->Text = L"0";
			this->Dev2PortC_O0->UseVisualStyleBackColor = true;
			this->Dev2PortC_O0->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortC_O_Changed);
			// 
			// Dev2PortC_I6
			// 
			this->Dev2PortC_I6->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortC_I6->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortC_I6->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I6.ErrorImage")));
			this->Dev2PortC_I6->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortC_I6->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I6.Image")));
			this->Dev2PortC_I6->Location = System::Drawing::Point(48, 130);
			this->Dev2PortC_I6->Name = L"Dev2PortC_I6";
			this->Dev2PortC_I6->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortC_I6->Size = System::Drawing::Size(16, 17);
			this->Dev2PortC_I6->TabIndex = 15;
			this->Dev2PortC_I6->TabStop = false;
			// 
			// Dev2PortCLSelOut
			// 
			this->Dev2PortCLSelOut->AutoSize = true;
			this->Dev2PortCLSelOut->Location = System::Drawing::Point(16, 46);
			this->Dev2PortCLSelOut->Name = L"Dev2PortCLSelOut";
			this->Dev2PortCLSelOut->Size = System::Drawing::Size(57, 17);
			this->Dev2PortCLSelOut->TabIndex = 1;
			this->Dev2PortCLSelOut->TabStop = true;
			this->Dev2PortCLSelOut->Text = L"Output";
			this->Dev2PortCLSelOut->UseVisualStyleBackColor = true;
			// 
			// Dev2PortC_I5
			// 
			this->Dev2PortC_I5->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortC_I5->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortC_I5->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I5.ErrorImage")));
			this->Dev2PortC_I5->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortC_I5->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I5.Image")));
			this->Dev2PortC_I5->Location = System::Drawing::Point(48, 106);
			this->Dev2PortC_I5->Name = L"Dev2PortC_I5";
			this->Dev2PortC_I5->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortC_I5->Size = System::Drawing::Size(16, 17);
			this->Dev2PortC_I5->TabIndex = 14;
			this->Dev2PortC_I5->TabStop = false;
			// 
			// Dev2PortC_I4
			// 
			this->Dev2PortC_I4->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortC_I4->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortC_I4->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I4.ErrorImage")));
			this->Dev2PortC_I4->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortC_I4->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I4.Image")));
			this->Dev2PortC_I4->Location = System::Drawing::Point(48, 82);
			this->Dev2PortC_I4->Name = L"Dev2PortC_I4";
			this->Dev2PortC_I4->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortC_I4->Size = System::Drawing::Size(16, 17);
			this->Dev2PortC_I4->TabIndex = 13;
			this->Dev2PortC_I4->TabStop = false;
			// 
			// Dev2PortCLSelIn
			// 
			this->Dev2PortCLSelIn->AutoSize = true;
			this->Dev2PortCLSelIn->Location = System::Drawing::Point(16, 23);
			this->Dev2PortCLSelIn->Name = L"Dev2PortCLSelIn";
			this->Dev2PortCLSelIn->Size = System::Drawing::Size(49, 17);
			this->Dev2PortCLSelIn->TabIndex = 0;
			this->Dev2PortCLSelIn->TabStop = true;
			this->Dev2PortCLSelIn->Text = L"Input";
			this->Dev2PortCLSelIn->UseVisualStyleBackColor = true;
			// 
			// Dev2PortC_O7
			// 
			this->Dev2PortC_O7->AutoSize = true;
			this->Dev2PortC_O7->Location = System::Drawing::Point(16, 153);
			this->Dev2PortC_O7->Name = L"Dev2PortC_O7";
			this->Dev2PortC_O7->Size = System::Drawing::Size(32, 17);
			this->Dev2PortC_O7->TabIndex = 5;
			this->Dev2PortC_O7->Text = L"7";
			this->Dev2PortC_O7->UseVisualStyleBackColor = true;
			this->Dev2PortC_O7->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortC_O_Changed);
			// 
			// Dev2PortC_O6
			// 
			this->Dev2PortC_O6->AutoSize = true;
			this->Dev2PortC_O6->Location = System::Drawing::Point(16, 129);
			this->Dev2PortC_O6->Name = L"Dev2PortC_O6";
			this->Dev2PortC_O6->Size = System::Drawing::Size(32, 17);
			this->Dev2PortC_O6->TabIndex = 4;
			this->Dev2PortC_O6->Text = L"6";
			this->Dev2PortC_O6->UseVisualStyleBackColor = true;
			this->Dev2PortC_O6->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortC_O_Changed);
			// 
			// Dev2PortC_O5
			// 
			this->Dev2PortC_O5->AutoSize = true;
			this->Dev2PortC_O5->Location = System::Drawing::Point(16, 105);
			this->Dev2PortC_O5->Name = L"Dev2PortC_O5";
			this->Dev2PortC_O5->Size = System::Drawing::Size(32, 17);
			this->Dev2PortC_O5->TabIndex = 3;
			this->Dev2PortC_O5->Text = L"5";
			this->Dev2PortC_O5->UseVisualStyleBackColor = true;
			this->Dev2PortC_O5->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortC_O_Changed);
			// 
			// Dev2PortC_O4
			// 
			this->Dev2PortC_O4->AutoSize = true;
			this->Dev2PortC_O4->Location = System::Drawing::Point(16, 82);
			this->Dev2PortC_O4->Name = L"Dev2PortC_O4";
			this->Dev2PortC_O4->Size = System::Drawing::Size(32, 17);
			this->Dev2PortC_O4->TabIndex = 2;
			this->Dev2PortC_O4->Text = L"4";
			this->Dev2PortC_O4->UseVisualStyleBackColor = true;
			this->Dev2PortC_O4->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortC_O_Changed);
			// 
			// Dev2PortC_O2
			// 
			this->Dev2PortC_O2->AutoSize = true;
			this->Dev2PortC_O2->Location = System::Drawing::Point(16, 129);
			this->Dev2PortC_O2->Name = L"Dev2PortC_O2";
			this->Dev2PortC_O2->Size = System::Drawing::Size(32, 17);
			this->Dev2PortC_O2->TabIndex = 4;
			this->Dev2PortC_O2->Text = L"2";
			this->Dev2PortC_O2->UseVisualStyleBackColor = true;
			this->Dev2PortC_O2->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortC_O_Changed);
			// 
			// Dev2PortC_I2
			// 
			this->Dev2PortC_I2->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortC_I2->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortC_I2->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I2.ErrorImage")));
			this->Dev2PortC_I2->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortC_I2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I2.Image")));
			this->Dev2PortC_I2->Location = System::Drawing::Point(48, 130);
			this->Dev2PortC_I2->Name = L"Dev2PortC_I2";
			this->Dev2PortC_I2->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortC_I2->Size = System::Drawing::Size(16, 17);
			this->Dev2PortC_I2->TabIndex = 15;
			this->Dev2PortC_I2->TabStop = false;
			// 
			// Dev2PortA_GB
			// 
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_I7);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_I6);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_I5);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_I4);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_I3);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_I2);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_I1);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_I0);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_O7);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_O6);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_O5);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_O4);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_O3);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_O2);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_O1);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortA_O0);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortASelOut);
			this->Dev2PortA_GB->Controls->Add(this->Dev2PortASelIn);
			this->Dev2PortA_GB->Location = System::Drawing::Point(13, 19);
			this->Dev2PortA_GB->Name = L"Dev2PortA_GB";
			this->Dev2PortA_GB->Size = System::Drawing::Size(79, 283);
			this->Dev2PortA_GB->TabIndex = 0;
			this->Dev2PortA_GB->TabStop = false;
			this->Dev2PortA_GB->Text = L"PortA";
			// 
			// Dev2PortA_I7
			// 
			this->Dev2PortA_I7->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortA_I7->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortA_I7->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I7.ErrorImage")));
			this->Dev2PortA_I7->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortA_I7->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I7.Image")));
			this->Dev2PortA_I7->Location = System::Drawing::Point(48, 248);
			this->Dev2PortA_I7->Name = L"Dev2PortA_I7";
			this->Dev2PortA_I7->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortA_I7->Size = System::Drawing::Size(16, 17);
			this->Dev2PortA_I7->TabIndex = 20;
			this->Dev2PortA_I7->TabStop = false;
			// 
			// Dev2PortA_I6
			// 
			this->Dev2PortA_I6->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortA_I6->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortA_I6->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I6.ErrorImage")));
			this->Dev2PortA_I6->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortA_I6->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I6.Image")));
			this->Dev2PortA_I6->Location = System::Drawing::Point(48, 224);
			this->Dev2PortA_I6->Name = L"Dev2PortA_I6";
			this->Dev2PortA_I6->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortA_I6->Size = System::Drawing::Size(16, 17);
			this->Dev2PortA_I6->TabIndex = 19;
			this->Dev2PortA_I6->TabStop = false;
			// 
			// Dev2PortA_I5
			// 
			this->Dev2PortA_I5->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortA_I5->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortA_I5->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I5.ErrorImage")));
			this->Dev2PortA_I5->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortA_I5->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I5.Image")));
			this->Dev2PortA_I5->Location = System::Drawing::Point(48, 201);
			this->Dev2PortA_I5->Name = L"Dev2PortA_I5";
			this->Dev2PortA_I5->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortA_I5->Size = System::Drawing::Size(16, 17);
			this->Dev2PortA_I5->TabIndex = 18;
			this->Dev2PortA_I5->TabStop = false;
			// 
			// Dev2PortA_I4
			// 
			this->Dev2PortA_I4->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortA_I4->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortA_I4->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I4.ErrorImage")));
			this->Dev2PortA_I4->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortA_I4->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I4.Image")));
			this->Dev2PortA_I4->Location = System::Drawing::Point(48, 176);
			this->Dev2PortA_I4->Name = L"Dev2PortA_I4";
			this->Dev2PortA_I4->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortA_I4->Size = System::Drawing::Size(16, 17);
			this->Dev2PortA_I4->TabIndex = 17;
			this->Dev2PortA_I4->TabStop = false;
			// 
			// Dev2PortA_I3
			// 
			this->Dev2PortA_I3->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortA_I3->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortA_I3->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I3.ErrorImage")));
			this->Dev2PortA_I3->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortA_I3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I3.Image")));
			this->Dev2PortA_I3->Location = System::Drawing::Point(48, 154);
			this->Dev2PortA_I3->Name = L"Dev2PortA_I3";
			this->Dev2PortA_I3->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortA_I3->Size = System::Drawing::Size(16, 17);
			this->Dev2PortA_I3->TabIndex = 16;
			this->Dev2PortA_I3->TabStop = false;
			// 
			// Dev2PortA_I2
			// 
			this->Dev2PortA_I2->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortA_I2->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortA_I2->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I2.ErrorImage")));
			this->Dev2PortA_I2->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortA_I2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I2.Image")));
			this->Dev2PortA_I2->Location = System::Drawing::Point(48, 130);
			this->Dev2PortA_I2->Name = L"Dev2PortA_I2";
			this->Dev2PortA_I2->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortA_I2->Size = System::Drawing::Size(16, 17);
			this->Dev2PortA_I2->TabIndex = 15;
			this->Dev2PortA_I2->TabStop = false;
			// 
			// Dev2PortA_I1
			// 
			this->Dev2PortA_I1->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortA_I1->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortA_I1->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I1.ErrorImage")));
			this->Dev2PortA_I1->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortA_I1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I1.Image")));
			this->Dev2PortA_I1->Location = System::Drawing::Point(48, 106);
			this->Dev2PortA_I1->Name = L"Dev2PortA_I1";
			this->Dev2PortA_I1->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortA_I1->Size = System::Drawing::Size(16, 17);
			this->Dev2PortA_I1->TabIndex = 14;
			this->Dev2PortA_I1->TabStop = false;
			// 
			// Dev2PortA_I0
			// 
			this->Dev2PortA_I0->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortA_I0->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortA_I0->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I0.ErrorImage")));
			this->Dev2PortA_I0->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortA_I0->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortA_I0.Image")));
			this->Dev2PortA_I0->Location = System::Drawing::Point(48, 82);
			this->Dev2PortA_I0->Name = L"Dev2PortA_I0";
			this->Dev2PortA_I0->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortA_I0->Size = System::Drawing::Size(16, 17);
			this->Dev2PortA_I0->TabIndex = 13;
			this->Dev2PortA_I0->TabStop = false;
			// 
			// Dev2PortA_O7
			// 
			this->Dev2PortA_O7->AutoSize = true;
			this->Dev2PortA_O7->Location = System::Drawing::Point(16, 248);
			this->Dev2PortA_O7->Name = L"Dev2PortA_O7";
			this->Dev2PortA_O7->Size = System::Drawing::Size(32, 17);
			this->Dev2PortA_O7->TabIndex = 9;
			this->Dev2PortA_O7->Text = L"7";
			this->Dev2PortA_O7->UseVisualStyleBackColor = true;
			this->Dev2PortA_O7->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortA_O_Changed);
			// 
			// Dev2PortA_O6
			// 
			this->Dev2PortA_O6->AutoSize = true;
			this->Dev2PortA_O6->Location = System::Drawing::Point(16, 224);
			this->Dev2PortA_O6->Name = L"Dev2PortA_O6";
			this->Dev2PortA_O6->Size = System::Drawing::Size(32, 17);
			this->Dev2PortA_O6->TabIndex = 8;
			this->Dev2PortA_O6->Text = L"6";
			this->Dev2PortA_O6->UseVisualStyleBackColor = true;
			this->Dev2PortA_O6->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortA_O_Changed);
			// 
			// Dev2PortA_O5
			// 
			this->Dev2PortA_O5->AutoSize = true;
			this->Dev2PortA_O5->Location = System::Drawing::Point(16, 201);
			this->Dev2PortA_O5->Name = L"Dev2PortA_O5";
			this->Dev2PortA_O5->Size = System::Drawing::Size(32, 17);
			this->Dev2PortA_O5->TabIndex = 7;
			this->Dev2PortA_O5->Text = L"5";
			this->Dev2PortA_O5->UseVisualStyleBackColor = true;
			this->Dev2PortA_O5->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortA_O_Changed);
			// 
			// Dev2PortA_O4
			// 
			this->Dev2PortA_O4->AutoSize = true;
			this->Dev2PortA_O4->Location = System::Drawing::Point(16, 176);
			this->Dev2PortA_O4->Name = L"Dev2PortA_O4";
			this->Dev2PortA_O4->Size = System::Drawing::Size(32, 17);
			this->Dev2PortA_O4->TabIndex = 6;
			this->Dev2PortA_O4->Text = L"4";
			this->Dev2PortA_O4->UseVisualStyleBackColor = true;
			this->Dev2PortA_O4->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortA_O_Changed);
			// 
			// Dev2PortA_O3
			// 
			this->Dev2PortA_O3->AutoSize = true;
			this->Dev2PortA_O3->Location = System::Drawing::Point(16, 153);
			this->Dev2PortA_O3->Name = L"Dev2PortA_O3";
			this->Dev2PortA_O3->Size = System::Drawing::Size(32, 17);
			this->Dev2PortA_O3->TabIndex = 5;
			this->Dev2PortA_O3->Text = L"3";
			this->Dev2PortA_O3->UseVisualStyleBackColor = true;
			this->Dev2PortA_O3->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortA_O_Changed);
			// 
			// Dev2PortA_O2
			// 
			this->Dev2PortA_O2->AutoSize = true;
			this->Dev2PortA_O2->Location = System::Drawing::Point(16, 129);
			this->Dev2PortA_O2->Name = L"Dev2PortA_O2";
			this->Dev2PortA_O2->Size = System::Drawing::Size(32, 17);
			this->Dev2PortA_O2->TabIndex = 4;
			this->Dev2PortA_O2->Text = L"2";
			this->Dev2PortA_O2->UseVisualStyleBackColor = true;
			this->Dev2PortA_O2->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortA_O_Changed);
			// 
			// Dev2PortA_O1
			// 
			this->Dev2PortA_O1->AutoSize = true;
			this->Dev2PortA_O1->Location = System::Drawing::Point(16, 105);
			this->Dev2PortA_O1->Name = L"Dev2PortA_O1";
			this->Dev2PortA_O1->Size = System::Drawing::Size(32, 17);
			this->Dev2PortA_O1->TabIndex = 3;
			this->Dev2PortA_O1->Text = L"1";
			this->Dev2PortA_O1->UseVisualStyleBackColor = true;
			this->Dev2PortA_O1->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortA_O_Changed);
			// 
			// Dev2PortA_O0
			// 
			this->Dev2PortA_O0->AutoSize = true;
			this->Dev2PortA_O0->Location = System::Drawing::Point(16, 82);
			this->Dev2PortA_O0->Name = L"Dev2PortA_O0";
			this->Dev2PortA_O0->Size = System::Drawing::Size(32, 17);
			this->Dev2PortA_O0->TabIndex = 2;
			this->Dev2PortA_O0->Text = L"0";
			this->Dev2PortA_O0->UseVisualStyleBackColor = true;
			this->Dev2PortA_O0->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortA_O_Changed);
			// 
			// Dev2PortASelIn
			// 
			this->Dev2PortASelIn->AutoSize = true;
			this->Dev2PortASelIn->Location = System::Drawing::Point(16, 23);
			this->Dev2PortASelIn->Name = L"Dev2PortASelIn";
			this->Dev2PortASelIn->Size = System::Drawing::Size(49, 17);
			this->Dev2PortASelIn->TabIndex = 0;
			this->Dev2PortASelIn->TabStop = true;
			this->Dev2PortASelIn->Text = L"Input";
			this->Dev2PortASelIn->UseVisualStyleBackColor = true;
			// 
			// Dev2PortC_I1
			// 
			this->Dev2PortC_I1->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortC_I1->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortC_I1->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I1.ErrorImage")));
			this->Dev2PortC_I1->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortC_I1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I1.Image")));
			this->Dev2PortC_I1->Location = System::Drawing::Point(48, 106);
			this->Dev2PortC_I1->Name = L"Dev2PortC_I1";
			this->Dev2PortC_I1->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortC_I1->Size = System::Drawing::Size(16, 17);
			this->Dev2PortC_I1->TabIndex = 14;
			this->Dev2PortC_I1->TabStop = false;
			// 
			// Dev2PortC_I3
			// 
			this->Dev2PortC_I3->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortC_I3->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortC_I3->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I3.ErrorImage")));
			this->Dev2PortC_I3->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortC_I3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I3.Image")));
			this->Dev2PortC_I3->Location = System::Drawing::Point(48, 154);
			this->Dev2PortC_I3->Name = L"Dev2PortC_I3";
			this->Dev2PortC_I3->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortC_I3->Size = System::Drawing::Size(16, 17);
			this->Dev2PortC_I3->TabIndex = 16;
			this->Dev2PortC_I3->TabStop = false;
			// 
			// Dev2PortCL_GB
			// 
			this->Dev2PortCL_GB->Controls->Add(this->Dev2PortC_I3);
			this->Dev2PortCL_GB->Controls->Add(this->Dev2PortC_I2);
			this->Dev2PortCL_GB->Controls->Add(this->Dev2PortC_I1);
			this->Dev2PortCL_GB->Controls->Add(this->Dev2PortC_I0);
			this->Dev2PortCL_GB->Controls->Add(this->Dev2PortC_O3);
			this->Dev2PortCL_GB->Controls->Add(this->Dev2PortC_O2);
			this->Dev2PortCL_GB->Controls->Add(this->Dev2PortC_O1);
			this->Dev2PortCL_GB->Controls->Add(this->Dev2PortC_O0);
			this->Dev2PortCL_GB->Controls->Add(this->Dev2PortCLSelOut);
			this->Dev2PortCL_GB->Controls->Add(this->Dev2PortCLSelIn);
			this->Dev2PortCL_GB->Location = System::Drawing::Point(183, 66);
			this->Dev2PortCL_GB->Name = L"Dev2PortCL_GB";
			this->Dev2PortCL_GB->Size = System::Drawing::Size(79, 188);
			this->Dev2PortCL_GB->TabIndex = 2;
			this->Dev2PortCL_GB->TabStop = false;
			this->Dev2PortCL_GB->Text = L"PortC_Low";
			// 
			// Dev2PortC_I0
			// 
			this->Dev2PortC_I0->BackColor = System::Drawing::SystemColors::Control;
			this->Dev2PortC_I0->Cursor = System::Windows::Forms::Cursors::Default;
			this->Dev2PortC_I0->ErrorImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I0.ErrorImage")));
			this->Dev2PortC_I0->ForeColor = System::Drawing::SystemColors::ControlText;
			this->Dev2PortC_I0->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Dev2PortC_I0.Image")));
			this->Dev2PortC_I0->Location = System::Drawing::Point(48, 82);
			this->Dev2PortC_I0->Name = L"Dev2PortC_I0";
			this->Dev2PortC_I0->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Dev2PortC_I0->Size = System::Drawing::Size(16, 17);
			this->Dev2PortC_I0->TabIndex = 13;
			this->Dev2PortC_I0->TabStop = false;
			// 
			// Dev2PortC_O3
			// 
			this->Dev2PortC_O3->AutoSize = true;
			this->Dev2PortC_O3->Location = System::Drawing::Point(16, 153);
			this->Dev2PortC_O3->Name = L"Dev2PortC_O3";
			this->Dev2PortC_O3->Size = System::Drawing::Size(32, 17);
			this->Dev2PortC_O3->TabIndex = 5;
			this->Dev2PortC_O3->Text = L"3";
			this->Dev2PortC_O3->UseVisualStyleBackColor = true;
			this->Dev2PortC_O3->CheckedChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2PortC_O_Changed);
			// 
			// Dev2PortCHSelOut
			// 
			this->Dev2PortCHSelOut->AutoSize = true;
			this->Dev2PortCHSelOut->Location = System::Drawing::Point(16, 46);
			this->Dev2PortCHSelOut->Name = L"Dev2PortCHSelOut";
			this->Dev2PortCHSelOut->Size = System::Drawing::Size(57, 17);
			this->Dev2PortCHSelOut->TabIndex = 1;
			this->Dev2PortCHSelOut->TabStop = true;
			this->Dev2PortCHSelOut->Text = L"Output";
			this->Dev2PortCHSelOut->UseVisualStyleBackColor = true;
			// 
			// Dev2PortCHSelIn
			// 
			this->Dev2PortCHSelIn->AutoSize = true;
			this->Dev2PortCHSelIn->Location = System::Drawing::Point(16, 23);
			this->Dev2PortCHSelIn->Name = L"Dev2PortCHSelIn";
			this->Dev2PortCHSelIn->Size = System::Drawing::Size(49, 17);
			this->Dev2PortCHSelIn->TabIndex = 0;
			this->Dev2PortCHSelIn->TabStop = true;
			this->Dev2PortCHSelIn->Text = L"Input";
			this->Dev2PortCHSelIn->UseVisualStyleBackColor = true;
			// 
			// Dev2PortCH_GB
			// 
			this->Dev2PortCH_GB->Controls->Add(this->Dev2PortC_I7);
			this->Dev2PortCH_GB->Controls->Add(this->Dev2PortC_I6);
			this->Dev2PortCH_GB->Controls->Add(this->Dev2PortC_I5);
			this->Dev2PortCH_GB->Controls->Add(this->Dev2PortC_I4);
			this->Dev2PortCH_GB->Controls->Add(this->Dev2PortC_O7);
			this->Dev2PortCH_GB->Controls->Add(this->Dev2PortC_O6);
			this->Dev2PortCH_GB->Controls->Add(this->Dev2PortC_O5);
			this->Dev2PortCH_GB->Controls->Add(this->Dev2PortC_O4);
			this->Dev2PortCH_GB->Controls->Add(this->Dev2PortCHSelOut);
			this->Dev2PortCH_GB->Controls->Add(this->Dev2PortCHSelIn);
			this->Dev2PortCH_GB->Location = System::Drawing::Point(268, 66);
			this->Dev2PortCH_GB->Name = L"Dev2PortCH_GB";
			this->Dev2PortCH_GB->Size = System::Drawing::Size(79, 188);
			this->Dev2PortCH_GB->TabIndex = 3;
			this->Dev2PortCH_GB->TabStop = false;
			this->Dev2PortCH_GB->Text = L"PortC_High";
			// 
			// Device2_GB
			// 
			this->Device2_GB->Controls->Add(this->Dev2SetBtn);
			this->Device2_GB->Controls->Add(this->Dev2PortCH_GB);
			this->Device2_GB->Controls->Add(this->Dev2PortCL_GB);
			this->Device2_GB->Controls->Add(this->Dev2PortB_GB);
			this->Device2_GB->Controls->Add(this->Dev2PortA_GB);
			this->Device2_GB->Location = System::Drawing::Point(379, 19);
			this->Device2_GB->Name = L"Device2_GB";
			this->Device2_GB->Size = System::Drawing::Size(365, 312);
			this->Device2_GB->TabIndex = 30;
			this->Device2_GB->TabStop = false;
			this->Device2_GB->Text = L"Device 2";
			// 
			// Dev2SetBtn
			// 
			this->Dev2SetBtn->Location = System::Drawing::Point(184, 26);
			this->Dev2SetBtn->Name = L"Dev2SetBtn";
			this->Dev2SetBtn->Size = System::Drawing::Size(163, 33);
			this->Dev2SetBtn->TabIndex = 18;
			this->Dev2SetBtn->Text = L"DirectionSet";
			this->Dev2SetBtn->UseVisualStyleBackColor = true;
			this->Dev2SetBtn->Click += gcnew System::EventHandler(this, &TUSBPIOControlGUI::Dev2SetBtn_Click);
			// 
			// IDNUM
			// 
			this->IDNUM->Items->AddRange(gcnew cli::array< System::Object^  >(16) {
				L"0", L"1", L"2", L"3", L"4", L"5", L"6", L"7", L"8",
					L"9", L"A", L"B", L"C", L"D", L"E", L"F"
			});
			this->IDNUM->Location = System::Drawing::Point(52, 354);
			this->IDNUM->Name = L"IDNUM";
			this->IDNUM->Size = System::Drawing::Size(48, 21);
			this->IDNUM->TabIndex = 26;
			this->IDNUM->Text = L"ComboBox1";
			this->IDNUM->SelectedIndexChanged += gcnew System::EventHandler(this, &TUSBPIOControlGUI::IDNUM_SelectedIndexChanged);
			// 
			// TUSBPIOControlGUI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(752, 394);
			this->Controls->Add(this->DevClose);
			this->Controls->Add(this->DevOpen);
			this->Controls->Add(this->Device1_GB);
			this->Controls->Add(this->Device2_GB);
			this->Controls->Add(this->IDNUM);
			this->Name = L"TUSBPIOControlGUI";
			this->Text = L"TUSB-PIO Sample Program";
			this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &TUSBPIOControlGUI::TUSBPIOControlGUI_FormClosed);
			this->Load += gcnew System::EventHandler(this, &TUSBPIOControlGUI::TUSBPIOControlGUI_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I0))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortB_I0))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I7))->EndInit();
			this->Dev1PortCH_GB->ResumeLayout(false);
			this->Dev1PortCH_GB->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortC_I4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev1PortA_I0))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I6))->EndInit();
			this->Dev1PortA_GB->ResumeLayout(false);
			this->Dev1PortA_GB->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I0))->EndInit();
			this->Device1_GB->ResumeLayout(false);
			this->Dev1PortCL_GB->ResumeLayout(false);
			this->Dev1PortCL_GB->PerformLayout();
			this->Dev1PortB_GB->ResumeLayout(false);
			this->Dev1PortB_GB->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortB_I1))->EndInit();
			this->Dev2PortB_GB->ResumeLayout(false);
			this->Dev2PortB_GB->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I2))->EndInit();
			this->Dev2PortA_GB->ResumeLayout(false);
			this->Dev2PortA_GB->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortA_I0))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I3))->EndInit();
			this->Dev2PortCL_GB->ResumeLayout(false);
			this->Dev2PortCL_GB->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Dev2PortC_I0))->EndInit();
			this->Dev2PortCH_GB->ResumeLayout(false);
			this->Dev2PortCH_GB->PerformLayout();
			this->Device2_GB->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion
	private: void Control_Enable(Boolean state)
	{
		DevClose->Enabled = state;
		DevOpen->Enabled = !state;
		IDNUM->Enabled = !state;
		Dev1SetBtn->Enabled = state;
		Dev2SetBtn->Enabled = state;
		ChangeLock = !state;
	}

	private: System::Void TUSBPIOControlGUI_Load(System::Object^ sender, System::EventArgs^ e) {
		// ***フォームの表示開始時の初期化処理***
		Control_Enable(false); //***
		IDNUM->SelectedIndex = 0; // '***初期選択IDを0にする
		Dev1PortASelIn->Checked = true;
		Dev1PortBSelIn->Checked = true;
		Dev1PortCLSelIn->Checked = true;
		Dev1PortCHSelIn->Checked = true;
		Dev2PortASelIn->Checked = true;
		Dev2PortBSelIn->Checked = true;
		Dev2PortCLSelIn->Checked = true;
		Dev2PortCHSelIn->Checked = true;
	}
	private: System::Void TUSBPIOControlGUI_FormClosed(System::Object^ sender, System::Windows::Forms::FormClosedEventArgs^ e) {
		//***アプリケーション終了時の処理
		if (DevClose->Enabled)
		{
			Tusbpio_Device_Close(DevId);//***デバイスクローズ
		}
	}
	private: System::Void DevOpen_Click(System::Object^ sender, System::EventArgs^ e) {
		short RetCode;

		//***デバイスのオープン***
		DevId = (short)IDNUM->SelectedIndex;
		RetCode = Tusbpio_Device_Open(DevId);
		if (RetCode == 0)
		{
			Control_Enable(true); //各機能ボタンを有効にする
			Timer1->Interval = 300; //タイマーインターバル
			Timer1->Enabled = true; //タイマー開始
		}
		else
		{
			MessageBox::Show(Tusbpio_GetErrMessage1(RetCode)); //***オープンに失敗した場合
		}
	}
	private: System::Void DevClose_Click(System::Object^ sender, System::EventArgs^ e) {
		//***デバイスのクローズ***
		Timer1->Enabled = false; //タイマー停止
		Tusbpio_Device_Close(DevId); //***デバイスクローズ
		Control_Enable(false); //***ボタンの有効無効切り替え
	}
	private: System::Drawing::Image^ GetLampImage(unsigned char bitmap, unsigned char bit)
	{
		unsigned char BitMask;
		BitMask = (unsigned char)Math::Pow(2, bit);
		return (((bitmap & BitMask) == 0) ? ImageList1->Images[1] : ImageList1->Images[0]);
	}
	private: System::Void Timer1_Tick(System::Object^ sender, System::EventArgs^ e) {
		short RetCode;
		unsigned char PortState[2][3];
		unsigned char port;

		// Device 1からのデータの取得
		for (port = 0; port < 3; port++)
		{
			RetCode = Tusbpio_Dev1_Read(DevId, port, &PortState[0][port]);
			if (RetCode != 0)
			{
				Timer1->Enabled = false;
				MessageBox::Show(Tusbpio_GetErrMessage2(RetCode));
				return;
			}
		}
		// Device 2からのデータの取得
		for (port = 0; port < 3; port++)
		{
			RetCode = Tusbpio_Dev2_Read(DevId, port, &PortState[1][port]);
			if (RetCode != 0)
			{
				Timer1->Enabled = false;
				MessageBox::Show(Tusbpio_GetErrMessage2(RetCode));
				return;
			}
		}
		// Device1 PortA
		Dev1PortA_I0->Image = GetLampImage(PortState[0][0], 0);
		Dev1PortA_I1->Image = GetLampImage(PortState[0][0], 1);
		Dev1PortA_I2->Image = GetLampImage(PortState[0][0], 2);
		Dev1PortA_I3->Image = GetLampImage(PortState[0][0], 3);
		Dev1PortA_I4->Image = GetLampImage(PortState[0][0], 4);
		Dev1PortA_I5->Image = GetLampImage(PortState[0][0], 5);
		Dev1PortA_I6->Image = GetLampImage(PortState[0][0], 6);
		Dev1PortA_I7->Image = GetLampImage(PortState[0][0], 7);
		// Device1 PortB
		Dev1PortB_I0->Image = GetLampImage(PortState[0][1], 0);
		Dev1PortB_I1->Image = GetLampImage(PortState[0][1], 1);
		Dev1PortB_I2->Image = GetLampImage(PortState[0][1], 2);
		Dev1PortB_I3->Image = GetLampImage(PortState[0][1], 3);
		Dev1PortB_I4->Image = GetLampImage(PortState[0][1], 4);
		Dev1PortB_I5->Image = GetLampImage(PortState[0][1], 5);
		Dev1PortB_I6->Image = GetLampImage(PortState[0][1], 6);
		Dev1PortB_I7->Image = GetLampImage(PortState[0][1], 7);
		// Device1 PortC
		Dev1PortC_I0->Image = GetLampImage(PortState[0][2], 0);
		Dev1PortC_I1->Image = GetLampImage(PortState[0][2], 1);
		Dev1PortC_I2->Image = GetLampImage(PortState[0][2], 2);
		Dev1PortC_I3->Image = GetLampImage(PortState[0][2], 3);
		Dev1PortC_I4->Image = GetLampImage(PortState[0][2], 4);
		Dev1PortC_I5->Image = GetLampImage(PortState[0][2], 5);
		Dev1PortC_I6->Image = GetLampImage(PortState[0][2], 6);
		Dev1PortC_I7->Image = GetLampImage(PortState[0][2], 7);
		// Device2 PortA
		Dev2PortA_I0->Image = GetLampImage(PortState[1][0], 0);
		Dev2PortA_I1->Image = GetLampImage(PortState[1][0], 1);
		Dev2PortA_I2->Image = GetLampImage(PortState[1][0], 2);
		Dev2PortA_I3->Image = GetLampImage(PortState[1][0], 3);
		Dev2PortA_I4->Image = GetLampImage(PortState[1][0], 4);
		Dev2PortA_I5->Image = GetLampImage(PortState[1][0], 5);
		Dev2PortA_I6->Image = GetLampImage(PortState[1][0], 6);
		Dev2PortA_I7->Image = GetLampImage(PortState[1][0], 7);
		// Device2 PortB
		Dev2PortB_I0->Image = GetLampImage(PortState[1][1], 0);
		Dev2PortB_I1->Image = GetLampImage(PortState[1][1], 1);
		Dev2PortB_I2->Image = GetLampImage(PortState[1][1], 2);
		Dev2PortB_I3->Image = GetLampImage(PortState[1][1], 3);
		Dev2PortB_I4->Image = GetLampImage(PortState[1][1], 4);
		Dev2PortB_I5->Image = GetLampImage(PortState[1][1], 5);
		Dev2PortB_I6->Image = GetLampImage(PortState[1][1], 6);
		Dev2PortB_I7->Image = GetLampImage(PortState[1][1], 7);
		// Device2 PortC
		Dev2PortC_I0->Image = GetLampImage(PortState[1][2], 0);
		Dev2PortC_I1->Image = GetLampImage(PortState[1][2], 1);
		Dev2PortC_I2->Image = GetLampImage(PortState[1][2], 2);
		Dev2PortC_I3->Image = GetLampImage(PortState[1][2], 3);
		Dev2PortC_I4->Image = GetLampImage(PortState[1][2], 4);
		Dev2PortC_I5->Image = GetLampImage(PortState[1][2], 5);
		Dev2PortC_I6->Image = GetLampImage(PortState[1][2], 6);
		Dev2PortC_I7->Image = GetLampImage(PortState[1][2], 7);
	}
	private: System::Void Dev1SetBtn_Click(System::Object^ sender, System::EventArgs^ e) {
		short RetCode;
		unsigned char ModeBitmap;

		ModeBitmap = 0x80; //モード設定 mode 0

		//Dev1 PortA 入出力方向
		ModeBitmap |= (unsigned char)(Dev1PortASelIn->Checked ? 0x10 : 0); //第4ビットを1

		//Dev1 PortB 入出力方向
		ModeBitmap |= (unsigned char)(Dev1PortBSelIn->Checked ? 0x2 : 0); //第1ビットを1

		//Dev1 PortC 下位　入出力方向
		ModeBitmap |= (unsigned char)(Dev1PortCLSelIn->Checked ? 0x1 : 0); //第0ビットを1

		//dev1 PortC 上位　入出力方向
		ModeBitmap |= (unsigned char)(Dev1PortCHSelIn->Checked ? 0x8 : 0); //第3ビットを1

		RetCode = Tusbpio_Dev1_Write(DevId, 3, ModeBitmap);
		if (RetCode != 0)
			MessageBox::Show(Tusbpio_GetErrMessage2(RetCode));

		//モードを設定すると出力ビットパターンが初期化される
		ChangeLock = true;
		Dev1PortA_O0->Checked = false;
		Dev1PortA_O1->Checked = false;
		Dev1PortA_O2->Checked = false;
		Dev1PortA_O3->Checked = false;
		Dev1PortA_O4->Checked = false;
		Dev1PortA_O5->Checked = false;
		Dev1PortA_O6->Checked = false;
		Dev1PortA_O7->Checked = false;
		Dev1PortB_O0->Checked = false;
		Dev1PortB_O1->Checked = false;
		Dev1PortB_O2->Checked = false;
		Dev1PortB_O3->Checked = false;
		Dev1PortB_O4->Checked = false;
		Dev1PortB_O5->Checked = false;
		Dev1PortB_O6->Checked = false;
		Dev1PortB_O7->Checked = false;
		Dev1PortC_O0->Checked = false;
		Dev1PortC_O1->Checked = false;
		Dev1PortC_O2->Checked = false;
		Dev1PortC_O3->Checked = false;
		Dev1PortC_O4->Checked = false;
		Dev1PortC_O5->Checked = false;
		Dev1PortC_O6->Checked = false;
		Dev1PortC_O7->Checked = false;
		ChangeLock = false;
	}
	private: System::Void Dev2SetBtn_Click(System::Object^ sender, System::EventArgs^ e) {
		short RetCode;
		unsigned char ModeBitmap;

		ModeBitmap = 0x80; //モード設定 mode 0

		//Dev2 PortA 入出力方向
		ModeBitmap |= (unsigned char)(Dev2PortASelIn->Checked ? 0x10 : 0); //第4ビットを1

		//Dev2 PortB 入出力方向
		ModeBitmap |= (unsigned char)(Dev2PortBSelIn->Checked ? 0x2 : 0); //第1ビットを1

		//Dev2 PortC 下位　入出力方向
		ModeBitmap |= (unsigned char)(Dev2PortCLSelIn->Checked ? 0x1 : 0); //第0ビットを1

		//dev1 PortC 上位　入出力方向
		ModeBitmap |= (unsigned char)(Dev2PortCHSelIn->Checked ? 0x8 : 0); //第3ビットを1

		RetCode = Tusbpio_Dev2_Write(DevId, 3, ModeBitmap);
		if (RetCode != 0)
			MessageBox::Show(Tusbpio_GetErrMessage2(RetCode));

		//モードを設定すると出力ビットパターンが初期化される
		ChangeLock = true;
		Dev2PortA_O0->Checked = false;
		Dev2PortA_O1->Checked = false;
		Dev2PortA_O2->Checked = false;
		Dev2PortA_O3->Checked = false;
		Dev2PortA_O4->Checked = false;
		Dev2PortA_O5->Checked = false;
		Dev2PortA_O6->Checked = false;
		Dev2PortA_O7->Checked = false;
		Dev2PortB_O0->Checked = false;
		Dev2PortB_O1->Checked = false;
		Dev2PortB_O2->Checked = false;
		Dev2PortB_O3->Checked = false;
		Dev2PortB_O4->Checked = false;
		Dev2PortB_O5->Checked = false;
		Dev2PortB_O6->Checked = false;
		Dev2PortB_O7->Checked = false;
		Dev2PortC_O0->Checked = false;
		Dev2PortC_O1->Checked = false;
		Dev2PortC_O2->Checked = false;
		Dev2PortC_O3->Checked = false;
		Dev2PortC_O4->Checked = false;
		Dev2PortC_O5->Checked = false;
		Dev2PortC_O6->Checked = false;
		Dev2PortC_O7->Checked = false;
		ChangeLock = false;
	}
	private: System::Void Dev1PortA_O_Changed(System::Object^ sender, System::EventArgs^ e) {
		short RetCode;
		unsigned char Bitmap = 0;

		if (!ChangeLock)
		{
			Bitmap |= (unsigned char)(Dev1PortA_O0->Checked ? 0x1 : 0);
			Bitmap |= (unsigned char)(Dev1PortA_O1->Checked ? 0x2 : 0);
			Bitmap |= (unsigned char)(Dev1PortA_O2->Checked ? 0x4 : 0);
			Bitmap |= (unsigned char)(Dev1PortA_O3->Checked ? 0x8 : 0);
			Bitmap |= (unsigned char)(Dev1PortA_O4->Checked ? 0x10 : 0);
			Bitmap |= (unsigned char)(Dev1PortA_O5->Checked ? 0x20 : 0);
			Bitmap |= (unsigned char)(Dev1PortA_O6->Checked ? 0x40 : 0);
			Bitmap |= (unsigned char)(Dev1PortA_O7->Checked ? 0x80 : 0);
			
			RetCode = Tusbpio_Dev1_Write(DevId, 0, Bitmap);
			if (RetCode != 0)
			{
				MessageBox::Show(Tusbpio_GetErrMessage2(RetCode));
			}
		}
	}
	private: System::Void Dev1PortB_O_Changed(System::Object^ sender, System::EventArgs^ e) {
		short RetCode;
		unsigned char Bitmap = 0;

		if (!ChangeLock)
		{
			Bitmap |= (unsigned char)(Dev1PortB_O0->Checked ? 0x1 : 0);
			Bitmap |= (unsigned char)(Dev1PortB_O1->Checked ? 0x2 : 0);
			Bitmap |= (unsigned char)(Dev1PortB_O2->Checked ? 0x4 : 0);
			Bitmap |= (unsigned char)(Dev1PortB_O3->Checked ? 0x8 : 0);
			Bitmap |= (unsigned char)(Dev1PortB_O4->Checked ? 0x10 : 0);
			Bitmap |= (unsigned char)(Dev1PortB_O5->Checked ? 0x20 : 0);
			Bitmap |= (unsigned char)(Dev1PortB_O6->Checked ? 0x40 : 0);
			Bitmap |= (unsigned char)(Dev1PortB_O7->Checked ? 0x80 : 0);
			
			RetCode = Tusbpio_Dev1_Write(DevId, 1, Bitmap);
			if (RetCode != 0)
			{
				MessageBox::Show(Tusbpio_GetErrMessage2(RetCode));
			}
		}
	}
	private: System::Void Dev1PortC_O_Changed(System::Object^ sender, System::EventArgs^ e) {
		short RetCode;
		unsigned char Bitmap = 0;

		if (!ChangeLock)
		{
			Bitmap |= (unsigned char)(Dev1PortC_O0->Checked ? 0x1 : 0);
			Bitmap |= (unsigned char)(Dev1PortC_O1->Checked ? 0x2 : 0);
			Bitmap |= (unsigned char)(Dev1PortC_O2->Checked ? 0x4 : 0);
			Bitmap |= (unsigned char)(Dev1PortC_O3->Checked ? 0x8 : 0);
			Bitmap |= (unsigned char)(Dev1PortC_O4->Checked ? 0x10 : 0);
			Bitmap |= (unsigned char)(Dev1PortC_O5->Checked ? 0x20 : 0);
			Bitmap |= (unsigned char)(Dev1PortC_O6->Checked ? 0x40 : 0);
			Bitmap |= (unsigned char)(Dev1PortC_O7->Checked ? 0x80 : 0);

			RetCode = Tusbpio_Dev1_Write(DevId, 2, Bitmap);
			if (RetCode != 0)
			{
				MessageBox::Show(Tusbpio_GetErrMessage2(RetCode));
			}
		}
	}
	private: System::Void Dev2PortA_O_Changed(System::Object^ sender, System::EventArgs^ e) {
		short RetCode;
		unsigned char Bitmap = 0;

		if (!ChangeLock)
		{
			Bitmap |= (unsigned char)(Dev2PortA_O0->Checked ? 0x1 : 0);
			Bitmap |= (unsigned char)(Dev2PortA_O1->Checked ? 0x2 : 0);
			Bitmap |= (unsigned char)(Dev2PortA_O2->Checked ? 0x4 : 0);
			Bitmap |= (unsigned char)(Dev2PortA_O3->Checked ? 0x8 : 0);
			Bitmap |= (unsigned char)(Dev2PortA_O4->Checked ? 0x10 : 0);
			Bitmap |= (unsigned char)(Dev2PortA_O5->Checked ? 0x20 : 0);
			Bitmap |= (unsigned char)(Dev2PortA_O6->Checked ? 0x40 : 0);
			Bitmap |= (unsigned char)(Dev2PortA_O7->Checked ? 0x80 : 0);

			RetCode = Tusbpio_Dev2_Write(DevId, 0, Bitmap);
			if (RetCode != 0)
			{
				MessageBox::Show(Tusbpio_GetErrMessage2(RetCode));
			}
		}
	}
	private: System::Void Dev2PortB_O_Changed(System::Object^ sender, System::EventArgs^ e) {
		short RetCode;
		unsigned char Bitmap = 0;

		if (!ChangeLock)
		{
			Bitmap |= (unsigned char)(Dev2PortB_O0->Checked ? 0x1 : 0);
			Bitmap |= (unsigned char)(Dev2PortB_O1->Checked ? 0x2 : 0);
			Bitmap |= (unsigned char)(Dev2PortB_O2->Checked ? 0x4 : 0);
			Bitmap |= (unsigned char)(Dev2PortB_O3->Checked ? 0x8 : 0);
			Bitmap |= (unsigned char)(Dev2PortB_O4->Checked ? 0x10 : 0);
			Bitmap |= (unsigned char)(Dev2PortB_O5->Checked ? 0x20 : 0);
			Bitmap |= (unsigned char)(Dev2PortB_O6->Checked ? 0x40 : 0);
			Bitmap |= (unsigned char)(Dev2PortB_O7->Checked ? 0x80 : 0);

			RetCode = Tusbpio_Dev2_Write(DevId, 1, Bitmap);
			if (RetCode != 0)
			{
				MessageBox::Show(Tusbpio_GetErrMessage2(RetCode));
			}
		}
	}
	private: System::Void Dev2PortC_O_Changed(System::Object^ sender, System::EventArgs^ e) {
		short RetCode;
		unsigned char Bitmap = 0;

		if (!ChangeLock)
		{
			Bitmap |= (unsigned char)(Dev2PortC_O0->Checked ? 0x1 : 0);
			Bitmap |= (unsigned char)(Dev2PortC_O1->Checked ? 0x2 : 0);
			Bitmap |= (unsigned char)(Dev2PortC_O2->Checked ? 0x4 : 0);
			Bitmap |= (unsigned char)(Dev2PortC_O3->Checked ? 0x8 : 0);
			Bitmap |= (unsigned char)(Dev2PortC_O4->Checked ? 0x10 : 0);
			Bitmap |= (unsigned char)(Dev2PortC_O5->Checked ? 0x20 : 0);
			Bitmap |= (unsigned char)(Dev2PortC_O6->Checked ? 0x40 : 0);
			Bitmap |= (unsigned char)(Dev2PortC_O7->Checked ? 0x80 : 0);

			RetCode = Tusbpio_Dev2_Write(DevId, 2, Bitmap);
			if (RetCode != 0)
			{
				MessageBox::Show(Tusbpio_GetErrMessage2(RetCode));
			}
		}
	}
	private: System::Void IDNUM_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	}
private: System::Void Dev1PortBSelOut_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
}
};
}
