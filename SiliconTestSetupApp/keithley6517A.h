#pragma once
#ifndef INCLUDED_KEITHLEY6517A
#define INCLUDED_KEITHLEY6517A
#include "power_supply.h"
#include<chrono>
#include<thread>
#include "stdafx.h"

class keithley6517A :
	public power_supply
{
public:
    keithley6517A() { m_device_name = "KEITHLEY6517A"; };
    keithley6517A(int address) { m_address = address; m_device_name = "KEITHLEY6517A";};
    ~keithley6517A() {};
    
    int initialize();
    int finalize();

    int reset(serial_interface* si);
    int configure(serial_interface* si);
    void setvoltrange(serial_interface* si, double range) {};
    int power_on(serial_interface* si);
    int power_off(serial_interface* si);
    int config_voltage(serial_interface* si);
    int config_compliance(serial_interface* si);
    double read_voltage(serial_interface* si);
    double read_current(serial_interface* si);
    void read_voltage_and_current(serial_interface* si, double& voltage, double& current);
    void read_voltage_and_current2(serial_interface* si, double& voltage, double& current);
    
    int voltage_sweep(serial_interface* si);

    bool is_on(serial_interface* si);
    bool is_off(serial_interface* si);
    bool is_mcon_on(serial_interface* si);

    void set_sweep_steps(int sweep_steps) { m_sweep_steps = sweep_steps; };
    void set_sweep_sleep_in_ms(int sweep_sleep_in_ms) { m_sweep_sleep_in_ms = sweep_sleep_in_ms; };

    std::string get_device_type() { return m_device_name; };

    std::string m_device_name;
};

#endif

