// power_supply.h
#ifndef INCLUDED_LCR_METER
#define INCLUDED_LCR_METER

#define MAX 524

class serial_interface;

class LCR_meter
{
 public:
  LCR_meter(){};
  virtual ~LCR_meter(){};

  virtual int initialize() = 0;
  virtual int finalize() = 0;
  virtual int reset(serial_interface *si) = 0;
  virtual int zeroopen(serial_interface *si) = 0;
  virtual int zeroshort(serial_interface *si) = 0;
  //  virtual int config_frequency(serial_interface *si) = 0;
  virtual int configure(serial_interface *si) = 0;
  virtual int config_frequency(serial_interface *si) = 0;
  virtual void read_capacitance_and_degree(serial_interface *si, double &cap, double &deg)=0;
  virtual void read_capacitance_and_degree2(serial_interface *si, double &cap, double &deg)=0;
  virtual void sweep_frequency(serial_interface *si, double &cap, double &deg, double &freq)=0;
  //  virtual bool is_on(serial_interface *si) = 0;
  //  virtual bool is_off(serial_interface *si) = 0;
  virtual void set_displayfunc(serial_interface* si, int type) = 0;

  //  virtual int voltage_sweep(serial_interface *si){};

  int get_address(){return m_address;};
  //  double get_voltage(){return m_voltage;};
  //  double get_compliance(){return m_compliance;};

  virtual void set_address(int address){m_address = address;}; //needed for GPIB devices;
  virtual void set_frequency(double freq){m_frequency= freq;};
  virtual void set_sfrequency(double sfreq){m_sfrequency= sfreq;};
  virtual void set_tfrequency(double tfreq){m_tfrequency= tfreq;};
  virtual void set_pfrequency(double pfreq){m_pfrequency= pfreq;};
  virtual std::string get_device_type(){return "UNDEFINED";};
  virtual std::string get_ofile_name(std::string filedir, std::string filename){return "data/"+filedir+"/CV_"+filename+".dat";};
 protected:
  int m_address = -1;
  double m_frequency = 1e6; // LCR frequency [Hz]
  double m_capacitance = 0; 
  double m_degree = 0; 
  double m_sfrequency = 1e6; // LCR frequency [Hz]
  double m_tfrequency = 1e6; // LCR frequency [Hz]
  double m_pfrequency = 1e6; // LCR frequency [Hz]

};

#endif
