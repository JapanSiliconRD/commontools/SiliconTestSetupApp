#pragma once
#ifndef INCLUDED_HP4192A
#define INCLUDED_HP4192A
#include<string>
#include "LCR_meter.h"

class hp4192A : public LCR_meter
{
public:
    hp4192A() {}
    hp4192A(int address) { m_address = address; }
    ~hp4192A() {};
    int initialize();
    int finalize();
    int reset(serial_interface* si);
    int zeroopen(serial_interface* si);
    int zeroshort(serial_interface* si);
    int config_frequency(serial_interface* si);
    int configure(serial_interface* si);
    void set_displayfunc(serial_interface* si, int type);
    //  double read_capacitance(serial_interface *si);
    //  double read_degree(serial_interface *si);
    void read_capacitance_and_degree(serial_interface* si, double& cap, double& deg);
    void read_capacitance_and_degree2(serial_interface* si, double& cap, double& deg);
    void sweep_frequency(serial_interface* si, double& cap, double& deg, double& freq);

    std::string get_device_type() { return "hp4192A"; };
};

#endif

