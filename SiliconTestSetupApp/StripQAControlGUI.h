﻿#pragma once
#include "SiliconTestSetup.h"
#include "IVCVControlGUI.h"
#include "TUSBPIOCtrl.h"
#include "windows.h"
//#include "number.h"
#include <iostream>
#include <sstream>
#include <string>
#include <msclr/marshal_cppstd.h>
#include "message.h"
//#include "../../../../../../Program Files (x86)/Reference Assemblies/Microsoft/Framework/.NETFramework/v4.6.1/mscorlib.dll"
//#include "../../../../../../Program Files (x86)/Reference Assemblies/Microsoft/Framework/.NETFramework/v4.6.1/System.Windows.Forms.dll"
#include <filesystem>


namespace SiliconTestSetupApp {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// StripQAControlGUI ‚ÌŠT—v
	/// </summary>
	public ref class StripQAControlGUI : public System::Windows::Forms::Form
	{
	public:
		StripQAControlGUI(SiliconTestSetup^ _sts)
		{
			sts = _sts;
			InitializeComponent();

		}
		StripQAControlGUI(void)
		{

			InitializeComponent();
			//
			//TODO: ‚±‚±‚ÉƒRƒ“ƒXƒgƒ‰ƒNƒ^[ ƒR[ƒh‚ð’Ç‰Á‚µ‚Ü‚·
			//
		}
	private: System::Windows::Forms::Timer^ timer1;
	private: System::Windows::Forms::Label^ label46;
	private: System::Windows::Forms::RadioButton^ radioButton2;
	private: System::Windows::Forms::RadioButton^ radioButton1;

	private: System::Windows::Forms::Label^ label48;
	private: System::Windows::Forms::Button^ button8;
	public:
		int m_GraphType; //IV=1,CV=2,ZV=3,RV=4
	protected:
		/// <summary>
		/// Žg—p’†‚ÌƒŠƒ\[ƒX‚ð‚·‚×‚ÄƒNƒŠ[ƒ“ƒAƒbƒv‚µ‚Ü‚·B
		/// </summary>
		~StripQAControlGUI()
		{
			if (components)
			{
				delete components;
			}
		}
	private: double nowTime = 0;
	private: SiliconTestSetup^ sts;
	private: Thread^ tscan;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Button^ button4;
	private: System::Windows::Forms::Button^ button5;
	private: System::Windows::Forms::GroupBox^ groupBox1;
	private: System::Windows::Forms::GroupBox^ groupBox2;
	private: System::Windows::Forms::GroupBox^ groupBox3;
	private: System::Windows::Forms::GroupBox^ groupBox4;
	private: System::Windows::Forms::GroupBox^ groupBox5;
	private: System::Windows::Forms::GroupBox^ groupBox6;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::ComboBox^ comboBox4;
	private: System::Windows::Forms::ComboBox^ comboBox3;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::ComboBox^ comboBox5;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::ComboBox^ comboBox6;
	private: System::Windows::Forms::GroupBox^ groupBox7;
	private: System::Windows::Forms::TextBox^ textBox5;
	private: System::Windows::Forms::Label^ label9;
	private: System::Windows::Forms::TextBox^ textBox4;
	private: System::Windows::Forms::Label^ label8;
	private: System::Windows::Forms::TextBox^ textBox3;
	private: System::Windows::Forms::Label^ label7;
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::RadioButton^ radioButton4;
	private: System::Windows::Forms::RadioButton^ radioButton5;
	private: System::Windows::Forms::RadioButton^ radioButton6;
	private: System::Windows::Forms::TextBox^ textBox14;
	private: System::Windows::Forms::Label^ label18;
	private: System::Windows::Forms::TextBox^ textBox18;
	private: System::Windows::Forms::TextBox^ textBox15;
	private: System::Windows::Forms::Label^ label22;
	private: System::Windows::Forms::Label^ label19;
	private: System::Windows::Forms::Label^ label21;
	private: System::Windows::Forms::TextBox^ textBox16;
	private: System::Windows::Forms::TextBox^ textBox17;
	private: System::Windows::Forms::Label^ label20;

	private: System::Windows::Forms::TextBox^ textBox20;


	private: System::Windows::Forms::Label^ label24;
	private: System::Windows::Forms::TextBox^ textBox21;

	private: System::Windows::Forms::TextBox^ textBox22;
	private: System::Windows::Forms::Label^ label25;
	private: System::Windows::Forms::Label^ label28;
	private: System::Windows::Forms::Label^ label26;
	private: System::Windows::Forms::TextBox^ textBox24;
	private: System::Windows::Forms::Label^ label27;
	private: System::Windows::Forms::TextBox^ textBox23;
	private: System::Windows::Forms::Button^ button7;
	private: System::Windows::Forms::Button^ button6;



	private: System::Windows::Forms::Button^ button9;




private: System::Windows::Forms::GroupBox^ groupBox11;



private: System::Windows::Forms::Label^ label39;
private: System::Windows::Forms::Label^ label38;
private: System::Windows::Forms::TextBox^ textBox34;
private: System::Windows::Forms::Label^ label37;
private: System::Windows::Forms::TextBox^ textBox33;
private: System::Windows::Forms::Label^ label36;
private: System::Windows::Forms::TextBox^ textBox32;
private: System::Windows::Forms::Label^ label35;
private: System::Windows::Forms::TextBox^ textBox31;
private: System::Windows::Forms::Label^ label34;
private: System::Windows::Forms::TextBox^ textBox30;
private: System::Windows::Forms::Label^ label33;
private: System::Windows::Forms::TextBox^ textBox29;
private: System::Windows::Forms::Label^ label32;
private: System::Windows::Forms::TextBox^ textBox28;
private: System::Windows::Forms::Label^ label31;
private: System::Windows::Forms::TextBox^ textBox27;
private: System::Windows::Forms::Label^ label30;



private: System::Windows::Forms::RadioButton^ radioButton10;
private: System::Windows::Forms::RadioButton^ radioButton11;
private: System::Windows::Forms::RadioButton^ radioButton15;
private: System::Windows::Forms::RadioButton^ radioButton16;
private: System::Windows::Forms::TextBox^ textBox6;
private: System::Windows::Forms::Label^ label10;
private: System::Windows::Forms::TextBox^ textBox7;
private: System::Windows::Forms::Label^ label11;
private: System::Windows::Forms::TextBox^ textBox8;
private: System::Windows::Forms::Label^ label12;
private: System::Windows::Forms::GroupBox^ groupBox10;
private: System::Windows::Forms::Button^ button10;
private: System::Windows::Forms::GroupBox^ groupBox13;
private: System::Windows::Forms::TextBox^ textBox9;
private: System::Windows::Forms::Label^ label13;
private: System::Windows::Forms::TextBox^ textBox10;
private: System::Windows::Forms::Label^ label14;
private: System::Windows::Forms::Button^ button11;
private: System::Windows::Forms::TextBox^ textBox11;
private: System::Windows::Forms::TextBox^ textBox12;
private: System::Windows::Forms::Label^ label15;
private: System::Windows::Forms::Label^ label16;
private: System::Windows::Forms::Label^ label17;
private: System::Windows::Forms::TextBox^ textBox13;
private: System::Windows::Forms::Label^ label23;
private: System::Windows::Forms::TextBox^ textBox19;
private: System::Windows::Forms::GroupBox^ groupBox14;
private: System::Windows::Forms::TextBox^ textBox25;
private: System::Windows::Forms::Label^ label29;
private: System::Windows::Forms::TextBox^ textBox35;
private: System::Windows::Forms::Label^ label40;
private: System::Windows::Forms::Button^ button12;
private: System::Windows::Forms::TextBox^ textBox36;
private: System::Windows::Forms::TextBox^ textBox37;
private: System::Windows::Forms::Label^ label41;
private: System::Windows::Forms::Label^ label42;
private: System::Windows::Forms::Label^ label43;
private: System::Windows::Forms::TextBox^ textBox38;
private: System::Windows::Forms::Label^ label44;
private: System::Windows::Forms::TextBox^ textBox39;
private: System::Windows::Forms::RadioButton^ radioButton3;
private: System::Windows::Forms::RadioButton^ radioButton7;
private: System::Windows::Forms::RadioButton^ radioButton8;
private: System::Windows::Forms::Label^ label45;
private: System::Windows::Forms::TextBox^ textBox26;
private: System::Windows::Forms::Button^ button13;
private: System::Windows::Forms::DataVisualization::Charting::Chart^ chart1;
private: System::ComponentModel::IContainer^ components;

	protected:

	private:
		/// <summary>
		/// •K—v‚ÈƒfƒUƒCƒi[•Ï”‚Å‚·B
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// ƒfƒUƒCƒi[ ƒTƒ|[ƒg‚É•K—v‚Èƒƒ\ƒbƒh‚Å‚·B‚±‚Ìƒƒ\ƒbƒh‚Ì“à—e‚ð
		/// ƒR[ƒh ƒGƒfƒBƒ^[‚Å•ÏX‚µ‚È‚¢‚Å‚­‚¾‚³‚¢B
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^ chartArea1 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::Series^ series1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^ series2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^ series3 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^ series4 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^ series5 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^ series6 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^ series7 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^ series8 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^ series9 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->radioButton4 = (gcnew System::Windows::Forms::RadioButton());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->radioButton3 = (gcnew System::Windows::Forms::RadioButton());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->radioButton5 = (gcnew System::Windows::Forms::RadioButton());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->radioButton6 = (gcnew System::Windows::Forms::RadioButton());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->radioButton7 = (gcnew System::Windows::Forms::RadioButton());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->radioButton10 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton11 = (gcnew System::Windows::Forms::RadioButton());
			this->textBox14 = (gcnew System::Windows::Forms::TextBox());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->textBox18 = (gcnew System::Windows::Forms::TextBox());
			this->textBox15 = (gcnew System::Windows::Forms::TextBox());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->textBox16 = (gcnew System::Windows::Forms::TextBox());
			this->textBox17 = (gcnew System::Windows::Forms::TextBox());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->radioButton8 = (gcnew System::Windows::Forms::RadioButton());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->radioButton15 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton16 = (gcnew System::Windows::Forms::RadioButton());
			this->textBox20 = (gcnew System::Windows::Forms::TextBox());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->textBox21 = (gcnew System::Windows::Forms::TextBox());
			this->textBox22 = (gcnew System::Windows::Forms::TextBox());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->label28 = (gcnew System::Windows::Forms::Label());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->textBox24 = (gcnew System::Windows::Forms::TextBox());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->textBox23 = (gcnew System::Windows::Forms::TextBox());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->comboBox6 = (gcnew System::Windows::Forms::ComboBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->comboBox5 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox4 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox3 = (gcnew System::Windows::Forms::ComboBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->groupBox7 = (gcnew System::Windows::Forms::GroupBox());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->groupBox11 = (gcnew System::Windows::Forms::GroupBox());
			this->radioButton2 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton1 = (gcnew System::Windows::Forms::RadioButton());
			this->label46 = (gcnew System::Windows::Forms::Label());
			this->button13 = (gcnew System::Windows::Forms::Button());
			this->label45 = (gcnew System::Windows::Forms::Label());
			this->textBox26 = (gcnew System::Windows::Forms::TextBox());
			this->label39 = (gcnew System::Windows::Forms::Label());
			this->label38 = (gcnew System::Windows::Forms::Label());
			this->textBox34 = (gcnew System::Windows::Forms::TextBox());
			this->label37 = (gcnew System::Windows::Forms::Label());
			this->textBox33 = (gcnew System::Windows::Forms::TextBox());
			this->label36 = (gcnew System::Windows::Forms::Label());
			this->textBox32 = (gcnew System::Windows::Forms::TextBox());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->textBox31 = (gcnew System::Windows::Forms::TextBox());
			this->label34 = (gcnew System::Windows::Forms::Label());
			this->textBox30 = (gcnew System::Windows::Forms::TextBox());
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->textBox29 = (gcnew System::Windows::Forms::TextBox());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->textBox28 = (gcnew System::Windows::Forms::TextBox());
			this->label31 = (gcnew System::Windows::Forms::Label());
			this->textBox27 = (gcnew System::Windows::Forms::TextBox());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->groupBox10 = (gcnew System::Windows::Forms::GroupBox());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->groupBox13 = (gcnew System::Windows::Forms::GroupBox());
			this->textBox9 = (gcnew System::Windows::Forms::TextBox());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->textBox10 = (gcnew System::Windows::Forms::TextBox());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->button11 = (gcnew System::Windows::Forms::Button());
			this->textBox11 = (gcnew System::Windows::Forms::TextBox());
			this->textBox12 = (gcnew System::Windows::Forms::TextBox());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->textBox13 = (gcnew System::Windows::Forms::TextBox());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->textBox19 = (gcnew System::Windows::Forms::TextBox());
			this->groupBox14 = (gcnew System::Windows::Forms::GroupBox());
			this->textBox25 = (gcnew System::Windows::Forms::TextBox());
			this->label29 = (gcnew System::Windows::Forms::Label());
			this->textBox35 = (gcnew System::Windows::Forms::TextBox());
			this->label40 = (gcnew System::Windows::Forms::Label());
			this->button12 = (gcnew System::Windows::Forms::Button());
			this->textBox36 = (gcnew System::Windows::Forms::TextBox());
			this->textBox37 = (gcnew System::Windows::Forms::TextBox());
			this->label41 = (gcnew System::Windows::Forms::Label());
			this->label42 = (gcnew System::Windows::Forms::Label());
			this->label43 = (gcnew System::Windows::Forms::Label());
			this->textBox38 = (gcnew System::Windows::Forms::TextBox());
			this->label44 = (gcnew System::Windows::Forms::Label());
			this->textBox39 = (gcnew System::Windows::Forms::TextBox());
			this->chart1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->label48 = (gcnew System::Windows::Forms::Label());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->groupBox6->SuspendLayout();
			this->groupBox7->SuspendLayout();
			this->groupBox11->SuspendLayout();
			this->groupBox10->SuspendLayout();
			this->groupBox13->SuspendLayout();
			this->groupBox14->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart1))->BeginInit();
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::Color::LightGreen;
			this->button1->Location = System::Drawing::Point(18, 195);
			this->button1->Margin = System::Windows::Forms::Padding(2);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(88, 64);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Rbias";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button1_Click);
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::Color::LightGreen;
			this->button2->Location = System::Drawing::Point(15, 239);
			this->button2->Margin = System::Windows::Forms::Padding(2);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(88, 64);
			this->button2->TabIndex = 1;
			this->button2->Text = L"Rint";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button2_Click);
			// 
			// button3
			// 
			this->button3->BackColor = System::Drawing::Color::LightGreen;
			this->button3->Location = System::Drawing::Point(16, 125);
			this->button3->Margin = System::Windows::Forms::Padding(2);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(88, 64);
			this->button3->TabIndex = 2;
			this->button3->Text = L"Cint";
			this->button3->UseVisualStyleBackColor = false;
			this->button3->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button3_Click);
			// 
			// button4
			// 
			this->button4->BackColor = System::Drawing::Color::LightGreen;
			this->button4->Location = System::Drawing::Point(18, 44);
			this->button4->Margin = System::Windows::Forms::Padding(2);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(88, 64);
			this->button4->TabIndex = 3;
			this->button4->Text = L"Ccpl";
			this->button4->UseVisualStyleBackColor = false;
			this->button4->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button4_Click);
			// 
			// button5
			// 
			this->button5->BackColor = System::Drawing::Color::LightGreen;
			this->button5->Location = System::Drawing::Point(11, 302);
			this->button5->Margin = System::Windows::Forms::Padding(2);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(88, 64);
			this->button5->TabIndex = 4;
			this->button5->Text = L"PTP";
			this->button5->UseVisualStyleBackColor = false;
			this->button5->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button5_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->textBox5);
			this->groupBox1->Controls->Add(this->label9);
			this->groupBox1->Controls->Add(this->textBox4);
			this->groupBox1->Controls->Add(this->label8);
			this->groupBox1->Controls->Add(this->textBox3);
			this->groupBox1->Controls->Add(this->label7);
			this->groupBox1->Controls->Add(this->textBox2);
			this->groupBox1->Controls->Add(this->label6);
			this->groupBox1->Controls->Add(this->textBox1);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->button1);
			this->groupBox1->Location = System::Drawing::Point(200, 137);
			this->groupBox1->Margin = System::Windows::Forms::Padding(2);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Padding = System::Windows::Forms::Padding(2);
			this->groupBox1->Size = System::Drawing::Size(120, 271);
			this->groupBox1->TabIndex = 5;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Rbias";
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(62, 171);
			this->textBox5->Margin = System::Windows::Forms::Padding(2);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(50, 20);
			this->textBox5->TabIndex = 12;
			this->textBox5->Text = L"1000";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(-2, 154);
			this->label9->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(83, 13);
			this->label9->TabIndex = 11;
			this->label9->Text = L"Current limit [uA]";
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(62, 123);
			this->textBox4->Margin = System::Windows::Forms::Padding(2);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(50, 20);
			this->textBox4->TabIndex = 10;
			this->textBox4->Text = L"3";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(4, 101);
			this->label8->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(62, 13);
			this->label8->TabIndex = 9;
			this->label8->Text = L"wait time [s]";
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(62, 77);
			this->textBox3->Margin = System::Windows::Forms::Padding(2);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(50, 20);
			this->textBox3->TabIndex = 8;
			this->textBox3->Text = L"0.1";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(4, 79);
			this->label7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(55, 13);
			this->label7->TabIndex = 7;
			this->label7->Text = L"Step V [V]";
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(62, 45);
			this->textBox2->Margin = System::Windows::Forms::Padding(2);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(50, 20);
			this->textBox2->TabIndex = 6;
			this->textBox2->Text = L"5";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(4, 46);
			this->label6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(52, 13);
			this->label6->TabIndex = 5;
			this->label6->Text = L"End V [V]";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(62, 15);
			this->textBox1->Margin = System::Windows::Forms::Padding(2);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(50, 20);
			this->textBox1->TabIndex = 4;
			this->textBox1->Text = L"-5";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(4, 16);
			this->label5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(55, 13);
			this->label5->TabIndex = 1;
			this->label5->Text = L"Start V [V]";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->button4);
			this->groupBox2->Controls->Add(this->radioButton4);
			this->groupBox2->Location = System::Drawing::Point(198, 412);
			this->groupBox2->Margin = System::Windows::Forms::Padding(2);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Padding = System::Windows::Forms::Padding(2);
			this->groupBox2->Size = System::Drawing::Size(120, 117);
			this->groupBox2->TabIndex = 6;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Ccpl";
			// 
			// radioButton4
			// 
			this->radioButton4->AutoSize = true;
			this->radioButton4->Checked = true;
			this->radioButton4->Location = System::Drawing::Point(11, 19);
			this->radioButton4->Margin = System::Windows::Forms::Padding(2);
			this->radioButton4->Name = L"radioButton4";
			this->radioButton4->Size = System::Drawing::Size(65, 17);
			this->radioButton4->TabIndex = 13;
			this->radioButton4->TabStop = true;
			this->radioButton4->Text = L"CCPL_C";
			this->radioButton4->UseVisualStyleBackColor = true;
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->radioButton3);
			this->groupBox3->Controls->Add(this->textBox6);
			this->groupBox3->Controls->Add(this->radioButton5);
			this->groupBox3->Controls->Add(this->label10);
			this->groupBox3->Controls->Add(this->button3);
			this->groupBox3->Controls->Add(this->radioButton6);
			this->groupBox3->Location = System::Drawing::Point(324, 137);
			this->groupBox3->Margin = System::Windows::Forms::Padding(2);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Padding = System::Windows::Forms::Padding(2);
			this->groupBox3->Size = System::Drawing::Size(120, 198);
			this->groupBox3->TabIndex = 7;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Cint";
			// 
			// radioButton3
			// 
			this->radioButton3->AutoSize = true;
			this->radioButton3->Location = System::Drawing::Point(16, 59);
			this->radioButton3->Margin = System::Windows::Forms::Padding(2);
			this->radioButton3->Name = L"radioButton3";
			this->radioButton3->Size = System::Drawing::Size(59, 17);
			this->radioButton3->TabIndex = 27;
			this->radioButton3->Text = L"manual";
			this->radioButton3->UseVisualStyleBackColor = true;
			// 
			// textBox6
			// 
			this->textBox6->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->textBox6->Location = System::Drawing::Point(55, 90);
			this->textBox6->Margin = System::Windows::Forms::Padding(2);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(50, 20);
			this->textBox6->TabIndex = 26;
			this->textBox6->Text = L"-500";
			// 
			// radioButton5
			// 
			this->radioButton5->AutoSize = true;
			this->radioButton5->Location = System::Drawing::Point(16, 38);
			this->radioButton5->Margin = System::Windows::Forms::Padding(2);
			this->radioButton5->Name = L"radioButton5";
			this->radioButton5->Size = System::Drawing::Size(45, 17);
			this->radioButton5->TabIndex = 22;
			this->radioButton5->Text = L"irrad";
			this->radioButton5->UseVisualStyleBackColor = true;
			this->radioButton5->CheckedChanged += gcnew System::EventHandler(this, &StripQAControlGUI::radioButton5_CheckedChanged);
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(13, 93);
			this->label10->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(38, 13);
			this->label10->TabIndex = 25;
			this->label10->Text = L"HV [V]";
			// 
			// radioButton6
			// 
			this->radioButton6->AutoSize = true;
			this->radioButton6->Checked = true;
			this->radioButton6->Location = System::Drawing::Point(16, 19);
			this->radioButton6->Margin = System::Windows::Forms::Padding(2);
			this->radioButton6->Name = L"radioButton6";
			this->radioButton6->Size = System::Drawing::Size(63, 17);
			this->radioButton6->TabIndex = 21;
			this->radioButton6->TabStop = true;
			this->radioButton6->Text = L"nonirrad";
			this->radioButton6->UseVisualStyleBackColor = true;
			this->radioButton6->CheckedChanged += gcnew System::EventHandler(this, &StripQAControlGUI::radioButton6_CheckedChanged);
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->radioButton7);
			this->groupBox4->Controls->Add(this->textBox7);
			this->groupBox4->Controls->Add(this->label11);
			this->groupBox4->Controls->Add(this->radioButton10);
			this->groupBox4->Controls->Add(this->radioButton11);
			this->groupBox4->Controls->Add(this->textBox14);
			this->groupBox4->Controls->Add(this->button2);
			this->groupBox4->Controls->Add(this->label18);
			this->groupBox4->Controls->Add(this->textBox18);
			this->groupBox4->Controls->Add(this->textBox15);
			this->groupBox4->Controls->Add(this->label22);
			this->groupBox4->Controls->Add(this->label19);
			this->groupBox4->Controls->Add(this->label21);
			this->groupBox4->Controls->Add(this->textBox16);
			this->groupBox4->Controls->Add(this->textBox17);
			this->groupBox4->Controls->Add(this->label20);
			this->groupBox4->Location = System::Drawing::Point(324, 335);
			this->groupBox4->Margin = System::Windows::Forms::Padding(2);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Padding = System::Windows::Forms::Padding(2);
			this->groupBox4->Size = System::Drawing::Size(120, 305);
			this->groupBox4->TabIndex = 8;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"Rint";
			// 
			// radioButton7
			// 
			this->radioButton7->AutoSize = true;
			this->radioButton7->Location = System::Drawing::Point(6, 59);
			this->radioButton7->Margin = System::Windows::Forms::Padding(2);
			this->radioButton7->Name = L"radioButton7";
			this->radioButton7->Size = System::Drawing::Size(59, 17);
			this->radioButton7->TabIndex = 27;
			this->radioButton7->Text = L"manual";
			this->radioButton7->UseVisualStyleBackColor = true;
			// 
			// textBox7
			// 
			this->textBox7->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->textBox7->Location = System::Drawing::Point(68, 77);
			this->textBox7->Margin = System::Windows::Forms::Padding(2);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(50, 20);
			this->textBox7->TabIndex = 26;
			this->textBox7->Text = L"-150";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(10, 78);
			this->label11->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(38, 13);
			this->label11->TabIndex = 25;
			this->label11->Text = L"HV [V]";
			// 
			// radioButton10
			// 
			this->radioButton10->AutoSize = true;
			this->radioButton10->Location = System::Drawing::Point(6, 38);
			this->radioButton10->Margin = System::Windows::Forms::Padding(2);
			this->radioButton10->Name = L"radioButton10";
			this->radioButton10->Size = System::Drawing::Size(45, 17);
			this->radioButton10->TabIndex = 24;
			this->radioButton10->Text = L"irrad";
			this->radioButton10->UseVisualStyleBackColor = true;
			this->radioButton10->CheckedChanged += gcnew System::EventHandler(this, &StripQAControlGUI::radioButton10_CheckedChanged_1);
			// 
			// radioButton11
			// 
			this->radioButton11->AutoSize = true;
			this->radioButton11->Checked = true;
			this->radioButton11->Location = System::Drawing::Point(6, 19);
			this->radioButton11->Margin = System::Windows::Forms::Padding(2);
			this->radioButton11->Name = L"radioButton11";
			this->radioButton11->Size = System::Drawing::Size(63, 17);
			this->radioButton11->TabIndex = 23;
			this->radioButton11->TabStop = true;
			this->radioButton11->Text = L"nonirrad";
			this->radioButton11->UseVisualStyleBackColor = true;
			this->radioButton11->CheckedChanged += gcnew System::EventHandler(this, &StripQAControlGUI::radioButton11_CheckedChanged_1);
			// 
			// textBox14
			// 
			this->textBox14->Location = System::Drawing::Point(68, 216);
			this->textBox14->Margin = System::Windows::Forms::Padding(2);
			this->textBox14->Name = L"textBox14";
			this->textBox14->Size = System::Drawing::Size(50, 20);
			this->textBox14->TabIndex = 22;
			this->textBox14->Text = L"1000";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(4, 199);
			this->label18->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(83, 13);
			this->label18->TabIndex = 21;
			this->label18->Text = L"Current limit [uA]";
			// 
			// textBox18
			// 
			this->textBox18->Location = System::Drawing::Point(68, 101);
			this->textBox18->Margin = System::Windows::Forms::Padding(2);
			this->textBox18->Name = L"textBox18";
			this->textBox18->Size = System::Drawing::Size(50, 20);
			this->textBox18->TabIndex = 14;
			this->textBox18->Text = L"-5";
			// 
			// textBox15
			// 
			this->textBox15->Location = System::Drawing::Point(68, 176);
			this->textBox15->Margin = System::Windows::Forms::Padding(2);
			this->textBox15->Name = L"textBox15";
			this->textBox15->Size = System::Drawing::Size(50, 20);
			this->textBox15->TabIndex = 20;
			this->textBox15->Text = L"3";
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(10, 102);
			this->label22->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(55, 13);
			this->label22->TabIndex = 13;
			this->label22->Text = L"Start V [V]";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(1, 177);
			this->label19->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(62, 13);
			this->label19->TabIndex = 19;
			this->label19->Text = L"wait time [s]";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(10, 130);
			this->label21->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(52, 13);
			this->label21->TabIndex = 15;
			this->label21->Text = L"End V [V]";
			// 
			// textBox16
			// 
			this->textBox16->Location = System::Drawing::Point(68, 153);
			this->textBox16->Margin = System::Windows::Forms::Padding(2);
			this->textBox16->Name = L"textBox16";
			this->textBox16->Size = System::Drawing::Size(50, 20);
			this->textBox16->TabIndex = 18;
			this->textBox16->Text = L"0.1";
			// 
			// textBox17
			// 
			this->textBox17->Location = System::Drawing::Point(68, 128);
			this->textBox17->Margin = System::Windows::Forms::Padding(2);
			this->textBox17->Name = L"textBox17";
			this->textBox17->Size = System::Drawing::Size(50, 20);
			this->textBox17->TabIndex = 16;
			this->textBox17->Text = L"5";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(10, 154);
			this->label20->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(55, 13);
			this->label20->TabIndex = 17;
			this->label20->Text = L"Step V [V]";
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->radioButton8);
			this->groupBox5->Controls->Add(this->textBox8);
			this->groupBox5->Controls->Add(this->label12);
			this->groupBox5->Controls->Add(this->radioButton15);
			this->groupBox5->Controls->Add(this->radioButton16);
			this->groupBox5->Controls->Add(this->textBox20);
			this->groupBox5->Controls->Add(this->label24);
			this->groupBox5->Controls->Add(this->button5);
			this->groupBox5->Controls->Add(this->textBox21);
			this->groupBox5->Controls->Add(this->textBox22);
			this->groupBox5->Controls->Add(this->label25);
			this->groupBox5->Controls->Add(this->label28);
			this->groupBox5->Controls->Add(this->label26);
			this->groupBox5->Controls->Add(this->textBox24);
			this->groupBox5->Controls->Add(this->label27);
			this->groupBox5->Controls->Add(this->textBox23);
			this->groupBox5->Location = System::Drawing::Point(448, 140);
			this->groupBox5->Margin = System::Windows::Forms::Padding(2);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Padding = System::Windows::Forms::Padding(2);
			this->groupBox5->Size = System::Drawing::Size(131, 380);
			this->groupBox5->TabIndex = 9;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"PTP";
			// 
			// radioButton8
			// 
			this->radioButton8->AutoSize = true;
			this->radioButton8->Location = System::Drawing::Point(11, 59);
			this->radioButton8->Margin = System::Windows::Forms::Padding(2);
			this->radioButton8->Name = L"radioButton8";
			this->radioButton8->Size = System::Drawing::Size(59, 17);
			this->radioButton8->TabIndex = 39;
			this->radioButton8->Text = L"manual";
			this->radioButton8->UseVisualStyleBackColor = true;
			// 
			// textBox8
			// 
			this->textBox8->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->textBox8->Location = System::Drawing::Point(67, 96);
			this->textBox8->Margin = System::Windows::Forms::Padding(2);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(50, 20);
			this->textBox8->TabIndex = 38;
			this->textBox8->Text = L"-400";
			this->textBox8->TextChanged += gcnew System::EventHandler(this, &StripQAControlGUI::textBox8_TextChanged);
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(9, 97);
			this->label12->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(38, 13);
			this->label12->TabIndex = 37;
			this->label12->Text = L"HV [V]";
			// 
			// radioButton15
			// 
			this->radioButton15->AutoSize = true;
			this->radioButton15->Location = System::Drawing::Point(11, 38);
			this->radioButton15->Margin = System::Windows::Forms::Padding(2);
			this->radioButton15->Name = L"radioButton15";
			this->radioButton15->Size = System::Drawing::Size(45, 17);
			this->radioButton15->TabIndex = 36;
			this->radioButton15->Text = L"irrad";
			this->radioButton15->UseVisualStyleBackColor = true;
			this->radioButton15->CheckedChanged += gcnew System::EventHandler(this, &StripQAControlGUI::radioButton15_CheckedChanged);
			// 
			// radioButton16
			// 
			this->radioButton16->AutoSize = true;
			this->radioButton16->Checked = true;
			this->radioButton16->Location = System::Drawing::Point(11, 19);
			this->radioButton16->Margin = System::Windows::Forms::Padding(2);
			this->radioButton16->Name = L"radioButton16";
			this->radioButton16->Size = System::Drawing::Size(63, 17);
			this->radioButton16->TabIndex = 35;
			this->radioButton16->TabStop = true;
			this->radioButton16->Text = L"nonirrad";
			this->radioButton16->UseVisualStyleBackColor = true;
			this->radioButton16->CheckedChanged += gcnew System::EventHandler(this, &StripQAControlGUI::radioButton16_CheckedChanged);
			// 
			// textBox20
			// 
			this->textBox20->Location = System::Drawing::Point(67, 276);
			this->textBox20->Margin = System::Windows::Forms::Padding(2);
			this->textBox20->Name = L"textBox20";
			this->textBox20->Size = System::Drawing::Size(50, 20);
			this->textBox20->TabIndex = 34;
			this->textBox20->Text = L"1000";
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->Location = System::Drawing::Point(4, 254);
			this->label24->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(83, 13);
			this->label24->TabIndex = 33;
			this->label24->Text = L"Current limit [uA]";
			// 
			// textBox21
			// 
			this->textBox21->Location = System::Drawing::Point(67, 126);
			this->textBox21->Margin = System::Windows::Forms::Padding(2);
			this->textBox21->Name = L"textBox21";
			this->textBox21->Size = System::Drawing::Size(50, 20);
			this->textBox21->TabIndex = 26;
			this->textBox21->Text = L"0";
			// 
			// textBox22
			// 
			this->textBox22->Location = System::Drawing::Point(67, 232);
			this->textBox22->Margin = System::Windows::Forms::Padding(2);
			this->textBox22->Name = L"textBox22";
			this->textBox22->Size = System::Drawing::Size(50, 20);
			this->textBox22->TabIndex = 32;
			this->textBox22->Text = L"3";
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Location = System::Drawing::Point(9, 127);
			this->label25->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(55, 13);
			this->label25->TabIndex = 25;
			this->label25->Text = L"Start V [V]";
			// 
			// label28
			// 
			this->label28->AutoSize = true;
			this->label28->Location = System::Drawing::Point(9, 190);
			this->label28->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label28->Name = L"label28";
			this->label28->Size = System::Drawing::Size(55, 13);
			this->label28->TabIndex = 29;
			this->label28->Text = L"Step V [V]";
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Location = System::Drawing::Point(9, 210);
			this->label26->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(62, 13);
			this->label26->TabIndex = 31;
			this->label26->Text = L"wait time [s]";
			// 
			// textBox24
			// 
			this->textBox24->Location = System::Drawing::Point(67, 156);
			this->textBox24->Margin = System::Windows::Forms::Padding(2);
			this->textBox24->Name = L"textBox24";
			this->textBox24->Size = System::Drawing::Size(50, 20);
			this->textBox24->TabIndex = 28;
			this->textBox24->Text = L"40";
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(9, 158);
			this->label27->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(52, 13);
			this->label27->TabIndex = 27;
			this->label27->Text = L"End V [V]";
			// 
			// textBox23
			// 
			this->textBox23->Location = System::Drawing::Point(67, 188);
			this->textBox23->Margin = System::Windows::Forms::Padding(2);
			this->textBox23->Name = L"textBox23";
			this->textBox23->Size = System::Drawing::Size(50, 20);
			this->textBox23->TabIndex = 30;
			this->textBox23->Text = L"0.2";
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->label4);
			this->groupBox6->Controls->Add(this->comboBox6);
			this->groupBox6->Controls->Add(this->label3);
			this->groupBox6->Controls->Add(this->comboBox5);
			this->groupBox6->Controls->Add(this->comboBox4);
			this->groupBox6->Controls->Add(this->comboBox3);
			this->groupBox6->Controls->Add(this->label2);
			this->groupBox6->Controls->Add(this->label1);
			this->groupBox6->Location = System::Drawing::Point(26, 15);
			this->groupBox6->Margin = System::Windows::Forms::Padding(2);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Padding = System::Windows::Forms::Padding(2);
			this->groupBox6->Size = System::Drawing::Size(430, 118);
			this->groupBox6->TabIndex = 10;
			this->groupBox6->TabStop = false;
			this->groupBox6->Text = L"Select Devices";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(217, 74);
			this->label4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(52, 13);
			this->label4->TabIndex = 8;
			this->label4->Text = L"Cint,MOS";
			this->label4->Click += gcnew System::EventHandler(this, &StripQAControlGUI::label4_Click);
			// 
			// comboBox6
			// 
			this->comboBox6->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->comboBox6->FormattingEnabled = true;
			this->comboBox6->Location = System::Drawing::Point(269, 71);
			this->comboBox6->Margin = System::Windows::Forms::Padding(2);
			this->comboBox6->Name = L"comboBox6";
			this->comboBox6->Size = System::Drawing::Size(134, 21);
			this->comboBox6->TabIndex = 7;
			this->comboBox6->SelectedIndexChanged += gcnew System::EventHandler(this, &StripQAControlGUI::comboBox6_SelectedIndexChanged);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(214, 29);
			this->label3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(57, 13);
			this->label3->TabIndex = 6;
			this->label3->Text = L"Ccpl,MD8 ";
			// 
			// comboBox5
			// 
			this->comboBox5->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->comboBox5->FormattingEnabled = true;
			this->comboBox5->Location = System::Drawing::Point(269, 27);
			this->comboBox5->Margin = System::Windows::Forms::Padding(2);
			this->comboBox5->Name = L"comboBox5";
			this->comboBox5->Size = System::Drawing::Size(134, 21);
			this->comboBox5->TabIndex = 5;
			this->comboBox5->SelectedIndexChanged += gcnew System::EventHandler(this, &StripQAControlGUI::comboBox5_SelectedIndexChanged);
			// 
			// comboBox4
			// 
			this->comboBox4->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->comboBox4->FormattingEnabled = true;
			this->comboBox4->Location = System::Drawing::Point(39, 71);
			this->comboBox4->Margin = System::Windows::Forms::Padding(2);
			this->comboBox4->Name = L"comboBox4";
			this->comboBox4->Size = System::Drawing::Size(134, 21);
			this->comboBox4->TabIndex = 4;
			this->comboBox4->SelectedIndexChanged += gcnew System::EventHandler(this, &StripQAControlGUI::comboBox4_SelectedIndexChanged);
			// 
			// comboBox3
			// 
			this->comboBox3->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->comboBox3->FormattingEnabled = true;
			this->comboBox3->Location = System::Drawing::Point(39, 27);
			this->comboBox3->Margin = System::Windows::Forms::Padding(2);
			this->comboBox3->Name = L"comboBox3";
			this->comboBox3->Size = System::Drawing::Size(134, 21);
			this->comboBox3->TabIndex = 3;
			this->comboBox3->SelectedIndexChanged += gcnew System::EventHandler(this, &StripQAControlGUI::comboBox3_SelectedIndexChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(4, 74);
			this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(22, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"HV";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(4, 29);
			this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(35, 13);
			this->label1->TabIndex = 1;
			this->label1->Text = L"TestV";
			// 
			// groupBox7
			// 
			this->groupBox7->Controls->Add(this->button7);
			this->groupBox7->Controls->Add(this->button6);
			this->groupBox7->Location = System::Drawing::Point(468, 15);
			this->groupBox7->Margin = System::Windows::Forms::Padding(2);
			this->groupBox7->Name = L"groupBox7";
			this->groupBox7->Padding = System::Windows::Forms::Padding(2);
			this->groupBox7->Size = System::Drawing::Size(103, 118);
			this->groupBox7->TabIndex = 11;
			this->groupBox7->TabStop = false;
			this->groupBox7->Text = L"LCR Calibration";
			this->groupBox7->Enter += gcnew System::EventHandler(this, &StripQAControlGUI::groupBox7_Enter);
			// 
			// button7
			// 
			this->button7->Location = System::Drawing::Point(15, 71);
			this->button7->Margin = System::Windows::Forms::Padding(2);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(53, 40);
			this->button7->TabIndex = 6;
			this->button7->Text = L"Short";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button7_Click);
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(15, 17);
			this->button6->Margin = System::Windows::Forms::Padding(2);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(53, 40);
			this->button6->TabIndex = 5;
			this->button6->Text = L"Open";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button6_Click);
			// 
			// button9
			// 
			this->button9->BackColor = System::Drawing::Color::Pink;
			this->button9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 26.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button9->Location = System::Drawing::Point(575, 20);
			this->button9->Margin = System::Windows::Forms::Padding(2);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(154, 106);
			this->button9->TabIndex = 36;
			this->button9->Text = L"STOP";
			this->button9->UseVisualStyleBackColor = false;
			this->button9->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button9_Click);
			// 
			// groupBox11
			// 
			this->groupBox11->Controls->Add(this->radioButton2);
			this->groupBox11->Controls->Add(this->radioButton1);
			this->groupBox11->Controls->Add(this->label46);
			this->groupBox11->Controls->Add(this->button13);
			this->groupBox11->Controls->Add(this->label45);
			this->groupBox11->Controls->Add(this->textBox26);
			this->groupBox11->Controls->Add(this->label39);
			this->groupBox11->Controls->Add(this->label38);
			this->groupBox11->Controls->Add(this->textBox34);
			this->groupBox11->Controls->Add(this->label37);
			this->groupBox11->Controls->Add(this->textBox33);
			this->groupBox11->Controls->Add(this->label36);
			this->groupBox11->Controls->Add(this->textBox32);
			this->groupBox11->Controls->Add(this->label35);
			this->groupBox11->Controls->Add(this->textBox31);
			this->groupBox11->Controls->Add(this->label34);
			this->groupBox11->Controls->Add(this->textBox30);
			this->groupBox11->Controls->Add(this->label33);
			this->groupBox11->Controls->Add(this->textBox29);
			this->groupBox11->Controls->Add(this->label32);
			this->groupBox11->Controls->Add(this->textBox28);
			this->groupBox11->Controls->Add(this->label31);
			this->groupBox11->Controls->Add(this->textBox27);
			this->groupBox11->Controls->Add(this->label30);
			this->groupBox11->Location = System::Drawing::Point(13, 140);
			this->groupBox11->Name = L"groupBox11";
			this->groupBox11->Size = System::Drawing::Size(180, 448);
			this->groupBox11->TabIndex = 37;
			this->groupBox11->TabStop = false;
			this->groupBox11->Text = L"General Settings";
			// 
			// radioButton2
			// 
			this->radioButton2->AutoSize = true;
			this->radioButton2->Location = System::Drawing::Point(20, 58);
			this->radioButton2->Name = L"radioButton2";
			this->radioButton2->Size = System::Drawing::Size(72, 17);
			this->radioButton2->TabIndex = 24;
			this->radioButton2->Text = L"mini+MD8";
			this->radioButton2->UseVisualStyleBackColor = true;
			// 
			// radioButton1
			// 
			this->radioButton1->AutoSize = true;
			this->radioButton1->Checked = true;
			this->radioButton1->Location = System::Drawing::Point(20, 35);
			this->radioButton1->Name = L"radioButton1";
			this->radioButton1->Size = System::Drawing::Size(96, 17);
			this->radioButton1->TabIndex = 23;
			this->radioButton1->TabStop = true;
			this->radioButton1->Text = L"TestChip+MD8";
			this->radioButton1->UseVisualStyleBackColor = true;
			// 
			// label46
			// 
			this->label46->AutoSize = true;
			this->label46->Location = System::Drawing::Point(6, 16);
			this->label46->Name = L"label46";
			this->label46->Size = System::Drawing::Size(65, 13);
			this->label46->TabIndex = 22;
			this->label46->Text = L"DeviceType";
			this->label46->Click += gcnew System::EventHandler(this, &StripQAControlGUI::label46_Click);
			// 
			// button13
			// 
			this->button13->BackColor = System::Drawing::Color::LightGreen;
			this->button13->Location = System::Drawing::Point(52, 375);
			this->button13->Margin = System::Windows::Forms::Padding(2);
			this->button13->Name = L"button13";
			this->button13->Size = System::Drawing::Size(88, 64);
			this->button13->TabIndex = 14;
			this->button13->Text = L"SET";
			this->button13->UseVisualStyleBackColor = false;
			this->button13->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button13_Click);
			// 
			// label45
			// 
			this->label45->AutoSize = true;
			this->label45->Location = System::Drawing::Point(6, 217);
			this->label45->Name = L"label45";
			this->label45->Size = System::Drawing::Size(60, 13);
			this->label45->TabIndex = 21;
			this->label45->Text = L"WaferType";
			this->label45->Click += gcnew System::EventHandler(this, &StripQAControlGUI::label45_Click);
			// 
			// textBox26
			// 
			this->textBox26->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->textBox26->Location = System::Drawing::Point(74, 214);
			this->textBox26->Name = L"textBox26";
			this->textBox26->Size = System::Drawing::Size(100, 20);
			this->textBox26->TabIndex = 3;
			// 
			// label39
			// 
			this->label39->AutoSize = true;
			this->label39->Location = System::Drawing::Point(151, 338);
			this->label39->Name = L"label39";
			this->label39->Size = System::Drawing::Size(11, 13);
			this->label39->TabIndex = 20;
			this->label39->Text = L"“";
			// 
			// label38
			// 
			this->label38->AutoSize = true;
			this->label38->Location = System::Drawing::Point(151, 314);
			this->label38->Name = L"label38";
			this->label38->Size = System::Drawing::Size(14, 13);
			this->label38->TabIndex = 19;
			this->label38->Text = L"Ž";
			// 
			// textBox34
			// 
			this->textBox34->Location = System::Drawing::Point(80, 280);
			this->textBox34->Name = L"textBox34";
			this->textBox34->Size = System::Drawing::Size(94, 20);
			this->textBox34->TabIndex = 18;
			// 
			// label37
			// 
			this->label37->AutoSize = true;
			this->label37->Location = System::Drawing::Point(7, 287);
			this->label37->Name = L"label37";
			this->label37->Size = System::Drawing::Size(44, 13);
			this->label37->TabIndex = 17;
			this->label37->Text = L"RunNo.";
			// 
			// textBox33
			// 
			this->textBox33->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->textBox33->Location = System::Drawing::Point(80, 334);
			this->textBox33->Name = L"textBox33";
			this->textBox33->Size = System::Drawing::Size(66, 20);
			this->textBox33->TabIndex = 16;
			// 
			// label36
			// 
			this->label36->AutoSize = true;
			this->label36->Location = System::Drawing::Point(7, 341);
			this->label36->Name = L"label36";
			this->label36->Size = System::Drawing::Size(47, 13);
			this->label36->TabIndex = 15;
			this->label36->Text = L"Humidity";
			// 
			// textBox32
			// 
			this->textBox32->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->textBox32->Location = System::Drawing::Point(80, 308);
			this->textBox32->Name = L"textBox32";
			this->textBox32->Size = System::Drawing::Size(66, 20);
			this->textBox32->TabIndex = 14;
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->Location = System::Drawing::Point(7, 314);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(67, 13);
			this->label35->TabIndex = 13;
			this->label35->Text = L"Temperature";
			// 
			// textBox31
			// 
			this->textBox31->Location = System::Drawing::Point(39, 256);
			this->textBox31->Name = L"textBox31";
			this->textBox31->Size = System::Drawing::Size(135, 20);
			this->textBox31->TabIndex = 12;
			// 
			// label34
			// 
			this->label34->AutoSize = true;
			this->label34->Location = System::Drawing::Point(7, 239);
			this->label34->Name = L"label34";
			this->label34->Size = System::Drawing::Size(76, 13);
			this->label34->TabIndex = 11;
			this->label34->Text = L"OperatorName";
			// 
			// textBox30
			// 
			this->textBox30->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->textBox30->Location = System::Drawing::Point(39, 180);
			this->textBox30->Name = L"textBox30";
			this->textBox30->Size = System::Drawing::Size(135, 20);
			this->textBox30->TabIndex = 10;
			// 
			// label33
			// 
			this->label33->AutoSize = true;
			this->label33->Location = System::Drawing::Point(7, 163);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(104, 13);
			this->label33->TabIndex = 9;
			this->label33->Text = L"ComponentSerialNo.";
			// 
			// textBox29
			// 
			this->textBox29->Location = System::Drawing::Point(80, 137);
			this->textBox29->Name = L"textBox29";
			this->textBox29->Size = System::Drawing::Size(94, 20);
			this->textBox29->TabIndex = 8;
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->Location = System::Drawing::Point(7, 139);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(33, 13);
			this->label32->TabIndex = 7;
			this->label32->Text = L"Label";
			// 
			// textBox28
			// 
			this->textBox28->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->textBox28->Location = System::Drawing::Point(80, 110);
			this->textBox28->Name = L"textBox28";
			this->textBox28->Size = System::Drawing::Size(94, 20);
			this->textBox28->TabIndex = 6;
			// 
			// label31
			// 
			this->label31->AutoSize = true;
			this->label31->Location = System::Drawing::Point(7, 112);
			this->label31->Name = L"label31";
			this->label31->Size = System::Drawing::Size(68, 13);
			this->label31->TabIndex = 5;
			this->label31->Text = L"Wafer:      W";
			// 
			// textBox27
			// 
			this->textBox27->Location = System::Drawing::Point(80, 86);
			this->textBox27->Name = L"textBox27";
			this->textBox27->Size = System::Drawing::Size(94, 20);
			this->textBox27->TabIndex = 4;
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->Location = System::Drawing::Point(7, 93);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(65, 13);
			this->label30->TabIndex = 1;
			this->label30->Text = L"Batch:  VPX";
			// 
			// groupBox10
			// 
			this->groupBox10->Controls->Add(this->button10);
			this->groupBox10->Location = System::Drawing::Point(592, 140);
			this->groupBox10->Margin = System::Windows::Forms::Padding(2);
			this->groupBox10->Name = L"groupBox10";
			this->groupBox10->Padding = System::Windows::Forms::Padding(2);
			this->groupBox10->Size = System::Drawing::Size(131, 116);
			this->groupBox10->TabIndex = 14;
			this->groupBox10->TabStop = false;
			this->groupBox10->Text = L"Ccpl_leak";
			// 
			// button10
			// 
			this->button10->BackColor = System::Drawing::Color::LightGreen;
			this->button10->Location = System::Drawing::Point(14, 47);
			this->button10->Margin = System::Windows::Forms::Padding(2);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(88, 64);
			this->button10->TabIndex = 3;
			this->button10->Text = L"Ccpl_leak";
			this->button10->UseVisualStyleBackColor = false;
			this->button10->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button10_Click);
			// 
			// groupBox13
			// 
			this->groupBox13->Controls->Add(this->textBox9);
			this->groupBox13->Controls->Add(this->label13);
			this->groupBox13->Controls->Add(this->textBox10);
			this->groupBox13->Controls->Add(this->label14);
			this->groupBox13->Controls->Add(this->button11);
			this->groupBox13->Controls->Add(this->textBox11);
			this->groupBox13->Controls->Add(this->textBox12);
			this->groupBox13->Controls->Add(this->label15);
			this->groupBox13->Controls->Add(this->label16);
			this->groupBox13->Controls->Add(this->label17);
			this->groupBox13->Controls->Add(this->textBox13);
			this->groupBox13->Controls->Add(this->label23);
			this->groupBox13->Controls->Add(this->textBox19);
			this->groupBox13->Location = System::Drawing::Point(592, 260);
			this->groupBox13->Margin = System::Windows::Forms::Padding(2);
			this->groupBox13->Name = L"groupBox13";
			this->groupBox13->Padding = System::Windows::Forms::Padding(2);
			this->groupBox13->Size = System::Drawing::Size(131, 380);
			this->groupBox13->TabIndex = 39;
			this->groupBox13->TabStop = false;
			this->groupBox13->Text = L"MOS";
			// 
			// textBox9
			// 
			this->textBox9->Location = System::Drawing::Point(67, 211);
			this->textBox9->Margin = System::Windows::Forms::Padding(2);
			this->textBox9->Name = L"textBox9";
			this->textBox9->Size = System::Drawing::Size(50, 20);
			this->textBox9->TabIndex = 38;
			this->textBox9->Text = L"10";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(9, 212);
			this->label13->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(53, 13);
			this->label13->TabIndex = 37;
			this->label13->Text = L"Freq[kHz]";
			// 
			// textBox10
			// 
			this->textBox10->Location = System::Drawing::Point(67, 186);
			this->textBox10->Margin = System::Windows::Forms::Padding(2);
			this->textBox10->Name = L"textBox10";
			this->textBox10->Size = System::Drawing::Size(50, 20);
			this->textBox10->TabIndex = 34;
			this->textBox10->Text = L"1000";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(4, 164);
			this->label14->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(83, 13);
			this->label14->TabIndex = 33;
			this->label14->Text = L"Current limit [uA]";
			// 
			// button11
			// 
			this->button11->BackColor = System::Drawing::Color::LightGreen;
			this->button11->Location = System::Drawing::Point(11, 302);
			this->button11->Margin = System::Windows::Forms::Padding(2);
			this->button11->Name = L"button11";
			this->button11->Size = System::Drawing::Size(88, 64);
			this->button11->TabIndex = 4;
			this->button11->Text = L"MOS";
			this->button11->UseVisualStyleBackColor = false;
			this->button11->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button11_Click);
			// 
			// textBox11
			// 
			this->textBox11->Location = System::Drawing::Point(67, 14);
			this->textBox11->Margin = System::Windows::Forms::Padding(2);
			this->textBox11->Name = L"textBox11";
			this->textBox11->Size = System::Drawing::Size(50, 20);
			this->textBox11->TabIndex = 26;
			this->textBox11->Text = L"-60";
			this->textBox11->TextChanged += gcnew System::EventHandler(this, &StripQAControlGUI::textBox11_TextChanged);
			// 
			// textBox12
			// 
			this->textBox12->Location = System::Drawing::Point(67, 133);
			this->textBox12->Margin = System::Windows::Forms::Padding(2);
			this->textBox12->Name = L"textBox12";
			this->textBox12->Size = System::Drawing::Size(50, 20);
			this->textBox12->TabIndex = 32;
			this->textBox12->Text = L"10";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(9, 15);
			this->label15->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(55, 13);
			this->label15->TabIndex = 25;
			this->label15->Text = L"Start V [V]";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(9, 77);
			this->label16->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(55, 13);
			this->label16->TabIndex = 29;
			this->label16->Text = L"Step V [V]";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(9, 111);
			this->label17->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(62, 13);
			this->label17->TabIndex = 31;
			this->label17->Text = L"wait time [s]";
			// 
			// textBox13
			// 
			this->textBox13->Location = System::Drawing::Point(67, 44);
			this->textBox13->Margin = System::Windows::Forms::Padding(2);
			this->textBox13->Name = L"textBox13";
			this->textBox13->Size = System::Drawing::Size(50, 20);
			this->textBox13->TabIndex = 28;
			this->textBox13->Text = L"0";
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Location = System::Drawing::Point(9, 45);
			this->label23->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(52, 13);
			this->label23->TabIndex = 27;
			this->label23->Text = L"End V [V]";
			// 
			// textBox19
			// 
			this->textBox19->Location = System::Drawing::Point(67, 76);
			this->textBox19->Margin = System::Windows::Forms::Padding(2);
			this->textBox19->Name = L"textBox19";
			this->textBox19->Size = System::Drawing::Size(50, 20);
			this->textBox19->TabIndex = 30;
			this->textBox19->Text = L"0.1";
			// 
			// groupBox14
			// 
			this->groupBox14->Controls->Add(this->textBox25);
			this->groupBox14->Controls->Add(this->label29);
			this->groupBox14->Controls->Add(this->textBox35);
			this->groupBox14->Controls->Add(this->label40);
			this->groupBox14->Controls->Add(this->button12);
			this->groupBox14->Controls->Add(this->textBox36);
			this->groupBox14->Controls->Add(this->textBox37);
			this->groupBox14->Controls->Add(this->label41);
			this->groupBox14->Controls->Add(this->label42);
			this->groupBox14->Controls->Add(this->label43);
			this->groupBox14->Controls->Add(this->textBox38);
			this->groupBox14->Controls->Add(this->label44);
			this->groupBox14->Controls->Add(this->textBox39);
			this->groupBox14->Location = System::Drawing::Point(727, 140);
			this->groupBox14->Margin = System::Windows::Forms::Padding(2);
			this->groupBox14->Name = L"groupBox14";
			this->groupBox14->Padding = System::Windows::Forms::Padding(2);
			this->groupBox14->Size = System::Drawing::Size(131, 380);
			this->groupBox14->TabIndex = 40;
			this->groupBox14->TabStop = false;
			this->groupBox14->Text = L"MD8";
			// 
			// textBox25
			// 
			this->textBox25->Location = System::Drawing::Point(67, 211);
			this->textBox25->Margin = System::Windows::Forms::Padding(2);
			this->textBox25->Name = L"textBox25";
			this->textBox25->Size = System::Drawing::Size(50, 20);
			this->textBox25->TabIndex = 38;
			this->textBox25->Text = L"10";
			// 
			// label29
			// 
			this->label29->AutoSize = true;
			this->label29->Location = System::Drawing::Point(9, 212);
			this->label29->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label29->Name = L"label29";
			this->label29->Size = System::Drawing::Size(53, 13);
			this->label29->TabIndex = 37;
			this->label29->Text = L"Freq[kHz]";
			// 
			// textBox35
			// 
			this->textBox35->Location = System::Drawing::Point(67, 186);
			this->textBox35->Margin = System::Windows::Forms::Padding(2);
			this->textBox35->Name = L"textBox35";
			this->textBox35->Size = System::Drawing::Size(50, 20);
			this->textBox35->TabIndex = 34;
			this->textBox35->Text = L"1000";
			// 
			// label40
			// 
			this->label40->AutoSize = true;
			this->label40->Location = System::Drawing::Point(4, 164);
			this->label40->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label40->Name = L"label40";
			this->label40->Size = System::Drawing::Size(83, 13);
			this->label40->TabIndex = 33;
			this->label40->Text = L"Current limit [uA]";
			// 
			// button12
			// 
			this->button12->BackColor = System::Drawing::Color::LightGreen;
			this->button12->Location = System::Drawing::Point(11, 302);
			this->button12->Margin = System::Windows::Forms::Padding(2);
			this->button12->Name = L"button12";
			this->button12->Size = System::Drawing::Size(88, 64);
			this->button12->TabIndex = 4;
			this->button12->Text = L"MD8";
			this->button12->UseVisualStyleBackColor = false;
			this->button12->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button12_Click);
			// 
			// textBox36
			// 
			this->textBox36->Location = System::Drawing::Point(67, 14);
			this->textBox36->Margin = System::Windows::Forms::Padding(2);
			this->textBox36->Name = L"textBox36";
			this->textBox36->Size = System::Drawing::Size(50, 20);
			this->textBox36->TabIndex = 26;
			this->textBox36->Text = L"0";
			// 
			// textBox37
			// 
			this->textBox37->Location = System::Drawing::Point(67, 133);
			this->textBox37->Margin = System::Windows::Forms::Padding(2);
			this->textBox37->Name = L"textBox37";
			this->textBox37->Size = System::Drawing::Size(50, 20);
			this->textBox37->TabIndex = 32;
			this->textBox37->Text = L"3";
			// 
			// label41
			// 
			this->label41->AutoSize = true;
			this->label41->Location = System::Drawing::Point(9, 15);
			this->label41->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label41->Name = L"label41";
			this->label41->Size = System::Drawing::Size(55, 13);
			this->label41->TabIndex = 25;
			this->label41->Text = L"Start V [V]";
			// 
			// label42
			// 
			this->label42->AutoSize = true;
			this->label42->Location = System::Drawing::Point(9, 77);
			this->label42->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label42->Name = L"label42";
			this->label42->Size = System::Drawing::Size(55, 13);
			this->label42->TabIndex = 29;
			this->label42->Text = L"Step V [V]";
			// 
			// label43
			// 
			this->label43->AutoSize = true;
			this->label43->Location = System::Drawing::Point(9, 111);
			this->label43->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label43->Name = L"label43";
			this->label43->Size = System::Drawing::Size(62, 13);
			this->label43->TabIndex = 31;
			this->label43->Text = L"wait time [s]";
			// 
			// textBox38
			// 
			this->textBox38->Location = System::Drawing::Point(67, 44);
			this->textBox38->Margin = System::Windows::Forms::Padding(2);
			this->textBox38->Name = L"textBox38";
			this->textBox38->Size = System::Drawing::Size(50, 20);
			this->textBox38->TabIndex = 28;
			this->textBox38->Text = L"1000";
			// 
			// label44
			// 
			this->label44->AutoSize = true;
			this->label44->Location = System::Drawing::Point(9, 45);
			this->label44->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label44->Name = L"label44";
			this->label44->Size = System::Drawing::Size(52, 13);
			this->label44->TabIndex = 27;
			this->label44->Text = L"End V [V]";
			// 
			// textBox39
			// 
			this->textBox39->Location = System::Drawing::Point(67, 76);
			this->textBox39->Margin = System::Windows::Forms::Padding(2);
			this->textBox39->Name = L"textBox39";
			this->textBox39->Size = System::Drawing::Size(50, 20);
			this->textBox39->TabIndex = 30;
			this->textBox39->Text = L"10";
			// 
			// chart1
			// 
			chartArea1->AxisX->InterlacedColor = System::Drawing::Color::Transparent;
			chartArea1->AxisX->LabelAutoFitMaxFontSize = 15;
			chartArea1->AxisX->LabelAutoFitMinFontSize = 14;
			chartArea1->AxisX->LineDashStyle = System::Windows::Forms::DataVisualization::Charting::ChartDashStyle::NotSet;
			chartArea1->AxisX->Title = L"Voltage[V]";
			chartArea1->AxisX->TitleFont = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			chartArea1->AxisX2->IsStartedFromZero = false;
			chartArea1->AxisX2->LabelAutoFitMaxFontSize = 14;
			chartArea1->AxisX2->TitleFont = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			chartArea1->AxisX2->TitleForeColor = System::Drawing::Color::Orange;
			chartArea1->AxisY->IsStartedFromZero = false;
			chartArea1->AxisY->LabelAutoFitMaxFontSize = 14;
			chartArea1->AxisY->LabelAutoFitMinFontSize = 14;
			chartArea1->AxisY->Title = L"Resistance[Ω]";
			chartArea1->AxisY->TitleFont = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			chartArea1->AxisY->TitleForeColor = System::Drawing::Color::DarkGreen;
			chartArea1->AxisY2->IsStartedFromZero = false;
			chartArea1->AxisY2->LabelAutoFitMaxFontSize = 14;
			chartArea1->AxisY2->Title = L"Impedance[ohm]";
			chartArea1->AxisY2->TitleFont = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			chartArea1->AxisY2->TitleForeColor = System::Drawing::Color::Orange;
			chartArea1->BackColor = System::Drawing::Color::LightCyan;
			chartArea1->Name = L"ChartArea1";
			this->chart1->ChartAreas->Add(chartArea1);
			this->chart1->Location = System::Drawing::Point(863, 20);
			this->chart1->Name = L"chart1";
			series1->ChartArea = L"ChartArea1";
			series1->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series1->Legend = L"Legend1";
			series1->MarkerColor = System::Drawing::Color::ForestGreen;
			series1->MarkerSize = 10;
			series1->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Circle;
			series1->Name = L"IV1";
			series2->ChartArea = L"ChartArea1";
			series2->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series2->MarkerColor = System::Drawing::Color::MidnightBlue;
			series2->MarkerSize = 10;
			series2->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Circle;
			series2->Name = L"IV2";
			series3->ChartArea = L"ChartArea1";
			series3->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series3->MarkerColor = System::Drawing::Color::Maroon;
			series3->MarkerSize = 10;
			series3->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Circle;
			series3->Name = L"IV3";
			series4->ChartArea = L"ChartArea1";
			series4->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series4->MarkerColor = System::Drawing::Color::ForestGreen;
			series4->MarkerSize = 10;
			series4->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Circle;
			series4->Name = L"IV4";
			series4->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Secondary;
			series5->ChartArea = L"ChartArea1";
			series5->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series5->MarkerColor = System::Drawing::Color::DarkOrange;
			series5->MarkerSize = 10;
			series5->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Star5;
			series5->Name = L"CV";
			series5->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Secondary;
			series6->ChartArea = L"ChartArea1";
			series6->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series6->MarkerColor = System::Drawing::Color::ForestGreen;
			series6->MarkerSize = 10;
			series6->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Cross;
			series6->Name = L"RV1";
			series6->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Secondary;
			series7->ChartArea = L"ChartArea1";
			series7->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series7->MarkerColor = System::Drawing::Color::MidnightBlue;
			series7->MarkerSize = 10;
			series7->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Cross;
			series7->Name = L"RV2";
			series8->ChartArea = L"ChartArea1";
			series8->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series8->MarkerColor = System::Drawing::Color::Maroon;
			series8->MarkerSize = 10;
			series8->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Cross;
			series8->Name = L"RV3";
			series9->ChartArea = L"ChartArea1";
			series9->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series9->MarkerColor = System::Drawing::Color::Orange;
			series9->MarkerSize = 10;
			series9->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Cross;
			series9->Name = L"ImpV";
			this->chart1->Series->Add(series1);
			this->chart1->Series->Add(series2);
			this->chart1->Series->Add(series3);
			this->chart1->Series->Add(series4);
			this->chart1->Series->Add(series5);
			this->chart1->Series->Add(series6);
			this->chart1->Series->Add(series7);
			this->chart1->Series->Add(series8);
			this->chart1->Series->Add(series9);
			this->chart1->Size = System::Drawing::Size(776, 672);
			this->chart1->TabIndex = 41;
			this->chart1->Text = L"chart1";
			// 
			// timer1
			// 
			this->timer1->Tick += gcnew System::EventHandler(this, &StripQAControlGUI::timer1_Tick);
			// 
			// label48
			// 
			this->label48->AutoSize = true;
			this->label48->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label48->Location = System::Drawing::Point(22, 662);
			this->label48->Name = L"label48";
			this->label48->Size = System::Drawing::Size(0, 24);
			this->label48->TabIndex = 43;
			// 
			// button8
			// 
			this->button8->Location = System::Drawing::Point(23, 620);
			this->button8->Margin = System::Windows::Forms::Padding(2);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(136, 40);
			this->button8->TabIndex = 7;
			this->button8->Text = L"Data File Path";
			this->button8->UseVisualStyleBackColor = true;
			this->button8->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button8_Click_1);
			// 
			// StripQAControlGUI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1660, 695);
			this->Controls->Add(this->button8);
			this->Controls->Add(this->label48);
			this->Controls->Add(this->chart1);
			this->Controls->Add(this->groupBox14);
			this->Controls->Add(this->groupBox13);
			this->Controls->Add(this->groupBox10);
			this->Controls->Add(this->groupBox11);
			this->Controls->Add(this->button9);
			this->Controls->Add(this->groupBox7);
			this->Controls->Add(this->groupBox6);
			this->Controls->Add(this->groupBox5);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Margin = System::Windows::Forms::Padding(2);
			this->Name = L"StripQAControlGUI";
			this->Text = L"StripQAControlGUI";
			this->Load += gcnew System::EventHandler(this, &StripQAControlGUI::StripQAControlGUI_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			this->groupBox6->ResumeLayout(false);
			this->groupBox6->PerformLayout();
			this->groupBox7->ResumeLayout(false);
			this->groupBox11->ResumeLayout(false);
			this->groupBox11->PerformLayout();
			this->groupBox10->ResumeLayout(false);
			this->groupBox13->ResumeLayout(false);
			this->groupBox13->PerformLayout();
			this->groupBox14->ResumeLayout(false);
			this->groupBox14->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		bool RB = false;
		bool CB = false;
		bool LB = false;
		bool isLS = false;
		bool isSS = false;
		bool isOtherW = false;
		bool isnonir = false;
		bool isir = false;
		bool isnonirc = false;
		bool isirc = false;
		bool isnonirp = false;
		bool isirp = false;
		int DefaultNumberOfVolt=0;
		int DefaultNumberOfImeas1=0, DefaultNumberOfImeas2=0, DefaultNumberOfImeas3=0;
		int DefaultNumberOfResistance =0;
		int DefaultNumberOfImpedance;
		int DefaultNumberOfCapacitance;
		int NumberOfVolt, NumberOfImeas1, NumberOfImeas2, NumberOfImeas3, NumberOfResistance, NumberOfImpedance, NumberOfCapacitance;
	private: System::Void StripQAControlGUI_Load(System::Object^ sender, System::EventArgs^ e) {
		bool setpsHV = false;
		bool setpsVtest = false;
		bool setlcr1 = false;
		bool setlcr2 = false;
		MSG_INFO(sts->getPS()->size() << "  power supplies have been added...");
		cli::array< System::Object^  >^ ary1 = gcnew cli::array<System::Object^>(sts->getPS()->size());
		int ii = 0;
		for (auto xx : *(sts->getPS())) {
			std::stringstream ss; ss.str("");
			ss << xx.second << ":" << xx.first->get_address();
			ary1[ii] = gcnew System::String(ss.str().c_str());
			System::Console::WriteLine(ary1[ii++]);
		}
		this->comboBox3->Items->AddRange(ary1);
		if (sts->getPS()->size() != 0) {
			this->comboBox3->Text = (String^)ary1[0];
			setpsVtest = true;
		}
		this->comboBox4->Items->AddRange(ary1);
		if (sts->getPS()->size() > 1) {
			this->comboBox4->Text = (String^)ary1[1];
			setpsHV = true;
		}
		MSG_INFO(sts->getLCR()->size() << "  LCR meters have been added...");
		cli::array< System::Object^  >^ ary2 = gcnew cli::array<System::Object^>(sts->getLCR()->size());
		ii = 0;
		for (auto xx : *(sts->getLCR())) {
			std::stringstream ss; ss.str("");
			ss << xx.second << ":" << xx.first->get_address();
			ary2[ii] = gcnew System::String(ss.str().c_str());
			System::Console::WriteLine(ary2[ii++]);
		}
		this->comboBox5->Items->AddRange(ary2);
		if (sts->getLCR()->size() != 0) {
			this->comboBox5->Text = (String^)ary2[0];
			setlcr1 = true;
		}
		this->comboBox6->Items->AddRange(ary2);
		if (sts->getLCR()->size() > 1) {
			this->comboBox6->Text = (String^)ary2[1];
			setlcr2 = true;
		}
		/*
		for (auto xx : *(sts->getLCR())) {
			std::cout << xx.first << " " << xx.second->get_address() << std::endl;
		*/
		if (sts->getPS()->size() < 2 && sts->getLCR()->size() < 2) {
			String^ mess_str = L"2 device needed.  Please select 2 devices for PS and LCR in Control Panel.";
			System::Windows::Forms::MessageBox::Show(mess_str);
			return;
		}


		String^ psHVdev;
		String^ psVtestdev;
		String^ lcr1dev;
		String^ lcr2dev;

		if (setpsVtest)	psVtestdev = (String^)ary1[0];
		else psVtestdev = L"";
		if (setpsHV) psHVdev = (String^)ary1[1];
		else psHVdev = L"";
		if (setlcr1) lcr1dev = (String^)ary2[0];
		else lcr1dev = L"";
		if (setlcr2) lcr2dev = (String^)ary2[1];
		else lcr1dev = L"";
		MSG_INFO(msclr::interop::marshal_as<std::string>(psVtestdev));
		MSG_INFO(msclr::interop::marshal_as<std::string>(psHVdev));
		MSG_INFO(msclr::interop::marshal_as<std::string>(lcr1dev));
		MSG_INFO(msclr::interop::marshal_as<std::string>(lcr2dev));
		sts->setCurrentActiveDeviceQA(msclr::interop::marshal_as<std::string>(psHVdev), msclr::interop::marshal_as<std::string>(psVtestdev),
									msclr::interop::marshal_as<std::string>(lcr1dev), msclr::interop::marshal_as<std::string>(lcr2dev));
		
	}
		   void GenSet() {
			   sts->m_QAparam_batchNumber = "VPX" + textBox27->Text;
			   sts->m_QAparam_waferNumber = "W" + textBox28->Text;
			   sts->m_QAparam_label = textBox29->Text;
			   sts->m_QAparam_serialNumber = textBox30->Text;
			   sts->m_QAparam_operatorName = textBox31->Text;
			   
				sts->m_QAparam_waferType = textBox26->Text;
				   
				 
			   sts->m_QAparam_runNumber = textBox34->Text;
			   sts->m_QAparam_temperature = textBox32->Text;
			   sts->m_QAparam_humidity = textBox33->Text;
			   sts->recordQAHeaderFile();
			   
		   }
		   void NumberReset() {
			   DefaultNumberOfVolt = 0;
			   DefaultNumberOfImeas1 = 0;
			   DefaultNumberOfImeas2 = 0;
			   DefaultNumberOfImeas3 = 0;
			   DefaultNumberOfCapacitance = 0;
			   DefaultNumberOfImpedance = 0;
			   DefaultNumberOfResistance = 0;
		   }
		   void RunRbias() {
			  
			   TUSBPIOCtrl usbpio(0);
			   usbpio.CloseDevice();
			   usbpio.OpenDevice();
			   
			   MSG_INFO("====  Rbias  ==== ");
			  // sts->m_isNegativeVoltage = 0;//false
			   sts->m_currentLimit = fabs(float::Parse(this->textBox5->Text));//uA
			   sts->m_Vstart = fabs(float::Parse(this->textBox1->Text));//V
			   sts->m_Vend = fabs(float::Parse(this->textBox2->Text));//V
			   sts->m_Vstep = fabs(float::Parse(this->textBox3->Text));//V
			   sts->m_interval = fabs(float::Parse(this->textBox4->Text));//s
			   sts->m_Tramp = 200;//us
			   sts->m_F = 100;//kHz(don't use)
			   sts->m_Fstart = 0.1;//kHz(don't use)
			   sts->m_Fend = 1000;//kHz(don't use)
			   sts->m_Vfixed = 0;//V(don't use)
			   MSG_INFO("All parameters were set!!");
			   sts->m_QAparam_testType = "RBIAS_IV";
			   sts->m_QAparam_label = "A2";
			   sts->m_QAparam_testTypeIndex = SiliconTestSetup::TestTypeIdentifier::RBIAS_IV;
			   GenSet();
			   //	sts.dh = new DataHandler;
			//   sts->getDataHandler()->SetDataDirPath("C:\\work\\Silicon\\SiliconTestSetupApp\\Data\\StripQA\\");
			//   sts->getDataHandler()->SetFilename("test");
			   sts->runIVCVScan(true, false);
			   
			   Sleep(10000);
			   usbpio.SetSWoff();
	}
	void RunCcp(){
		
		sts->m_QAparam_testType = "CCPL_C";
		sts->m_QAparam_label = "A2";
		sts->m_QAparam_testTypeIndex = SiliconTestSetup::TestTypeIdentifier::CCPL_C;
		sts->m_Fsingle = 1;//kHz
		GenSet();
		MSG_INFO("====  Ccpl  ==== ");
		//std::cout << dh->GetFullDataFilename().c_str() << std::endl;
		std::ofstream ofscon(sts->getDataHandler()->GetFullDataFilename().c_str(), std::ofstream::app);
		ofscon << "Capacitance[F] Resistance[Ohm]" << std::endl;
		sts->m_Fsingle =1 ;//kHz
		float cap = -1, dfac = -1,imp=-1,deg=-1;
		//sts->setLCRFuncType(0);
		sts->readSingleCorZ(cap, dfac, SiliconTestSetup::LCRFuncType::Cp_D);
		//sts->setLCRFuncType(1);
		sts->readSingleCorZ(imp, deg, SiliconTestSetup::LCRFuncType::Z_theta);
		ofscon << cap <<"    "<< imp << std::endl;
		Sleep(10000);
		ofscon.close();
	}
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
	//Rbias	
	//	TUSBPIOCtrl(0);
		ClearPlots();
		NumberReset();
		sts->m_QAparam_testTypeIndex = SiliconTestSetup::TestTypeIdentifier::RBIAS_IV;

		//this->chart1->Invalidate();
		
		//this->chart1->Titles->Clear();
		//this->chart1->Series->Clear();
		this->chart1->Series["IV1"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Primary;
		SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY,10);

		
		
		ThreadStart^ threadDelegate = gcnew ThreadStart(this,&StripQAControlGUI::RunRbias);
		tscan = gcnew Thread(threadDelegate);
		tscan->IsBackground = true;
		tscan->Start();
		
		//timer1->Stop();
		/*
		sts->runIVCVScan(true, false);
		Sleep(10000);
		usbpio.SetSWoff();
		

		*/


	}
	private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) {
	//	TUSBPIOCtrl(0);
		
		ThreadStart^ threadDelegate = gcnew ThreadStart(this, &StripQAControlGUI::RunCint);
		tscan = gcnew Thread(threadDelegate);
		tscan->IsBackground = true;
		tscan->Start();
	}
	
	void RunCint() {
			   //   DataHandler* dh;
			   if (radioButton6->Checked) {
				   textBox6->Text = "-150";
			    sts->m_Vend_HV = -150;
			   }
			   else if (radioButton5->Checked) {
				   textBox6->Text = "-500";
			     sts->m_Vend_HV = -500;
			   }
			   else {
				   sts->m_Vend_HV = -1 * fabs(float::Parse(this->textBox6->Text));
			   }
			   sts->m_QAparam_testType = "INT_LOW_C";
			   sts->m_QAparam_testTypeIndex = SiliconTestSetup::TestTypeIdentifier::INT_LOW_C;
			   sts->m_Fsingle = 100;//kHz
			   GenSet();
			   TUSBPIOCtrl usbpio(0);
			   usbpio.CloseDevice();
			   usbpio.OpenDevice();
			   usbpio.SetBit(0, 0x20);
			   // Tusbpio_Dev1_Write(0, 0, 0x20);
			   MSG_INFO("V_end(HV) = " << sts->m_Vend_HV);

			   usbpio.SetSWon(0);
			   sts->HVON();
			   MSG_INFO("====  Cint  ==== ");
			   std::ofstream ofscon(sts->getDataHandler()->GetFullDataFilename().c_str(), std::ofstream::app);
			   ofscon << "Capacitance[F] Resistance[Ohm]" << std::endl;
			   float cap = -1, dfac = -1, imp = -1, deg = -1;
			   //sts->setLCRFuncType(0);
			   sts->readSingleCorZ(cap, dfac, SiliconTestSetup::LCRFuncType::Cp_D);
			   //sts->setLCRFuncType(1);
			   sts->readSingleCorZ(imp, deg, SiliconTestSetup::LCRFuncType::Z_theta);
			   ofscon << cap << " " << imp << std::endl;
			   ofscon.close();
			   // sts->HVOFF();
			   Sleep(10000);
			   usbpio.SetSWoff();
			   
		   }
private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {
	//TUSBPIOCtrl(0);
	//Rint
	ClearPlots();
	NumberReset();
	this->chart1->Series["IV1"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Primary;
	SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY, 10);
	ThreadStart^ threadDelegate = gcnew ThreadStart(this, &StripQAControlGUI::RunRint);
	tscan = gcnew Thread(threadDelegate);
	tscan->IsBackground = true;
	tscan->Start();
	
}
	   void RunRint() {
		   
		   if (radioButton11->Checked) {
			   textBox7->Text = "-150";
			   sts->m_Vend_HV = -150;
		   }
		   else if (radioButton10->Checked) {
			   textBox7->Text = "-500";
			   sts->m_Vend_HV = -500;
		   }
		   else {
			   sts->m_Vend_HV = -1 * fabs(float::Parse(this->textBox7->Text));
		   }
		   MSG_INFO("VolendHV = " << sts->m_Vend_HV);
		   
		   sts->m_QAparam_testType = "INT_MID_IV";
		   sts->m_QAparam_testTypeIndex = SiliconTestSetup::TestTypeIdentifier::INT_MID_IV;
		   GenSet();

		   TUSBPIOCtrl usbpio(0);
		   usbpio.CloseDevice();
		   usbpio.OpenDevice();
		   //Tusbpio_Dev1_Write(0,0,0x10);
		   usbpio.SetBit(0, 0x10);
		   Tusbpio_Dev1_Write(0, 1, 0x4);
		   Tusbpio_Dev1_Write(0, 2, 0x1);
		   usbpio.SetSWon(0);
		   sts->HVON();
		   
		   sts->m_currentLimit = fabs(float::Parse(this->textBox14->Text));//uA
		   sts->m_Vstart = fabs(float::Parse(this->textBox18->Text));//V
		   sts->m_Vend = fabs(float::Parse(this->textBox17->Text));//V
		   sts->m_Vstep = fabs(float::Parse(this->textBox16->Text));//V
		   sts->m_interval = fabs(float::Parse(this->textBox15->Text));//s
		   sts->m_Tramp = 200;//us
		   sts->m_F = 100;//kHz(don't use)
		   sts->m_Fstart = 0.1;//kHz(don't use)
		   sts->m_Fend = 1000;//kHz(don't use)
		   sts->m_Vfixed = 0;//V(don't use)
		   sts->runIVCVScan(true, false);
		  // sts->HVOFF();
		   Sleep(10000);
		   usbpio.SetSWoff();
		//   sts->stopRun();
	   
	   }
	   void Runleak(){
		   sts->m_QAparam_testType = "CCPL_IV_10";
		   sts->m_QAparam_testTypeIndex = SiliconTestSetup::TestTypeIdentifier::CCPL_IV_10;
		   GenSet();
		   TUSBPIOCtrl usbpio(0);
		   usbpio.CloseDevice();
		   usbpio.OpenDevice();
		   Tusbpio_Dev2_Write(0, 0, 0x0);
		   usbpio.SetBit(0, 0x0);
		   usbpio.SetSWon(0);
		   sts->m_currentLimit = 100;//uA
		   sts->m_Vstart = 0;//V
		   sts->m_Vend = 10;//V
		   sts->m_Vstep = 0.5;//V
		   sts->m_interval = 3;//s
		   sts->m_Tramp = 200;//us
		   sts->m_F = 100;//kHz(don't use)
		   sts->m_Fstart = 0.1;//kHz(don't use)
		   sts->m_Fend = 1000;//kHz(don't use)
		   sts->m_Vfixed = 0;//V(don't use)
		   sts->runIVCVScan(true, false);
		   //ToDo plotsReset
		   sts->m_QAparam_testType = "CCPL_IV_100";
		   sts->m_Vend = 100;//V
		   sts->m_Vstep = 5;//V
		   sts->runIVCVScan(true, false);
		//   sts->stopRun();
	   }
	   void RunPTP() {
		   if (radioButton16->Checked) {
			   textBox8->Text = "-400";
			   sts->m_Vend_HV = -400;
		   }
		   else if (radioButton15->Checked) {
			   textBox8->Text = "-500";
			   sts->m_Vend_HV = -500;
		   }
		   else {
			   sts->m_Vend_HV = -1 * fabs(float::Parse(this->textBox8->Text));
		   }
		   sts->m_QAparam_testType = "PTP_IV";
		   GenSet();
		   TUSBPIOCtrl usbpio(0);
		   usbpio.CloseDevice();
		   usbpio.OpenDevice();
		   Tusbpio_Dev1_Write(0, 2, 0x2);
		   Tusbpio_Dev1_Write(0, 1, 0xB);
		   //Tusbpio_Dev1_Write(0,0,0x80);
		   usbpio.SetBit(0, 0x80);
		   usbpio.SetSWon(0);
		   sts->HVON();
		   sts->m_isNegativeVoltage = true;
		   sts->m_currentLimit = fabs(float::Parse(this->textBox20->Text));//uA
		   sts->m_Vstart = fabs(float::Parse(this->textBox21->Text));//V
		   sts->m_Vend = fabs(float::Parse(this->textBox24->Text));//V
		   sts->m_Vstep = fabs(float::Parse(this->textBox23->Text));//V
		   sts->m_interval = fabs(float::Parse(this->textBox22->Text));//s
		   sts->m_Tramp = 200;//us
		   sts->m_F = 100;//kHz(don't use)
		   sts->m_Fstart = 0.1;//kHz(don't use)
		   sts->m_Fend = 1000;//kHz(don't use)
		   sts->m_Vfixed = 0;//V(don't use)
		   sts->runIVCVScan(true, false);
		   //sts->HVOFF();
		   Sleep(10000);
		   usbpio.SetSWoff();
	   }
	   void RunMOS() {
		   sts->m_QAparam_testType = "CFLD_CV";
		   TUSBPIOCtrl usbpio(0);
		   usbpio.CloseDevice();
		   usbpio.OpenDevice();
		   Tusbpio_Dev2_Write(0, 0, 0x8);
		   usbpio.SetBit(0, 0x8);
		   usbpio.SetSWon(0);
		   sts->m_QAparam_testTypeIndex = SiliconTestSetup::TestTypeIdentifier::CFLD_CV;
		   sts->m_currentLimit = fabs(float::Parse(this->textBox10->Text));//uA
		   sts->m_Vend = fabs(float::Parse(this->textBox11->Text));//V
		   sts->m_Vstart = fabs(float::Parse(this->textBox13->Text));//V
		   sts->m_Vstep = fabs(float::Parse(this->textBox19->Text));//V
		   sts->m_interval = fabs(float::Parse(this->textBox12->Text));//s
		   sts->m_Tramp = 200;//us
		   sts->m_F = fabs(float::Parse(this->textBox9->Text));//kHz(don't use)
		   sts->m_Fstart = 0.1;//kHz(don't use)
		   sts->m_Fend = 1000;//kHz(don't use)
		   sts->m_Vfixed = 0;//V(don't use)
		   GenSet();
		   
		   sts->runIVCVScan(false, true);		   
		//   sts->stopRun();
	   
	   }
	   void RunMD8() {
		   TUSBPIOCtrl usbpio(0);
		   usbpio.CloseDevice();
		   usbpio.OpenDevice();
		   Tusbpio_Dev2_Write(0, 0, 0x0);
		   usbpio.SetSWon(0);
		   sts->m_QAparam_testType = "MD8_IVCV";
		   sts->m_QAparam_testTypeIndex = SiliconTestSetup::TestTypeIdentifier::MD8_IVCV;
		   sts->m_currentLimit = fabs(float::Parse(this->textBox35->Text));//uA
		   sts->m_Vstart = fabs(float::Parse(this->textBox36->Text));//V
		   sts->m_Vend = fabs(float::Parse(this->textBox38->Text));//V
		   sts->m_Vstep = fabs(float::Parse(this->textBox39->Text));//V
		   sts->m_interval = fabs(float::Parse(this->textBox37->Text));//s
		   sts->m_Tramp = 200;//us
		   sts->m_F = fabs(float::Parse(this->textBox25->Text));//kHz(don't use)
		   sts->m_Fstart = 0.1;//kHz(don't use)
		   sts->m_Fend = 1000;//kHz(don't use)
		   sts->m_Vfixed = 0;//V(don't use)
		   GenSet();
		   sts->runIVCVScan(true, true);
		//   sts->stopRun();
	   }

	   //////////////////
	   // Graph Section//
	   //////////////////
	   void SetAxisTo(System::Windows::Forms::DataVisualization::Charting::Axis^ xaxis, System::Windows::Forms::DataVisualization::Charting::Axis^ yaxis, int GraphType) {
		   if (GraphType == 1) {
			   xaxis->LabelStyle->ForeColor = System::Drawing::Color::Black;
			   xaxis->LineColor = System::Drawing::Color::Black;
			   xaxis->Title = L"Voltage [V]";
			   xaxis->TitleForeColor = System::Drawing::Color::Black;
			   //xaxis->Minimum = 0;
			   yaxis->LabelStyle->ForeColor = System::Drawing::Color::Blue;
			   yaxis->LineColor = System::Drawing::Color::Blue;
			   yaxis->Title = L"Current [uA]";
			   yaxis->TitleForeColor = System::Drawing::Color::Blue;
			   //				xaxis->IsLogarithmic = false;
			   xaxis->Minimum = 0;
		   }
		   else if (GraphType == 10) {
			   xaxis->LabelStyle->ForeColor = System::Drawing::Color::Black;
			   xaxis->LineColor = System::Drawing::Color::Black;
			   xaxis->Title = L"Voltage [V]";
			   xaxis->TitleForeColor = System::Drawing::Color::Black;
			   //xaxis->Minimum = 0;
			   yaxis->LabelStyle->ForeColor = System::Drawing::Color::Blue;
			   yaxis->LineColor = System::Drawing::Color::Blue;
			   yaxis->Title = L"Current [uA]";
			   yaxis->TitleForeColor = System::Drawing::Color::Blue;
			   //				xaxis->IsLogarithmic = false;
			   xaxis->Minimum = -5;
		   }
		   else if (GraphType == 11) {
			   xaxis->LabelStyle->ForeColor = System::Drawing::Color::Black;
			   xaxis->LineColor = System::Drawing::Color::Black;
			   xaxis->Title = L"Voltage [V]";
			   xaxis->TitleForeColor = System::Drawing::Color::Black;
			   //xaxis->Minimum = 0;
			   yaxis->LabelStyle->ForeColor = System::Drawing::Color::Blue;
			   yaxis->LineColor = System::Drawing::Color::Blue;
			   yaxis->Title = L"Current [uA]";
			   yaxis->TitleForeColor = System::Drawing::Color::Blue;
			   //				xaxis->IsLogarithmic = false;
			   xaxis->Maximum = 0;
		   }
		   else if (GraphType == 2) {
			   xaxis->LabelStyle->ForeColor = System::Drawing::Color::Black;
			   xaxis->LineColor = System::Drawing::Color::Black;
			   xaxis->Title = L"Voltage [V]";
			   xaxis->TitleForeColor = System::Drawing::Color::Black;
			   yaxis->LabelStyle->ForeColor = System::Drawing::Color::DarkOrange;
			   yaxis->LineColor = System::Drawing::Color::DarkOrange;
			   yaxis->Title = L"Capacitance [pF]";
			   yaxis->TitleForeColor = System::Drawing::Color::DarkOrange;
			   xaxis->Minimum = 0;
		   }
		   else if (GraphType == 20) {
			   xaxis->LabelStyle->ForeColor = System::Drawing::Color::Black;
			   xaxis->LineColor = System::Drawing::Color::Black;
			   xaxis->Title = L"Voltage [V]";
			   xaxis->TitleForeColor = System::Drawing::Color::Black;
			   yaxis->LabelStyle->ForeColor = System::Drawing::Color::DarkOrange;
			   yaxis->LineColor = System::Drawing::Color::DarkOrange;
			   yaxis->Title = L"Capacitance [pF]";
			   yaxis->TitleForeColor = System::Drawing::Color::DarkOrange;
			   //xaxis->Maximum = 0;
		   }
		   else if (GraphType == 3) {
			   xaxis->LabelStyle->ForeColor = System::Drawing::Color::Black;
			   xaxis->LineColor = System::Drawing::Color::Black;
			   xaxis->Title = L"Voltage [V]";
			   xaxis->TitleForeColor = System::Drawing::Color::Black;
			   yaxis->LabelStyle->ForeColor = System::Drawing::Color::DarkOrange;
			   yaxis->LineColor = System::Drawing::Color::DarkOrange;
			   yaxis->Title = L"Impedance [ohm]";
			   yaxis->TitleForeColor = System::Drawing::Color::DarkOrange;
			   xaxis->Minimum = 0;
		   }
		   else if (GraphType == 4) {
			   xaxis->LabelStyle->ForeColor = System::Drawing::Color::Black;
			   xaxis->LineColor = System::Drawing::Color::Black;
			   xaxis->Title = L"Voltage [V]";
			   xaxis->TitleForeColor = System::Drawing::Color::Black;
			   yaxis->LabelStyle->ForeColor = System::Drawing::Color::DarkOrange;
			   yaxis->LineColor = System::Drawing::Color::DarkOrange;
			   yaxis->Title = L"Resistance [M ohm]";
			   yaxis->TitleForeColor = System::Drawing::Color::DarkOrange;
			   xaxis->Maximum = 0;
		   }
		   else {
			   xaxis->LabelStyle->ForeColor = System::Drawing::Color::Black;
			   xaxis->LineColor = System::Drawing::Color::Black;
			   xaxis->Title = L"x";
			   xaxis->TitleForeColor = System::Drawing::Color::Black;
			   //xaxis->Minimum = 0;
			   yaxis->LabelStyle->ForeColor = System::Drawing::Color::Blue;
			   yaxis->LineColor = System::Drawing::Color::Blue;
			   yaxis->Title = L"y";
			   yaxis->TitleForeColor = System::Drawing::Color::Blue;
		   }




	   }
	   void ClearPlots() {
		   //			SaveImageToFile();

		   

		   if (NumberOfVolt != 0) this->chart1->Series["IV1"]->Points->Clear();
		   if (DefaultNumberOfImeas2 != 0) {
			   this->chart1->Series["IV2"]->Points->Clear();
			   this->chart1->Series["IV3"]->Points->Clear();
		   }
		   if (NumberOfCapacitance != 0) {
			   this->chart1->Series["CV"]->Points->Clear();
		   }
		   if (NumberOfResistance != 0) {
			   this->chart1->Series["RV1"]->Points->Clear();
		   }
		   sts->clearData();
	   }
	   void PlotValue(std::vector<double> x, std::vector<double> y1, std::vector<double> y2, std::vector<double> y3) {
		   if (sts->m_QAparam_testTypeIndex == SiliconTestSetup::TestTypeIdentifier::RBIAS_IV) {
			   for (unsigned int i = 0; i < y3.size(); i++) {
				   System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint21 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(x[i], y1[i]));
				   this->chart1->Series["IV1"]->Points->Add(tmppoint21);
				   System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint22 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(x[i], y2[i]));
				   this->chart1->Series["IV2"]->Points->Add(tmppoint22);
				   System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint23 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(x[i], y3[i]));
				   this->chart1->Series["IV3"]->Points->Add(tmppoint23);
			   }
		   }
		   if (sts->m_QAparam_testTypeIndex == SiliconTestSetup::TestTypeIdentifier::INT_MID_IV) {
			   for (unsigned int i = 0; i < x.size(); i++) {
				   System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint4 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(x[i], y1[i]));
				   this->chart1->Series["IV1"]->Points->Add(tmppoint4);
				   }
		   }
		   if (sts->m_QAparam_testTypeIndex == SiliconTestSetup::TestTypeIdentifier::CFLD_CV) {
			   for (unsigned int i = 0; i < x.size(); i++) {
				   System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint5 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(x[i]*-1, y1[i]));
				   this->chart1->Series["CV"]->Points->Add(tmppoint5);
			   }
		   }
		   if (sts->m_QAparam_testTypeIndex == SiliconTestSetup::TestTypeIdentifier::CCPL_IV_10) {
			   for (unsigned int i = 0; i < x.size(); i++) {
				   System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint6 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(x[i], y1[i]));
				   this->chart1->Series["IV1"]->Points->Add(tmppoint6);
			   }
		   }
		   if (sts->m_QAparam_testTypeIndex == SiliconTestSetup::TestTypeIdentifier::PTP_IV) {
			   std::cout << "NumberOfResistance = " << y2.size() << std::endl;
			   if (y2.size() > 0) {
				   for (unsigned int i = 0; i < y2.size(); i++) {
					   System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint1 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(x[i], y1[i]));
					   this->chart1->Series["IV1"]->Points->Add(tmppoint1);
					   System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint2 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(x[i], y2[i]));
					   this->chart1->Series["RV1"]->Points->Add(tmppoint2);
				   }
			   }
		   }
		   if (sts->m_QAparam_testTypeIndex == SiliconTestSetup::TestTypeIdentifier::MD8_IVCV) {
			   if(y2.size() > 0){
				   for (unsigned int i = 0; i < y2.size(); i++) {
					   
					   System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint9 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(x[i], y1[i]));
					   this->chart1->Series["IV1"]->Points->Add(tmppoint9);
					   System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint10 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(x[i], y2[i]));
					   this->chart1->Series["CV"]->Points->Add(tmppoint10);
				   }
			   }
		   }

	   }
	   


private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e) {
//	TUSBPIOCtrl(0);
	//PTP
	ClearPlots();
	NumberReset();
	sts->m_QAparam_testTypeIndex = SiliconTestSetup::TestTypeIdentifier::PTP_IV;
	this->chart1->Series["IV1"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Primary;
	SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY, 11);
	this->chart1->Series["RV1"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Secondary;
	SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY2, 4);
	ThreadStart^ threadDelegate = gcnew ThreadStart(this, &StripQAControlGUI::RunPTP);
	tscan = gcnew Thread(threadDelegate);
	tscan->IsBackground = true;
	tscan->Start();
	

}


private: System::Void groupBox7_Enter(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void comboBox3_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	sts->setCurrentActiveDeviceQA("",msclr::interop::marshal_as<std::string>((String^)comboBox3->Text), "", "");
}
private: System::Void comboBox4_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	sts->setCurrentActiveDeviceQA(msclr::interop::marshal_as<std::string>((String^)comboBox4->Text), "", "", "");
}
private: System::Void comboBox5_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	sts->setCurrentActiveDeviceQA("", "", msclr::interop::marshal_as<std::string>((String^)comboBox5->Text), "");
}
private: System::Void comboBox6_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	sts->setCurrentActiveDeviceQA("", "", "", msclr::interop::marshal_as<std::string>((String^)comboBox6->Text));
}
private: System::Void button9_Click(System::Object^ sender, System::EventArgs^ e) {
	tscan->Abort();
	delete tscan;
	sts->stopRun();
}
private: System::Void radioButton1_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	sts->m_isNegativeVoltage = 1;//negative
}
private: System::Void radioButton2_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	sts->m_isNegativeVoltage = 0;//positive
}
private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e) {
	ThreadStart^ threadDelegate = gcnew ThreadStart(this, &StripQAControlGUI::RunCcp);
	tscan = gcnew Thread(threadDelegate);
	tscan->IsBackground = true;
	tscan->Start();
}
private: System::Void radioButton9_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	LB = true;
}

private: System::Void radioButton10_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	CB = true;
}

private: System::Void radioButton11_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	RB = true;
}

private: System::Void radioButton13_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	isLS = true;
}

private: System::Void radioButton12_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	isSS = true;
}
private: System::Void radioButton14_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	isOtherW = true;
}
private: System::Void radioButton6_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	if (radioButton6->Checked) {
		textBox6->Text = "-150";
	}
}
private: System::Void radioButton5_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	if (radioButton5->Checked) {
		textBox6->Text = "-500";
	}
}


private: System::Void radioButton11_CheckedChanged_1(System::Object^ sender, System::EventArgs^ e) {
	if (radioButton11->Checked) {
		textBox7->Text = "-150";
	}
}
private: System::Void radioButton10_CheckedChanged_1(System::Object^ sender, System::EventArgs^ e) {
	if (radioButton10->Checked) {
		textBox7->Text = "-500";
	}
}
private: System::Void radioButton16_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	if (radioButton16->Checked) {
		textBox8->Text = "-400";
	}
}
private: System::Void radioButton15_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	if (radioButton15->Checked) {
		textBox8->Text = "-500";
	}
}
private: System::Void button8_Click(System::Object^ sender, System::EventArgs^ e) {
	ThreadStart^ threadDelegate1 = gcnew ThreadStart(this, &StripQAControlGUI::RunRbias);
	tscan = gcnew Thread(threadDelegate1);
	tscan->IsBackground = true;
	tscan->Start();
	ThreadStart^ threadDelegate2 = gcnew ThreadStart(this, &StripQAControlGUI::RunCcp);
	tscan = gcnew Thread(threadDelegate2);
	tscan->IsBackground = true;
	tscan->Start();
	ThreadStart^ threadDelegate3 = gcnew ThreadStart(this, &StripQAControlGUI::RunCint);
	tscan = gcnew Thread(threadDelegate3);
	tscan->IsBackground = true;
	tscan->Start();
	ThreadStart^ threadDelegate4 = gcnew ThreadStart(this, &StripQAControlGUI::RunRint);
	tscan = gcnew Thread(threadDelegate4);
	tscan->IsBackground = true;
	tscan->Start();
	ThreadStart^ threadDelegate5 = gcnew ThreadStart(this, &StripQAControlGUI::RunPTP);
	tscan = gcnew Thread(threadDelegate5);
	tscan->IsBackground = true;
	tscan->Start();
}
private: System::Void button10_Click(System::Object^ sender, System::EventArgs^ e) {

	ClearPlots();
	NumberReset();
	sts->m_QAparam_testTypeIndex = SiliconTestSetup::TestTypeIdentifier::CCPL_IV_10;
	this->chart1->Series["IV1"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Primary;
	SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY, 1);
	ThreadStart^ threadDelegate = gcnew ThreadStart(this, &StripQAControlGUI::Runleak);
	tscan = gcnew Thread(threadDelegate);
	tscan->IsBackground = true;
	tscan->Start();
}

private: System::Void button11_Click(System::Object^ sender, System::EventArgs^ e) {
	//CFLD_CV

	ClearPlots();
	NumberReset();
	sts->m_QAparam_testTypeIndex = SiliconTestSetup::TestTypeIdentifier::CFLD_CV;
	this->chart1->Series["CV"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Primary;
	SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY, 20);
	ThreadStart^ threadDelegate = gcnew ThreadStart(this, &StripQAControlGUI::RunMOS);
	tscan = gcnew Thread(threadDelegate);
	tscan->IsBackground = true;
	tscan->Start();
}
private: System::Void button12_Click(System::Object^ sender, System::EventArgs^ e) {
	ClearPlots();
	NumberReset();
	sts->m_QAparam_testTypeIndex = SiliconTestSetup::TestTypeIdentifier::MD8_IVCV;
	this->chart1->Series["IV1"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Primary;
	SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY, 1);
	this->chart1->Series["CV"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Secondary;
	SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY2, 2);
	ThreadStart^ threadDelegate = gcnew ThreadStart(this, &StripQAControlGUI::RunMD8);
	tscan = gcnew Thread(threadDelegate);
	tscan->IsBackground = true;
	tscan->Start();
	
}
private: System::Void textBox11_TextChanged(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void label4_Click(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void button6_Click(System::Object^ sender, System::EventArgs^ e) {
	comboBox3->Text = "keithley2410:24";
	comboBox4->Text = "keithley6517A:29";
	comboBox5->Text = "hp4284A:4";
	comboBox6->Text = "hp4284A:3";
	sts->setCurrentActiveDeviceQA("", msclr::interop::marshal_as<std::string>((String^)comboBox3->Text), "", "");
	sts->setCurrentActiveDeviceQA(msclr::interop::marshal_as<std::string>((String^)comboBox4->Text), "", "", "");
	sts->setCurrentActiveDeviceQA("", "", msclr::interop::marshal_as<std::string>((String^)comboBox5->Text), "");
	sts->setCurrentActiveDeviceQA("", "", "", msclr::interop::marshal_as<std::string>((String^)comboBox6->Text));
	TUSBPIOCtrl usbpio(0);
	usbpio.OpenDevice();
	Tusbpio_Dev2_Write(0, 0, 0x8);
	usbpio.SetSWon(0);
	sts->doOpenCalibration();
}
private: System::Void button7_Click(System::Object^ sender, System::EventArgs^ e) {
	comboBox3->Text = "keithley2410:24";
	comboBox4->Text = "keithley6517A:29";
	comboBox5->Text = "hp4284A:4";
	comboBox6->Text = "hp4284A:3";
	sts->setCurrentActiveDeviceQA("", msclr::interop::marshal_as<std::string>((String^)comboBox3->Text), "", "");
	sts->setCurrentActiveDeviceQA(msclr::interop::marshal_as<std::string>((String^)comboBox4->Text), "", "", "");
	sts->setCurrentActiveDeviceQA("", "", msclr::interop::marshal_as<std::string>((String^)comboBox5->Text), "");
	sts->setCurrentActiveDeviceQA("", "", "", msclr::interop::marshal_as<std::string>((String^)comboBox6->Text));
	TUSBPIOCtrl usbpio(0);
	usbpio.OpenDevice();
	Tusbpio_Dev2_Write(0, 0, 0x8);
	usbpio.SetSWon(0);
	sts->doShortCalibration();
}
private: System::Void label45_Click(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void button13_Click(System::Object^ sender, System::EventArgs^ e) {
	timer1->Enabled = true;
		String^ BatchNumber = "VPX" + fabs(float::Parse(this->textBox27->Text));
		if (radioButton1->Checked) {
			sts->m_QAparam_DeviceType = "7: Test chip & MD8";
			sts->m_QAparam_DeviceNumber = "7";
		}
		else if (radioButton2->Checked) {
			sts->m_QAparam_DeviceType = "1: Mini & MD8";
			sts->m_QAparam_DeviceNumber = "1";
		}
			std::ifstream StripInfo("C:\\Users\\atlas\\OneDrive\\Desktop\\shared\\db\\ITk strip TEGs stored in KEK - Sheet1.csv");
			std::vector < std::string> v;
			std::string line;
			while (getline(StripInfo, line)) {
				v = {};
				std::stringstream ss{ line };
				while (getline(ss, line, ',')) {
					v.push_back(line);
				}
				if(radioButton1->Checked && v[3] == "7: Test chip & MD8"){
						if (v[1] == msclr::interop::marshal_as<std::string>(BatchNumber)) {
							std::string WaferNumber = v[2].substr(1, 5);
							String^ WaferNumberS = gcnew String(WaferNumber.c_str());
							textBox28->Text = WaferNumberS;
							String^ Serial = gcnew String(v[0].c_str());
							textBox30->Text = Serial;
							String^ WaferType = gcnew String(v[5].c_str());
							textBox26->Text = WaferType;
						}
					}
				else if (radioButton2->Checked && v[3] == "1: Mini & MD8") {
						if (v[1] == msclr::interop::marshal_as<std::string>(BatchNumber)) {
							std::string WaferNumber = v[2].substr(1, 5);
							String^ WaferNumberS = gcnew String(WaferNumber.c_str());
							textBox28->Text = WaferNumberS;
							String^ Serial = gcnew String(v[0].c_str());
							textBox30->Text = Serial;
							String^ WaferType = gcnew String(v[5].c_str());
							textBox26->Text = WaferType;
						}
					}

			}
			StripInfo.close();
			std::ifstream TempInfo("C:\\Users\\atlas\\OneDrive\\Desktop\\shared\\trh\\temp.dat");
			while (getline(TempInfo, line)) {
				v = {};
				std::stringstream ss{ line };
				while (getline(ss, line, ' ')) {
					v.push_back(line);
				}
				int templong = v[2].size();
				int humlong = v[3].size();
				std::string TEMP = v[2].substr(5, templong - 6);
				String^ Temperature = gcnew String(TEMP.c_str());
				std::string hum = v[3].substr(0, humlong - 1);
				String^ Humidity = gcnew String(hum.c_str());
				textBox32->Text = Temperature;
				textBox33->Text = Humidity;

			}
			TempInfo.close();
	comboBox3->Text = "keithley2410:24";
	comboBox4->Text = "keithley6517A:29";
	comboBox5->Text = "hp4284A:4";
	comboBox6->Text = "hp4284A:3";
	sts->setCurrentActiveDeviceQA("", msclr::interop::marshal_as<std::string>((String^)comboBox3->Text), "","");
	sts->setCurrentActiveDeviceQA(msclr::interop::marshal_as<std::string>((String^)comboBox4->Text),"", "", "");
	sts->setCurrentActiveDeviceQA("","", msclr::interop::marshal_as<std::string>((String^)comboBox5->Text),"");
	sts->setCurrentActiveDeviceQA("", "","", msclr::interop::marshal_as<std::string>((String^)comboBox6->Text));

}
private: System::Void timer1_Tick(System::Object^ sender, System::EventArgs^ e) {
	
	NumberOfVolt = sts->getVolt1().size();
	NumberOfImeas1 = sts->getCurrent().size();
	NumberOfImeas2 = sts->getCurrent2().size();
	NumberOfImeas3 = sts->getCurrent3().size();
	NumberOfResistance = sts->getResistance().size();
	NumberOfCapacitance = sts->getCapacitance().size();
	
	if(NumberOfVolt != 0){
		
	if (DefaultNumberOfVolt != NumberOfVolt) {
		if (sts->m_QAparam_testTypeIndex == SiliconTestSetup::TestTypeIdentifier::RBIAS_IV) {
	
			PlotValue(sts->getVolt1(), sts->getCurrent1(), sts->getCurrent2(), sts->getCurrent3());
			DefaultNumberOfVolt = NumberOfVolt;
			DefaultNumberOfImeas1 = NumberOfImeas1;
			DefaultNumberOfImeas2 = NumberOfImeas2;
			DefaultNumberOfImeas3 = NumberOfImeas3;
		}
		if (sts->m_QAparam_testTypeIndex == SiliconTestSetup::TestTypeIdentifier::INT_MID_IV) {
			PlotValue(sts->getVolt1(), sts->getCurrent1(), sts->getCurrent2(), sts->getCurrent3());
			DefaultNumberOfVolt = NumberOfVolt;
			DefaultNumberOfImeas1 = NumberOfImeas1;
			}
		if (sts->m_QAparam_testTypeIndex == SiliconTestSetup::TestTypeIdentifier::PTP_IV) {

			PlotValue(sts->getVolt1(), sts->getCurrent1(), sts->getResistance(), sts->getCurrent1());
			DefaultNumberOfVolt = NumberOfVolt;
			DefaultNumberOfImeas1 = NumberOfImeas1;
			DefaultNumberOfResistance = NumberOfResistance;
		}
		if (sts->m_QAparam_testTypeIndex == SiliconTestSetup::TestTypeIdentifier::MD8_IVCV) {
		
			PlotValue(sts->getVolt1(), sts->getCurrent1(), sts->getCapacitance(), sts->getCurrent1());
			DefaultNumberOfVolt = NumberOfVolt;
			DefaultNumberOfImeas1 = NumberOfImeas1;
			DefaultNumberOfCapacitance = NumberOfCapacitance;

		}
		if (sts->m_QAparam_testTypeIndex == SiliconTestSetup::TestTypeIdentifier::CFLD_CV) {
		
			PlotValue(sts->getVolt1(), sts->getCapacitance(), sts->getCapacitance(), sts->getCapacitance());
			DefaultNumberOfVolt = NumberOfVolt;
			DefaultNumberOfCapacitance = NumberOfCapacitance;


		}
	}
	}
	
}
private: System::Void label46_Click(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void textBox8_TextChanged(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void label47_Click(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void button8_Click_1(System::Object^ sender, System::EventArgs^ e) {
	String^ QApath = gcnew String(sts->getDataHandler()->GetQApath().c_str());
	label48->Text = QApath;
}
};
}