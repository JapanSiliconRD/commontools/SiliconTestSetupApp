#include "pch.h"
#include "PrologixGPIB.h"
#include "message.h"

PrologixGPIB::PrologixGPIB() {
    port = "";
    initialized = false;

}
int PrologixGPIB::initialize() {
    // Open port
    MSG_INFO("Opening port: " << port << "...");
    error = PxSerialOpen(port.c_str());
    if (error != 0)
    {
 //       printf("Error %08x opening %s.\n", error, port);
        MSG_ERROR("Error in opening " << port << "(error code = " << error << ")");

        return -1;
    }
    initialized = true;

    return 0;
}
int PrologixGPIB::finalize() {
    return ClosePort();
}
int PrologixGPIB::ClosePort() {
    // Close port
    error = PxSerialClose();
    if (error != 0)
    {
 //       printf("Error %08x closing %s.\n", error, port);
        MSG_ERROR("Error in cloding " << port << "(error code = " << error << ")");

    }
    return 0;
}
int PrologixGPIB::write(char* buf, size_t length) {
    //   return ::write(dev, buf, length);
    DWORD  m_written = 0;
    MSG_DEBUG("Writing" << ": " << buf);
    return PxSerialWrite(buf, length, &m_written);
}

int PrologixGPIB::write(std::string bufstr) {
//    return ::write(dev, buf.c_str(), buf.size());
    const char* bufc = bufstr.c_str();
    DWORD m_written = 0;
    MSG_DEBUG("Writing" << ": " << bufc);
    return PxSerialWrite((char*)bufc, bufstr.size(), &m_written);
}
int PrologixGPIB::read(char* buf, size_t length) {
    //    return ::read(dev, buf, length);
    DWORD* const m_read = 0;
    int ret= PxSerialRead(buf, length, m_read);
    MSG_DEBUG("c reading" << " : " << buf);
    return ret;
}



int PrologixGPIB::read(std::string& buf) {

 //   char* tmp = new char[MAX_READ];
    char tmp[MAX_READ]="";
//    std::cout << MAX_READ << std::endl;
 //    char tmp[MAX_READ];
    //    unsigned n_read = ::read(dev, tmp, MAX_READ);
    DWORD m_read;
//    unsigned n_read = PxSerialRead(tmp, MAX_READ, m_read);
    error = PxSerialRead(tmp, MAX_READ, &m_read);
    if (m_read > 0) {
//        std::cout << m_read << std::endl;
//        for (int ii = 0; ii < m_read - 2; ii++) std::cout << (int)(tmp[ii]+5) << std::endl;
        tmp[m_read] = 0;
        buf = std::string(tmp, m_read - 1);
        //buf = std::string(tmp, m_read - 2);
        MSG_DEBUG("tmp: " << tmp << " " << m_read << " " << MAX_READ);
        MSG_DEBUG("s reading" << " : " << buf);
    }
    else {
//        std::cout << "received 0 size " << std::endl;
        buf = "";
    }
    return error;
}

int PrologixGPIB::writeread(std::string _cmd, std::string& buf) {
    cmd = _cmd;


    // Append CR and LF
    char buffer[256];
    sprintf_s(buffer, "%s\r\n", _cmd.c_str());
    // Write command to port
    DWORD written = 0;
    error = PxSerialWrite(buffer, (DWORD)strlen(buffer), &written);


//    const char* bufc = _cmd.c_str();
//    DWORD m_written = 0;
//    error = PxSerialWrite((char*)bufc, _cmd.size(), &m_written);
//    error = PxSerialWrite((char*)buffer, _cmd.size(), &m_written);

    if (error != 0)
    {
//        printf("Error %08x writing %s.\n", error, port.c_str());
        MSG_ERROR("Error " << error << " writing " << port);
        return -1;
    }
    // TODO: Adjust timeout as needed
    DWORD elapsedTime = 0;
    DWORD lastRead = timeGetTime();


    // Read until TIMEOUT time has elapsed since last
    // successful read.
    while (elapsedTime <= TIMEOUT)
    {
        DWORD bytesRead;

        error = PxSerialRead(buffer, sizeof(buffer) - 1, &bytesRead);
        if (error != 0)
        {
    //        printf("Error %08x reading %s.\n", error, port);
            MSG_ERROR("Error " << error << " reading " << port);

            break;
        }

        if (bytesRead > 0)
        {
            buffer[bytesRead] = 0;    // Append NULL to print to console
   //         printf("[return] %s", buffer);
            MSG_DEBUG("[return] " << buffer);
            buf = buffer;

            lastRead = timeGetTime();
        }
        elapsedTime = timeGetTime() - lastRead;
    }
    std::cout << std::endl;
    return 0;
}

