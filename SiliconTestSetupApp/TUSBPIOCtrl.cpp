#include "stdafx.h"
#include "pch.h"
#include "TUSBPIOCtrl.h"
#include <iostream>
#include "message.h"

TUSBPIOCtrl::TUSBPIOCtrl() {
    Initialize();
}
TUSBPIOCtrl::TUSBPIOCtrl(int id) {
    DevId = id;
    Initialize();
    MSG_INFO("TUSBPIO device has been successfully initialised.");
}

int TUSBPIOCtrl::Initialize() {
    PortABitmap = 0;
    PortBBitmap = 0;
    PortCBitmap = 0;
    OpenDevice();
    //SetAlloutput();
    return 0;
}
int TUSBPIOCtrl::OpenDevice() {
    short RetCode;
    
    //***デバイスのオープン***
    RetCode = Tusbpio_Device_Open(DevId);
    if (RetCode == 0)
    {
        MSG_INFO("TUSBPIO device has been opened successfully!");
    }
    else
    {
        MSG_ERROR("TUSBPIO device could not be open properly... (error code = " << RetCode << ")");
    }
    return RetCode;
}
void TUSBPIOCtrl::CloseDevice() {
    Tusbpio_Device_Close(DevId); //***デバイスクローズ
}
int TUSBPIOCtrl::SetAlloutput() {
    short RetCode;
    unsigned char ModeBitmap;

    ModeBitmap = 0x80; //モード設定 mode 0
    std::cout << std::hex << ModeBitmap << std::dec << std::endl;



    RetCode = Tusbpio_Dev1_Write(DevId, 3, ModeBitmap);
    if (RetCode != 0)
        MSG_ERROR("Mode set failed... (error code = " << RetCode << ")");
    return RetCode;
}
int TUSBPIOCtrl::SetSWon(int port) {
    unsigned char bit;
    if (port == 0)bit = PortABitmap;
    else if (port == 1)bit = PortBBitmap;
    else if (port == 2)bit = PortCBitmap;
    short RetCode = Tusbpio_Dev1_Write(DevId, port, bit);
       return 0;
}
int TUSBPIOCtrl::SetSWoff() {
    PortABitmap = 0;
    PortBBitmap = 0;
    PortCBitmap = 0;
    short RetCode;
    RetCode = Tusbpio_Dev1_Write(DevId, 0, PortABitmap);
    RetCode = Tusbpio_Dev1_Write(DevId, 1, PortBBitmap);
    RetCode = Tusbpio_Dev1_Write(DevId, 2, PortCBitmap);
    RetCode = Tusbpio_Dev2_Write(DevId, 0, PortABitmap);
    return 0;
}
